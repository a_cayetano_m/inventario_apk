package com.example.invfisico;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class DelLocationActivity extends AppCompatActivity {
    private ListView LocationList;
    SearchView SearchLocations;
    ArrayList<String> locations;
    ArrayList<String> locations_filters;
    ArrayAdapter<String> adapter;
    Button btn_deleteAllLocation;

    @SuppressLint("MissingInflatedId")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_location);
        getSupportActionBar().hide();

        //SearchLocations=findViewById(R.id.search_locations);
        btn_deleteAllLocation = findViewById(R.id.btn_deleteAllLocation);
        LocationList = (ListView) findViewById(R.id.LocationList);
        locations = new ArrayList<String>();
        locations_filters = new ArrayList<String>();
        showLocations();

        adapter= new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,locations);

        LocationList.setAdapter(adapter);

        LocationList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               /* System.out.println("id: "+id);
                System.out.println("position: "+position);
                System.out.println(SearchLocations.getQuery().toString());*/
                String valor_item="";
                Boolean searching=false;
                String val_search = SearchLocations.getQuery().toString();
                System.out.println(SearchLocations.getQuery().toString());
                System.out.println("position: "+position);
                System.out.println(val_search.length());
                if(val_search.length()!=0){
                    System.out.println("en if");
                    System.out.println(locations_filters);
                    searching=true;
                    valor_item=locations_filters.get(position);
                }else{
                    System.out.println("en else");
                    searching=false;
                    valor_item=locations.get(position);
                }
                Boolean finalSearching = searching;
                new AlertDialog.Builder(DelLocationActivity.this)
                        .setTitle("¿Borrar "+valor_item+" ?")
                        .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                removeLocation(position, finalSearching);
                            }
                        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).create().show();

            }
        });
        SearchLocations.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                adapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                ArrayList<String> loc_filters = new ArrayList<String>();
                for (String element:locations) {
                    if (element.contains(newText)){
                        //add element to result box
                        loc_filters.add(element);
                    }
                }
                locations_filters=loc_filters;
                adapter.getFilter().filter(newText);
                return false;
            }
        });
        btn_deleteAllLocation.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                new AlertDialog.Builder(DelLocationActivity.this)
                        .setTitle("¿Borrar todas las locaciones?")
                        .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                deleteAllLocation();
                            }
                        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).create().show();
            }
        });

    }

    public void showLocations(){
        locations.add("A01-A01");
        locations.add("A02-A02");
        locations.add("A03-A03");
        locations.add("A04-A04");
        locations.add("A05-A05");
        locations.add("A06-A06");
        locations.add("A07-A07");
        locations.add("A08-A08");
        locations.add("A09-A09");
        locations.add("A10-A10");
        locations.add("A11-A11");
        locations.add("A12-A12");
        locations.add("A13-A13");
        locations.add("A14-A14");
        locations.add("A15-A15");
        locations.add("A16-A16");
        locations.add("A17-A17");
        locations.add("A18-A18");
        locations.add("A19-A19");
        locations.add("A20-A20");

    }
    public void deleteAllLocation(){
        locations.removeAll(locations);
        locations.removeAll(locations_filters);
        adapter.notifyDataSetChanged();
    }
    public void removeLocation(int remove,Boolean searching){
        System.out.println(searching);
        System.out.println("postion: "+remove);
        if(searching){
            System.out.println("entra");
            locations_filters.remove(remove);
        }else{
            System.out.println("els");
            locations.remove(remove);
        }
        adapter.notifyDataSetChanged();
    }
}
