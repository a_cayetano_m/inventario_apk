package com.example.invfisico;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;


public class GlobalClass {
    Context mContext;

    /* USER */
    public static final String userId = "userId";
    public static final String user = "user";
    public static final String userName = "userName";
    public static final String versionApp = "1.6";
    public static final String userLastName = "userLastName";
    public static final String userEmail = "userEmail";
    public static final String userType = "userType";
    public static final String userSucursal = "userSucursal";
    public static final String userCodTienda = "userCodTienda";
    public static final String userDesTienda = "userDesTienda";
    public static final String userLocations = "userLocations";
    public static final String userAS = "userAS";
    public static final String userEAN = "userEAN";
    public static final String userFACTORESEAN = "userFACTORESEAN";
    public static final String userUNIDADEAN = "userUNIDADEAN";
    public static final String userZonas = "userZonas";
    public static final String userMuebles = "userMuebles";
    public static final String userNivel = "userNivel";
    public static final String userSeparacion = "userSeparacion";

    public static final String MaterialesLocation = "MaterialesLocation";
    public static final String IvnMaterialesLocation = "IvnMaterialesLocation";
    public static final String userNewLocations = "userNewLocations";
    public static final String userLocationsInv = "userLocationsInv";
    public static final String getKeyuserConteos = "getKeyuserConteos";
    /* NETWORK MESSAGE */
    public static final String notNetworkMessage = "Debe conectarse a internet";
    public static final String networkMessage = "Conectado a internet";
    public static  final String version_sistema = "1.6";

    /* CONFIRM MESSAGE */
    public static final String confirmMessage = "Agregado correctamente";
    /*TABLE SQLITE */
    public static final String Tbl_material = "t_material";
    public static final String Tbl_asig_locaciones = "t_asig_locaciones";
    public static final String Tbl_location = "t_location";
    public static final String Tbl_all_location = "t_all_location";
    public static final String Tbl_location_inv = "t_location_inv";
    public static final String Tbl_location_count = "t_location_count";
    public static final String Tbl_codigo_anterior = "t_cod_anterior";
    /* LOCATION */
    public static final String ListLocations = "ListLocations";
    /*Pre inventario*/
    public static final String Tbl_pre_inventario = "t_preinv_count";

    public static final String MyPREFERENCES = "MyPrefs";
    public final String codigo_anterior = "";

    // constructor
    public GlobalClass(Context context) {

        this.mContext = context;

    }

    public String getApiAuth(){
        return "Basic VVNSQVBJMDAxOmt5MDF0bDIk";
    }
    public String getUrlApi(){ return "https://www1.tailoy.com.pe/InvFisico/"; }
    public String getContentType(){ return "application/json"; }
    public String getTokenAuthApi(){ return "tailoy"; }

    public String getMaterialesLocation(){ return MaterialesLocation; }
    public String getIvnMaterialesLocation(){ return IvnMaterialesLocation; }
    public String getKeyuserNewLocations(){ return userNewLocations; }

    public String getKeyuserId(){ return userId; }
    public String getKeyuser(){ return user; }
    public String getKeyuserName(){ return userName; }
    public String getKeyversionApp() { return  versionApp;}
    public String getKeyuserLastName(){ return userLastName; }
    public String getKeyuserEmail(){ return userEmail; }
    public String getKeyuserType(){ return userType; }
    public String getKeyuserSucursal(){ return userSucursal; }
    public String getKeyuserCodTienda(){ return userCodTienda; }
    public String getKeyuserDesTienda(){ return userDesTienda; }
    public String getKeyuserZonas(){ return userZonas; }
    public String getKeyuserMuebles(){ return userMuebles; }
    public String getKeyuserNivel(){ return userNivel; }
    public String getKeyuserSeparacion(){ return userSeparacion; }
    public String getnotNetworkMessage(){ return notNetworkMessage; }
    public String getKnetworkMessage(){ return userLocations; }
    public String getKeyuserLocations(){ return userLocations; }
    public String getKeyuserAS(){ return userAS; }
    public String getKeyuserEAN(){ return userEAN; }
    public String getKeyuserFACTORESEAN(){ return userFACTORESEAN; }
    public String getKeyuseruserUNIDADEAN(){ return userUNIDADEAN; }
    public String getKeyuserLocationsInv(){ return userLocationsInv; }
    public String getKeyDATA(){ return "getKeyDATA"; }
    public String getKeyuserConteos(){ return getKeyuserConteos; }
    public String getTbl_material(){ return Tbl_material; }
    public String getTbl_asig_locaciones(){ return Tbl_asig_locaciones; }
    public String getTbl_location(){ return Tbl_location; }
    public String getTbl_all_location(){ return Tbl_all_location; }
    public String getTbl_location_inv(){ return Tbl_location_inv; }
    public String getTbl_location_count(){ return Tbl_location_count; }
    public String getcodigo_anterior(){ return codigo_anterior; }
    public String getconfirmMessage(){ return confirmMessage; }
    public String getTbl_codigo_anterior(){ return Tbl_codigo_anterior; }
    public String getTbl_pre_inventario() {return Tbl_pre_inventario; }
    public String getVersion_sistema() {return  version_sistema;}

    public boolean isNetworkConnected() { //Conexion a internet
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni == null) {
            return false;
        } else
            return true;
    }


}
