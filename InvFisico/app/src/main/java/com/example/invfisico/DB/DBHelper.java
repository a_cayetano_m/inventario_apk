package com.example.invfisico.DB;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DBHelper  extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION=5;
    private static final String DATABASE_NOMBRE="inv.db";
    private static final String TABLE_ASIGNACION_LOCACIONES="t_asig_locaciones";
    private static final String TABLE_MATERIAL="t_material";
    private static final String TABLE_LOCACIONES="t_location";
    private static final String TABLE_ALL_LOCACIONES="t_all_location";
    private static final String TABLE_LOCACIONES_INV="t_location_inv";
    private static final String TABLE_LOCACIONES_COUNT="t_location_count";
    private static final String TABLE_PREINVENT_COUNT="t_preinv_count";
    private static final String TABLE_ANTERIOR="t_cod_anterior";
    public DBHelper(@Nullable Context context) {
        super(context, DATABASE_NOMBRE, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE "+ TABLE_MATERIAL  +"(" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "locacion TEXT NOT NULL," +
                "descripcion TEXT NOT NULL," +
                "codean TEXT NOT NULL," +
                "factor TEXT NOT NULL," +
                "unidadbase TEXT NOT NULL," +
                "codas TEXT NOT NULL," +
                "indicador TEXT NOT NULL)");

        db.execSQL("CREATE TABLE "+ TABLE_ASIGNACION_LOCACIONES  +"(" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "locacion TEXT NOT NULL," +
                "descripcion TEXT NOT NULL," +
                "codean TEXT NOT NULL," +
                "codas TEXT NOT NULL," +
                "indicador TEXT NOT NULL)");

        db.execSQL("CREATE TABLE "+ TABLE_LOCACIONES  +"(" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "locacion TEXT NOT NULL)");

        db.execSQL("CREATE TABLE "+ TABLE_ALL_LOCACIONES  +"(" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "locacion TEXT NOT NULL)");

        db.execSQL("CREATE TABLE "+ TABLE_LOCACIONES_INV  +"(" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "locacion TEXT NOT NULL)");

        db.execSQL("CREATE TABLE "+ TABLE_LOCACIONES_COUNT  +"(" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "locacion TEXT NOT NULL," +
                "descripcion TEXT NOT NULL," +
                "codean TEXT NOT NULL," +
                "factor TEXT NOT NULL," +
                "unidadbase TEXT NOT NULL," +
                "codas TEXT NOT NULL," +
                "cantidad TEXT NOT NULL," +
                "indicador TEXT NOT NULL,"+
                "contado TEXT NOT NULL,"+
                "hora DATETIME default current_timestamp NOT NULL)");

        db.execSQL("CREATE TABLE "+ TABLE_ANTERIOR  +"(" +
                    "codas TEXT NOT NULL)");

        db.execSQL("CREATE TABLE "+TABLE_PREINVENT_COUNT+"(" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT,"+
                "locacion TEXT NOT NULL," +
                "cod_ean TEXT NOT NULL," +
                "cod_as TEXT NOT NULL,"+
                "description TEXT NOT NULL,"+
                "um_base TEXT,"+
                "cantidad INTEGER,"+
                "contado INTEGER DEFAULT 0,"+
                "hora DATETIME default current_timestamp NOT NULL"+
                ")");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+ TABLE_MATERIAL);
        db.execSQL("DROP TABLE IF EXISTS "+ TABLE_LOCACIONES);
        db.execSQL("DROP TABLE IF EXISTS "+ TABLE_LOCACIONES_COUNT);
        db.execSQL("DROP TABLE IF EXISTS "+ TABLE_ASIGNACION_LOCACIONES);
        db.execSQL("DROP TABLE IF EXISTS "+ TABLE_LOCACIONES);
        db.execSQL("DROP TABLE IF EXISTS "+ TABLE_ALL_LOCACIONES);
        db.execSQL("DROP TABLE IF EXISTS "+ TABLE_LOCACIONES_INV);
        db.execSQL("DROP TABLE IF EXISTS "+ TABLE_ANTERIOR);
        db.execSQL("DROP TABLE IF EXISTS "+ TABLE_PREINVENT_COUNT);
        onCreate(db);
    }
}
