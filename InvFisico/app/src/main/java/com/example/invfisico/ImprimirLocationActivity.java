package com.example.invfisico;

import android.annotation.SuppressLint;
import android.app.DownloadManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.URLUtil;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileOutputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ImprimirLocationActivity extends AppCompatActivity {
    private ListView LocationList;
    ArrayList<String> locations;
    ArrayList<String> locations_filters;
    ArrayAdapter<String> adapter;
    Button btn_imprimirAllLocation;
    Button btn_show_barcode_aux;
    Context context;

    @SuppressLint("MissingInflatedId")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imprimir_location);
        getSupportActionBar().hide();
        context=getApplicationContext();

        btn_imprimirAllLocation = findViewById(R.id.btn_imprimirAllLocation);
        LocationList = (ListView) findViewById(R.id.LocationList);
        locations = new ArrayList<String>();
        locations_filters = new ArrayList<String>();
        showLocations();

        adapter= new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,locations);

        LocationList.setAdapter(adapter);

        LocationList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                /* MOSTRAR VISTA COMO EN UN MODAL */
                LayoutInflater inflater = getLayoutInflater();
                final View v = inflater.inflate(R.layout.barcode, null);
                final AlertDialog.Builder builder =  new AlertDialog.Builder(ImprimirLocationActivity.this);
                builder.setView(v);
                final AlertDialog popud = builder.show();
                final ImageView icon_close = v.findViewById(R.id.icon_close); /* Initialice variables de vista llamada */
                final Button btn_show_barcode = v.findViewById(R.id.btn_show_barcode);
                btn_show_barcode_aux=btn_show_barcode;
                ImageView barcodeIMG = v.findViewById(R.id.barcodeIMG);
                TextView barcodeText= v.findViewById(R.id.barcodeText);
                icon_close.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        popud.dismiss();
                    }
                });
                btn_show_barcode.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v){

                        String valor_item=locations.get(position);
                        MultiFormatWriter multiFormatWriter= new MultiFormatWriter();
                        try {

                            BitMatrix bitMatrix = multiFormatWriter.encode(valor_item, BarcodeFormat.CODE_128,
                                    barcodeIMG.getWidth(),barcodeIMG.getHeight());
                            Bitmap bipmap = Bitmap.createBitmap(barcodeIMG.getWidth(),barcodeIMG.getHeight(), Bitmap.Config.RGB_565);
                            for (int i=0; i<barcodeIMG.getWidth(); i++){
                                for (int j=0; j<barcodeIMG.getHeight(); j++){
                                    bipmap.setPixel(i,j,bitMatrix.get(i,j)? Color.BLACK:Color.WHITE);
                                }
                            }
                            barcodeText.setText(valor_item);
                            barcodeIMG.setImageBitmap(bipmap);
                        }catch(WriterException e)  {
                            e.printStackTrace();
                        }
                    }
                });
                /*new AlertDialog.Builder(ImprimirLocationActivity.this)
                        .setTitle("¿Borrar "+valor_item+" ?")
                        .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                removeLocation(position, finalSearching);
                            }
                        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).create().show();*/
            }
        });

        btn_imprimirAllLocation.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                new AlertDialog.Builder(ImprimirLocationActivity.this)
                        .setTitle("¿Imprimir todas las locaciones?")
                        .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ImprimirAllLocation();
                            }
                        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).create().show();
            }
        });

    }

    public void showLocations(){
        locations.add("A01-A01");
        locations.add("A02-A02");
        locations.add("A03-A03");
        locations.add("A04-A04");
        locations.add("A05-A05");
        locations.add("A06-A06");
        locations.add("A07-A07");
        locations.add("A08-A08");
        locations.add("A09-A09");
        locations.add("A10-A10");
        locations.add("A11-A11");
        locations.add("A12-A12");
        locations.add("A13-A13");
        locations.add("A14-A14");
        locations.add("A15-A15");
        locations.add("A16-A16");
        locations.add("A17-A17");
        locations.add("A18-A18");
        locations.add("A19-A19");
        locations.add("A20-A20");

    }
    public void ImprimirAllLocation() {
        String mUrl= "https://www1.tailoy.com.pe/app/locaciones.pdf";
        DownloadManager.Request request=new DownloadManager.Request(Uri.parse(mUrl));
        String title = URLUtil.guessFileName(mUrl,null,null);
        request.setTitle(title);
        request.setDescription("Descangando archivo espere...");
        String cookie = CookieManager.getInstance().getCookie(mUrl);
        request.addRequestHeader("cookie",cookie);
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS,title);

        DownloadManager downloadManager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
        downloadManager.enqueue(request);

        Toast.makeText(ImprimirLocationActivity.this,"Iniciando descarga",Toast.LENGTH_SHORT).show();
    }

    /*public void ImprimirAllLocation3() {
        String mUrl= "https://www1.tailoy.com.pe/app/tailoy.pdf";
        InputStreamVolleyRequest request = new InputStreamVolleyRequest(Request.Method.GET, mUrl,
                response -> {
                    System.out.println(response);
                    try {
                        if (response!=null) {

                            FileOutputStream outputStream;
                            String name="tailoy.pdf";
                            outputStream = openFileOutput(name, Context.MODE_PRIVATE);
                            outputStream.write(response);
                            outputStream.close();
                            Toast.makeText(this, "Download complete.", Toast.LENGTH_LONG).show();

                            Intent intent=new Intent(this,ShowFileLocationActivity.class);
                            intent.putExtra("pdf",response);
                            startActivity(intent);

                            /*String filename = "myfile";
                            String fileContents = "Hello world!";

                            try (FileOutputStream fos = context.openFileOutput(filename, Context.MODE_PRIVATE)) {
                                fos.write(fileContents.toByteArray());
                            }

                        }
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        Log.d("KEY_ERROR", "UNABLE TO DOWNLOAD FILE");
                        e.printStackTrace();
                    }
                },new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO handle the error
                error.printStackTrace();
            }
        }, null);
        RequestQueue mRequestQueue = Volley.newRequestQueue(getApplicationContext(), new HurlStack());
        mRequestQueue.add(request);

    }*/

    public void ImprimirAllLocation2(){
        StringRequest request = new StringRequest(Request.Method.POST,
                "https://www1.tailoy.com.pe/app/descarga.php",
                response -> {
                    System.out.println(response);

                    /*JSONObject obj = null;
                    JSONObject obj_data = null;
                    String validate="";
                    try {
                        obj = new JSONObject(response);
                        JSONObject user = obj.getJSONObject("user");
                        validate = user.getString("validate");
                        String data = user.getString("data");
                        obj_data = new JSONObject(data);
                        String id_client=obj_data.getString("id");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }*/
                }, error -> { /**/ }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("param","parametro");
                return params;
            }
        };
        Volley.newRequestQueue(this).add(request);
    }
    public void removeLocation(int remove,Boolean searching){
        System.out.println(searching);
        System.out.println("postion: "+remove);
        if(searching){
            System.out.println("entra");
            locations_filters.remove(remove);
        }else{
            System.out.println("els");
            locations.remove(remove);
        }
        adapter.notifyDataSetChanged();
    }
}
