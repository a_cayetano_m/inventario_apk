package com.example.invfisico;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Toolbar;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.github.barteksc.pdfviewer.PDFView;

public class ShowFileLocationActivity extends AppCompatActivity {
    private PDFView pdfview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_showfile);
        byte[] pdf= getIntent().getByteArrayExtra("pdf");
        pdfview=findViewById(R.id.pdfView);
        pdfview.fromBytes(pdf).load();
       // init();
    }
    /*private void init(){
        Toolbar toolbar=this.findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(v -> {
            this.finish();
        });
    }*/
}
