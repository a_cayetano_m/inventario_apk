package com.example.invfisico;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.view.SoundEffectConstants;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.invfisico.DB.DBHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.sql.SQLOutput;
import java.util.HashMap;
import java.util.Map;

public class PrincipalActivity extends AppCompatActivity {

    Button btn_addLocation, btn_deleteLocation, btn_closeSession,btn_addMaterialLocation,btn_PreInventario;
    TextView userName;
    TextView versionApp;
    public static SharedPreferences sharedpreferences;

    public String Authorization="",UrlApi="",ContentType="";
    public GlobalClass global;
    public static final String MyPREFERENCES = "MyPrefs";
    public static final String GETLOCATIONS= "GETLOCATIONS";
    public static final String LoginStatus = "false";
    Handler handler;
    Runnable r;


    protected  void onResume() {

        super.onResume();
        System.out.println("eres un chucha");
        setContentView(R.layout.activity_principal);
        getSupportActionBar().hide();

        /*handler = new Handler();
        r = new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub
                Toast.makeText(getApplicationContext(), "user inactive",
                        Toast.LENGTH_SHORT).show();
                System.out.println("INACCCCTIVOOOOOOO");
            }
        };
        startHandler();*/

        Authorization=new GlobalClass(getApplicationContext()).getApiAuth();
        UrlApi=new GlobalClass(getApplicationContext()).getUrlApi();
        ContentType=new GlobalClass(getApplicationContext()).getContentType();
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        global = new GlobalClass(getApplicationContext());

        btn_addLocation = findViewById(R.id.btn_create_inventario);
        btn_deleteLocation = findViewById(R.id.btn_deleteLocation);
        //btn_imprimirlocaciones = findViewById(R.id.btn_imprimirlocaciones);
        btn_addMaterialLocation = findViewById(R.id.btn_addMaterialLocation);
        btn_PreInventario = findViewById(R.id.btn_PreInventario);
        btn_closeSession = findViewById(R.id.btn_closeSession);
        userName=findViewById(R.id.userName);
        userName.setText(sharedpreferences.getString(global.getKeyuser(), ""));
        versionApp = findViewById(R.id.versionApp);
        String version = "V"+global.getVersion_sistema().toString();
        versionApp.setText(version);
        System.out.println("Sistema->"+global.getVersion_sistema().toString());
        System.out.println("App->"+global.getKeyversionApp().toString());
        //Trae los datos que se pasaron por parametro a la vista
        //Bundle extras = getIntent().getExtras();
        getLocations();

        btn_addLocation.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                addLocation();
            }
        });
        btn_deleteLocation.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Location();
            }
        });
        /*btn_imprimirlocaciones.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                imprimirlocaciones();
            }
        });*/

        btn_addMaterialLocation.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                asignarMaterial();
            }
        });

        btn_PreInventario.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                asignarMaterialPreInventario();
            }
        });

        btn_closeSession.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                new AlertDialog.Builder(PrincipalActivity.this)
                        .setTitle("¿Cerrar sesión?")
                        .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                closeSession();
                            }
                        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).create().show();

            }
        });

    }
    /*@Override
    public void onUserInteraction() {
        // TODO Auto-generated method stub
        super.onUserInteraction();
        stopHandler();//stop first and then start
        startHandler();
    }
    public void stopHandler() {
        handler.removeCallbacks(r);
    }
    public void startHandler() {
        handler.postDelayed(r, 2000);
    }*/


    public void getLocations(){
        System.out.println("getLocations principalActivity");
        if(!global.isNetworkConnected()){
            Toast.makeText(getApplicationContext(), global.getnotNetworkMessage(), Toast.LENGTH_LONG).show();
        }else {
            DBHelper dbHelper = new DBHelper(PrincipalActivity.this);
            SQLiteDatabase db=dbHelper.getWritableDatabase();
            //db.execSQL("DELETE FROM "+global.getTbl_pre_inventario());
            String url=global.getUrlApi()+"showLocations/";
            System.out.println("response11");
            StringRequest request = new StringRequest(Request.Method.POST,
                    url,
                    response -> {
                        JSONObject obj = null;
                        try {
                            System.out.println("response");
                            System.out.println(response);
                            obj = new JSONObject(response);
                            if (obj.get("VALIDATE").toString().equals("1")) { // SI ES CORRECTO validate=1

                                db.execSQL("DELETE FROM "+global.getTbl_all_location());
                                String[] item= obj.get("LOCATIONS").toString().trim().split(",");
                                for (String l:item ) {
                                    ContentValues values = new ContentValues();
                                    values.put("locacion",l.trim());
                                    db.insert(global.getTbl_all_location(),null,values);
                                }

                            }else {
                                Toast.makeText(getApplicationContext(), obj.get("MSG").toString(), Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }, error -> {
            }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String,String> params = new HashMap<>();
                    params.put("SUCURSAL",sharedpreferences.getString(global.getKeyuserSucursal(), ""));
                    params.put("TOKEN", global.getTokenAuthApi());
                    System.out.println("params");
                    System.out.println(params);
                    return params;

                }
                /*@Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Content-Type", ContentType);
                    headers.put("Authorization", Authorization);
                    return headers;
                 }*/

            };
            Volley.newRequestQueue(getApplicationContext()).add(request);

        }

    }
    public void addLocation(){
        Intent intent = new Intent(this,AddLocationActivity.class);
        startActivity(intent);
    }
    public void Location(){
        Intent intent = new Intent(this,Locations.class);
        startActivity(intent);
    }

    public void imprimirlocaciones(){
        Intent intent = new Intent(this,ImprimirLocationActivity.class);
        startActivity(intent);
    }
    public void asignarMaterial(){
        Intent intent = new Intent(this,AsignarMaterialActivity.class);
        startActivity(intent);
    }

    public void asignarMaterialPreInventario(){
        Intent intent = new Intent(this,PreInventarioActivity.class);
        startActivity(intent);
        DeleteTbPreInv();
    }
    public void DeleteTbPreInv() {
        DBHelper dbHelper = new DBHelper(getApplicationContext());
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        //db.execSQL("DELETE FROM "+global.getTbl_pre_inventario());
    }
    public void DeleteTables(){
        DBHelper dbHelper = new DBHelper(getApplicationContext());
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.execSQL("DELETE FROM "+global.getTbl_all_location());
        db.execSQL("DELETE FROM "+global.getTbl_asig_locaciones());
        db.execSQL("DELETE FROM "+global.getTbl_location());
        db.execSQL("DELETE FROM "+global.getTbl_location_count());
        db.execSQL("DELETE FROM "+global.getTbl_location_inv());
        db.execSQL("DELETE FROM "+global.getTbl_material());
        //db.execSQL("DELETE FROM "+global.getTbl_pre_inventario());
    }
    public void closeSession(){
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putBoolean(LoginStatus, false);
        editor.commit();
        DeleteTables();
        Intent intent = new Intent(this,LoginActivity.class);
        startActivity(intent);
    }
}