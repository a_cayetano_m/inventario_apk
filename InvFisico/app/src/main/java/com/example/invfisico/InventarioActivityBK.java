package com.example.invfisico;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.invfisico.DB.DBHelper;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class InventarioActivityBK extends AppCompatActivity {

    public static SharedPreferences sharedpreferences;
    Button  btn_closeSession,btn_saveMaterial,btn_clean;
    public String Authorization="",UrlApi="",ContentType="";
    public GlobalClass global;
    public static final String MyPREFERENCES = "MyPrefs";
    ArrayAdapter<String> adapter;
    ArrayAdapter<String> adapterMissing;
    LinkedList<String> materiales = new LinkedList<String>();
    LinkedList<String> MatIngresado = new LinkedList<String>();
    LinkedList<String> materiales_send_as = new LinkedList<String>();
    LinkedList<String> materiales_send_ean = new LinkedList<String>();
    LinkedList<Integer> cantidad = new LinkedList<Integer>();
    LinkedList<String> locations = new LinkedList<String>();
    LinkedList<String> MatListMissing = new LinkedList<String>(); //Lista de materiales faltantes
    LinkedList<String> DesListMissing = new LinkedList<String>(); //Lista de  Descripcijones faltantes
    LinkedList<String> Missing = new LinkedList<String>(); //Lista de faltantes
    private ListView MaterialesList;
    LinkedList<String> locationsPreferer_general;
    EditText et_material, et_location;
    Spinner sp_locations;
    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventario);
        getSupportActionBar().hide();

        Authorization=new GlobalClass(getApplicationContext()).getApiAuth();
        UrlApi=new GlobalClass(getApplicationContext()).getUrlApi();
        ContentType=new GlobalClass(getApplicationContext()).getContentType();
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        global = new GlobalClass(getApplicationContext());

        MaterialesList = (ListView) findViewById(R.id.MaterialesList);

        et_material=findViewById(R.id.et_material);
        et_location=findViewById(R.id.et_location);

        btn_saveMaterial= (Button) findViewById(R.id.btn_saveMaterial);
        btn_clean= (Button) findViewById(R.id.btn_clean);

        Bundle extras = getIntent().getExtras();
        et_location.setText(extras.getString("LOCATION")); //SE OBTIENE EL DNI DEL ACTIVITY ANTERIOR

        et_location.setEnabled(false);
        et_material.requestFocus();

        Gson gson = new Gson();
        String materiales_send_eanPreferer=sharedpreferences.getString("materiales_send_ean_inv", "");
        String materiales_invPreferer=sharedpreferences.getString("materiales_inv", "");
        String materiales_send_asPreferer=sharedpreferences.getString("materiales_send_as_inv", "");
        String MatIngresadoPreferer=sharedpreferences.getString("MatIngresado_inv", "");
        String CantidadPreferer=sharedpreferences.getString("Cantidad_inv", "");

        String locationsPreferer=sharedpreferences.getString(global.getKeyuserLocationsInv(), "");

        LinkedList matsendean = gson.fromJson(materiales_send_eanPreferer, LinkedList.class);
        LinkedList mat = gson.fromJson(materiales_invPreferer, LinkedList.class);
        LinkedList matsendas = gson.fromJson(materiales_send_asPreferer, LinkedList.class);
        LinkedList mating = gson.fromJson(MatIngresadoPreferer, LinkedList.class);
        LinkedList cant = gson.fromJson(CantidadPreferer, LinkedList.class);

        locationsPreferer_general = new LinkedList<String>(Arrays.asList((String[])locationsPreferer.split(",")));
        if(locationsPreferer_general!=null) {
            locations=locationsPreferer_general;
        }
        if(matsendean!=null){
            materiales_send_ean=matsendean;
        }
        if(mat!=null){
            materiales=mat;
        }
        if(matsendas!=null){
            materiales_send_as=matsendas;
        }
        if(mating!=null){
            MatIngresado=mating;
        }
        if(cant!=null){
            cantidad=cant;
        }
        System.out.println("cantidad");
        System.out.println(cant);
        adapter= new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,materiales);
        MaterialesList.setAdapter(adapter);

        btn_saveMaterial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(InventarioActivityBK.this)
                        .setTitle("¿Guardar información de conteo?")
                        .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if(materiales.size()>0){
                                    sendConteo();
                                }else{
                                    Toast.makeText(getApplicationContext(),"Debe ingresar materiales",Toast.LENGTH_SHORT).show();
                                }
                                dialog.dismiss();
                            }
                        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                cleanFields();
                                cleanList();
                                dialog.dismiss();
                            }
                        }).create().show();
            }
        });
        btn_clean.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View vi) {
                new AlertDialog.Builder(InventarioActivityBK.this)
                        .setTitle("¿Ingresará nueva ubicación?")
                        .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                new AlertDialog.Builder(InventarioActivityBK.this)
                                        .setTitle("¿Guardar información de conteo?")
                                        .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if(materiales.size()>0){
                                                    sendConteo();
                                                }else{
                                                    Toast.makeText(getApplicationContext(),"Debe ingresar materiales",Toast.LENGTH_SHORT).show();
                                                }
                                                dialog.dismiss();
                                            }
                                        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                cleanFields();
                                                cleanList();
                                                dialog.dismiss();
                                            }
                                        }).create().show();

                            }
                        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).create().show();
            }
        });
        et_location.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                String loc_ipt=et_location.getText().toString();
                if(!loc_ipt.equals("")){
                    if(locations.indexOf(loc_ipt)==-1){ //SI NO PERTENCE A LA LISTA DE LOCACIONES
                        et_location.getText().clear();
                        Toast.makeText(getApplicationContext(),"Ubicación no habilitada",Toast.LENGTH_SHORT).show();
                    }else{
                        et_location.setEnabled(false);
                        et_material.setEnabled(true);
                        et_material.requestFocus();
                    }
                }

                return false;
            }
        });


        et_material.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {

                    String mat_ipt=et_material.getText().toString();
                    String loc_ipt=et_location.getText().toString();

                    if(!mat_ipt.equals("")){
                        addMaterial(mat_ipt,loc_ipt);
                        et_material.getText().clear();
                        et_material.requestFocus();
                    }else{
                        Toast.makeText(getApplicationContext(),"Debe ingresar un material",Toast.LENGTH_SHORT).show();
                        et_material.requestFocus();
                    }
                }
                return false;
            }

        });


        MaterialesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                LayoutInflater inflater = getLayoutInflater();
                final View v = inflater.inflate(R.layout.activity_inventario_det, null);
                final AlertDialog.Builder builder =  new AlertDialog.Builder(InventarioActivityBK.this);
                builder.setView(v);
                final ImageView icon_close = v.findViewById(R.id.icon_close); /* Initialice variables de vista llamada */
                final Button btn_delete_inv_item = v.findViewById(R.id.btn_delete_inv_item);
                final Button btn_save_inv_cantidad = v.findViewById(R.id.btn_save_inv_cantidad);
                final TextView title_inv_material = v.findViewById(R.id.title_inv_material);
                final TextView title_inv_location = v.findViewById(R.id.title_inv_location);
                final TextView title_inv_cantidad = v.findViewById(R.id.title_inv_cantidad);

                String[] item= materiales.get(position).toString().split("\\|");
                System.out.println(item);
                String mat=item[0].trim();
                String des=item[1].trim();
                String ca=item[2].split(":")[1].trim();

                title_inv_material.setText(mat);
                title_inv_location.setText(et_location.getText().toString().trim());
                title_inv_cantidad.setText(ca);

                final AlertDialog popud = builder.show();
                TextView et_cantidad_mod= v.findViewById(R.id.et_cantidad_mod);
                icon_close.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        popud.dismiss();
                    }
                });
                btn_save_inv_cantidad.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        String[] item= materiales.get(position).toString().split("C:");

                        Integer.parseInt(item[1].trim());
                        String b_item=item[0].trim();
                        String n_cantidad= String.valueOf(Integer.parseInt(et_cantidad_mod.getText().toString()));
                        materiales.remove(position);
                        materiales.add(position,b_item+" C:"+n_cantidad);
                        int p=MatIngresado.indexOf(mat);
                        System.out.println("posicion");
                        System.out.println(p);
                        if(p!=-1){ //Si existe
                            cantidad.set(p,Integer.parseInt(mat));
                        }
                        adapter.notifyDataSetChanged();
                        Toast.makeText(getApplicationContext(),"Se guardó correctamente",Toast.LENGTH_SHORT).show();
                        popud.dismiss();
                    }
                });
                btn_delete_inv_item.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        new AlertDialog.Builder(InventarioActivityBK.this)
                                .setTitle("¿Seguro de borrar?")
                                .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        removeLocation(position,mat);

                                        DBHelper dbHelper = new DBHelper(InventarioActivityBK.this);
                                        SQLiteDatabase db=dbHelper.getReadableDatabase();
                                        String query = "SELECT * FROM "+global.getTbl_material()+ " WHERE locacion='"+et_location.getText().toString()+"' and codas='"+mat+"'";
                                        Cursor c=(db.rawQuery(query,null));
                                        System.out.println("Todos los EAN");
                                        if(c.getCount()>0){
                                            if(c.moveToFirst()) {
                                                do{
                                                    String r_ean = c.getString(3).toString().trim();
                                                    String r_as = c.getString(6).toString().trim();
                                                    int p_ean=materiales_send_ean.indexOf(r_ean);
                                                    if(p_ean!=-1){ //Si existe
                                                        materiales_send_ean.remove(p_ean);
                                                    }else{
                                                        int p_as=materiales_send_as.indexOf(r_as);
                                                        if(p_as!=-1){ //Si existe
                                                            materiales_send_as.remove(p_as);
                                                        }
                                                    }
                                                }while (c.moveToNext());

                                            }
                                        }
                                        System.out.println("materiales_send_ean");
                                        System.out.println(materiales_send_ean);
                                        System.out.println("materiales_send_as");
                                        System.out.println(materiales_send_as);

                                        popud.dismiss();
                                        adapter.notifyDataSetChanged();
                                    }
                                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                }).create().show();

                    }
                });
            }
        });
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent = new Intent(getApplicationContext(),Principal2Activity.class);
        finish();
        startActivity(intent);

    }
    public void sendConteo(){
        if(!global.isNetworkConnected()){
            Toast.makeText(getApplicationContext(), global.getnotNetworkMessage(), Toast.LENGTH_LONG).show();
        }else{
            DBHelper dbHelper = new DBHelper(InventarioActivityBK.this);
            SQLiteDatabase db=dbHelper.getReadableDatabase();
            String query = "SELECT * FROM "+global.getTbl_material()+ " WHERE locacion='"+et_location.getText().toString()+"'";
            Cursor c=(db.rawQuery(query,null));

            if(c.getCount()>0) {
                if(c.moveToFirst()) {
                    do {
                        String r_descripcion = c.getString(2).toString().trim();
                        String r_as = c.getString(6).toString().trim();
                        System.out.println("MatIngresado");
                        System.out.println(MatIngresado);
                        System.out.println("Missing");
                        System.out.println(Missing);
                        System.out.println("MatListMissing");
                        System.out.println(MatListMissing);
                        System.out.println("materiales_send_ean");
                        System.out.println(materiales_send_ean);
                        System.out.println("materiales_send_as");
                        System.out.println(materiales_send_as);
                        if(!MatIngresado.contains(r_as) && !MatListMissing.contains(r_as) ){
                            MatListMissing.add(r_as);
                            DesListMissing.add(r_descripcion);
                            Missing.add(r_as+" | "+r_descripcion);
                        }
                    }while (c.moveToNext());
                    if(MatListMissing.size()>0){
                        /* MOSTRAR VISTA COMO EN UN MODAL */
                        LayoutInflater inflater = getLayoutInflater();
                        final View vi = inflater.inflate(R.layout.activity_mat_missing, null);
                        final AlertDialog.Builder builder =  new AlertDialog.Builder(InventarioActivityBK.this);
                        builder.setView(vi);
                        final ImageView icon_close_missing = vi.findViewById(R.id.icon_close_missing); /* Initialice variables de vista llamada */
                        final ListView ListMissing = (ListView) vi.findViewById(R.id.MissingListItem);
                        final AlertDialog popud = builder.show();
                        final TextView title_mat_missing= (TextView) vi.findViewById(R.id.title_mat_missing);
                        title_mat_missing.setText("MATERIALES FALTANTES "+et_location.getText().toString());
                        adapterMissing= new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,Missing);
                        ListMissing.setAdapter(adapterMissing);
                        icon_close_missing.setOnClickListener(new View.OnClickListener(){
                            @Override
                            public void onClick(View vi){
                                popud.dismiss();
                            }
                        });
                    }else{
                        DialogActivity dg = new DialogActivity(InventarioActivityBK.this);
                        dg.startLoadingDialog();


                        String url=global.getUrlApi()+"createFirstCont/";
                        StringRequest request = new StringRequest(Request.Method.POST,
                                url,
                                response -> {
                                    System.out.println("response");
                                    System.out.println(response);
                                    JSONObject obj = null;
                                    try {
                                        obj = new JSONObject(response);
                                        if (obj.get("VALIDATE").toString().equals("1")) { // SI ES CORRECTO validate=1
                                            Toast.makeText(getApplicationContext(),obj.get("MSG").toString(),Toast.LENGTH_SHORT).show();
                                            cleanList();
                                            et_material.setEnabled(false);
                                            et_location.getText().clear();
                                            et_location.requestFocus();
                                        }else{
                                            Toast.makeText(getApplicationContext(),obj.get("MSG").toString(),Toast.LENGTH_SHORT).show();
                                        }
                                        new android.os.Handler().postDelayed(
                                                new Runnable() {
                                                    public void run() {
                                                        dg.dismissDialog();
                                                        dg.startSuccessDialog();
                                                        new android.os.Handler().postDelayed(
                                                                new Runnable() {
                                                                    public void run() {
                                                                        dg.dismissDialog();
                                                                    }
                                                                }, 2000);

                                                    }
                                                }, 1000);

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }, error -> {
                        }){
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                Map<String,String> params = new HashMap<>();
                                String mat_send="";
                                String mat_send_cantidad="";
                                int j=0;
                                for(String mt: MatIngresado){
                                    if(j==0){
                                        mat_send=mt;
                                    }else{
                                        mat_send=mat_send+","+mt;
                                    }
                                    j++;
                                }
                                System.out.println("cantidad");
                                System.out.println(cantidad);
                                for(int i=0;i<cantidad.size();i++){
                                    if(i==0){
                                        mat_send_cantidad=String.valueOf(cantidad.get(i));
                                    }else{
                                        mat_send_cantidad=mat_send_cantidad+","+String.valueOf(cantidad.get(i));
                                    }
                                }
                                System.out.println("mat_send_cantidad");
                                System.out.println(mat_send_cantidad);
                                params.put("SUCURSAL",sharedpreferences.getString(global.getKeyuserSucursal(), ""));
                                params.put("MATERIAL",mat_send);
                                params.put("CANTIDAD",mat_send_cantidad);
                                params.put("LOCATION",et_location.getText().toString().trim());
                                params.put("TOKEN",global.getTokenAuthApi());
                                params.put("USERNAME",sharedpreferences.getString(global.getKeyuser(), ""));

                                return params;

                            }
                        };
                        Volley.newRequestQueue(getApplicationContext()).add(request);
                    }
                }

            }
        }
    }
    public void cleanList(){
        materiales.removeAll(materiales);
        materiales_send_ean.removeAll(materiales_send_ean);
        materiales_send_as.removeAll(materiales_send_as);
        cantidad.removeAll(cantidad);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.remove("materiales_send_ean_inv");
        editor.remove("materiales_inv");
        editor.remove("materiales_send_as_inv");
        editor.remove("MatIngresado_inv");
        editor.remove("Cantidad_inv");
        editor.apply();
        adapter.notifyDataSetChanged();
    }
    public void cleanFields(){
        et_material.setEnabled(false);
        et_location.setEnabled(true);
        et_location.requestFocus();
        et_location.getText().clear();
    }
    public void addMaterial(String material,String locacion){


        Gson gson = new Gson();

        DBHelper dbHelper = new DBHelper(InventarioActivityBK.this);
        SQLiteDatabase db=dbHelper.getReadableDatabase();
        String query = "SELECT * FROM "+global.getTbl_material()+ " WHERE codean='"+material+"' AND locacion='"+locacion+"' group by codean";
        Cursor c=(db.rawQuery(query,null));

        System.out.println("cantidad");
        System.out.println(c.getCount());
        if(c.getCount()>0){
            if(c.moveToFirst()) {
                do{
                    String r_descripcion = c.getString(2).trim();
                    String r_ean = c.getString(3).toString().trim();
                    String r_as = c.getString(6).toString().trim();
                    String r_factor = c.getString(4).toString().trim();
                    String r_unidad = c.getString(5).toString().trim();
                    System.out.println("materiales_send_ean");
                    System.out.println(materiales_send_ean);
                    if (materiales_send_ean.indexOf(material) == -1) {
                        if (MatIngresado.indexOf(r_as) == -1) {
                            materiales.addFirst(r_as + " | " + r_descripcion + " | C: "+r_factor);
                            materiales_send_ean.add(material);
                            MatIngresado.add(r_as.toString());
                            cantidad.add(Integer.parseInt(r_factor));
                            Toast.makeText(getApplicationContext(),global.getconfirmMessage(),Toast.LENGTH_LONG).show();
                        }else{
                            int i=0;
                            int ps=0;
                            boolean existe=false;
                            for (String m: materiales){
                                if(m.contains(r_as+" | "+r_descripcion)){
                                    ps=i;
                                    existe=true;
                                    break;
                                }
                                i++;
                            }
                            if(existe){
                                String[] valores= materiales.get(ps).split("C:");
                                String suma= String.valueOf(Integer.parseInt(r_factor)+Integer.parseInt(valores[1].trim()));
                                materiales.remove(ps);
                                materiales.add(ps,r_as+" | "+r_descripcion+" | C: "+suma);
                                materiales_send_ean.add(material);
                                int p=MatIngresado.indexOf(r_as);
                                if(p!=-1){ //Si existe
                                    cantidad.set(p,Integer.parseInt(suma));
                                }
                                Toast.makeText(getApplicationContext(),global.getconfirmMessage(),Toast.LENGTH_LONG).show();
                            }
                        }
                    }else{
                        int i=0;
                        int ps=0;
                        boolean existe=false;
                        for (String m: materiales){
                            if(m.contains(r_as+" | "+r_descripcion)){
                                ps=i;
                                existe=true;
                                break;
                            }
                            i++;
                        }
                        if(existe){
                            String[] valores= materiales.get(ps).split("C:");
                            String suma= String.valueOf(Integer.parseInt(r_factor)+Integer.parseInt(valores[1].trim()));
                            materiales.remove(ps);
                            materiales.add(ps,r_as+" | "+r_descripcion+" | C: "+suma);
                            int p=MatIngresado.indexOf(r_as);
                            if(p!=-1){ //Si existe
                                cantidad.set(p,Integer.parseInt(suma));
                            }
                            Toast.makeText(getApplicationContext(),global.getconfirmMessage(),Toast.LENGTH_LONG).show();
                        }
                    }

                }while (c.moveToNext());

            }
        }else{ // Sin EAN busca por AS
            query = "SELECT * FROM "+global.getTbl_material()+ " WHERE codas='"+material+"' AND locacion='"+locacion+"' ORDER BY factor ASC LIMIT 1";
            c=(db.rawQuery(query,null));
            System.out.println("No exite EAN");
            System.out.println(c.getCount());

            if(c.getCount()>0){
                if(c.moveToFirst()) {
                    do{
                        String r_descripcion = c.getString(2).trim();
                        String r_ean = c.getString(3).toString().trim();
                        String r_as = c.getString(6).toString().trim();
                        String r_factor = c.getString(4).toString().trim();
                        String r_unidad = c.getString(5).toString().trim();

                        if (MatIngresado.indexOf(r_as) == -1) {
                            materiales.addFirst(r_as + " | " + r_descripcion + " | C: "+r_factor);
                            materiales_send_as.add(material);
                            MatIngresado.add(r_as.toString());
                            cantidad.add(Integer.parseInt(r_factor));
                            Toast.makeText(getApplicationContext(),global.getconfirmMessage(),Toast.LENGTH_LONG).show();
                        }else{
                            int i=0;
                            int ps=0;
                            boolean existe=false;
                            for (String m: materiales){
                                if(m.contains(r_as+" | "+r_descripcion)){
                                    ps=i;
                                    existe=true;
                                    break;
                                }
                                i++;
                            }
                            if(existe){
                                String[] valores= materiales.get(ps).split("C:");
                                String suma= String.valueOf(Integer.parseInt(r_factor)+Integer.parseInt(valores[1].trim()));
                                materiales.remove(ps);
                                materiales.add(ps,r_as+" | "+r_descripcion+" | C: "+suma);
                                materiales_send_as.add(material);
                                int p=MatIngresado.indexOf(r_as);
                                if(p!=-1){ //Si existe
                                    cantidad.set(p,Integer.parseInt(suma));
                                }
                                Toast.makeText(getApplicationContext(),global.getconfirmMessage(),Toast.LENGTH_LONG).show();
                            }
                        }
                    }while (c.moveToNext());

                }
            }else{

                LayoutInflater inflater_nuevo = getLayoutInflater();
                final View vie = inflater_nuevo.inflate(R.layout.activity_inventario_det_nuevo, null);
                final AlertDialog.Builder builder =  new AlertDialog.Builder(InventarioActivityBK.this);
                builder.setView(vie);
                final ImageView icon_close = vie.findViewById(R.id.icon_close); /* Initialice variables de vista llamada */
                final Button btn_save_inv_cantidad_nuevo = vie.findViewById(R.id.btn_save_inv_cantidad_nuevo);
                final TextView title_det_nuevo = vie.findViewById(R.id.title_det_nuevo);
                final TextView title_inv_material_nuevo = vie.findViewById(R.id.title_inv_material_nuevo);
                final TextView title_inv_location_nuevo = vie.findViewById(R.id.title_inv_location_nuevo);

                title_det_nuevo.setText("MATERIAL NO ASIGNADO");
                title_inv_material_nuevo.setText(material);
                title_inv_location_nuevo.setText(locacion);
                TextView et_cantidad_mod_nuevo= vie.findViewById(R.id.et_cantidad_mod_nuevo);
                et_cantidad_mod_nuevo.requestFocus();
                final AlertDialog popud_nuevo = builder.show();

                btn_save_inv_cantidad_nuevo.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        String cantidad_nueva=et_cantidad_mod_nuevo.getText().toString();
                        if(!cantidad_nueva.equals("")){
                            materiales.addFirst(material +" | SIN DESCRIPCION | C: "+cantidad_nueva);
                            materiales_send_ean.add(material);
                            MatIngresado.add(material.toString());
                            cantidad.add(Integer.parseInt(cantidad_nueva));
                            adapter.notifyDataSetChanged();
                            Toast.makeText(getApplicationContext(),"Se guardó correctamente",Toast.LENGTH_SHORT).show();
                            popud_nuevo.dismiss();
                        }else{
                            Toast.makeText(getApplicationContext(),"Debe ingresar una cantidad",Toast.LENGTH_SHORT).show();
                        }

                    }
                });
                System.out.println("MatIngresado");
                System.out.println(MatIngresado);

            }

        }
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("materiales_send_ean_inv",gson.toJson(materiales_send_ean));
        editor.putString("materiales_inv",gson.toJson(materiales));
        editor.putString("materiales_send_as_inv",gson.toJson(materiales_send_as));
        editor.putString("MatIngresado_inv",gson.toJson(MatIngresado));
        editor.putString("Cantidad_inv",gson.toJson(cantidad));
        editor.commit();
        adapter.notifyDataSetChanged();
        c.close();
    }
    public void removeLocation(int position,String material){

        String[] item= materiales.get(position).toString().split("\\|");
        String mat=item[0].trim();
        String des=item[1].trim();
        String ca=item[2].split(":")[1].trim();
        int p=MatIngresado.indexOf(mat);
        if(p!=-1){ //Si existe
            MatIngresado.remove(p);
            cantidad.remove(p);
            materiales.remove(position);
        }

        Gson gson = new Gson();
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("materiales_inv",gson.toJson(materiales));
        editor.putString("MatIngresado_inv",gson.toJson(MatIngresado));
        editor.putString("Cantidad_inv",gson.toJson(cantidad));
        adapter.notifyDataSetChanged();


    }
}
