package com.example.invfisico;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class AsignarMaterialActivity2 extends AppCompatActivity {

    public static SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs";
    private ListView MaterialesList;
    EditText et_material, et_location;
    public String Authorization="",UrlApi="",ContentType="";

    public static final String ADDMATERIAL= "ADDMATERIAL";
    public GlobalClass global;
    Button btn_cleanTextView;
    View btn_saveMaterial;
    public ProgressBar progressBar;
    boolean locationNueva=false;
    LinkedList<String> materiales = new LinkedList<String>();
    LinkedList<String> materiales_send_as = new LinkedList<String>();
    LinkedList<String> materiales_send_ean = new LinkedList<String>();
    LinkedList<String> locations = new LinkedList<String>();
    LinkedList<String> locations_active = new LinkedList<String>();
    LinkedList<String> MatIngresado = new LinkedList<String>();
    LinkedList<String> locationsPreferer_general;
    LinkedList<String> materiales_aux = new LinkedList<String>();
    ArrayAdapter<String> adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asignarmaterial);
        getSupportActionBar().hide();
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        MaterialesList = (ListView) findViewById(R.id.MaterialesList);
        btn_cleanTextView = findViewById(R.id.btn_cleanTextView);
        btn_saveMaterial = findViewById(R.id.btn_saveMaterial);
        et_material=findViewById(R.id.et_material);
        et_location=findViewById(R.id.et_location);
        Authorization=new GlobalClass(getApplicationContext()).getApiAuth();
        UrlApi=new GlobalClass(getApplicationContext()).getUrlApi();
        ContentType=new GlobalClass(getApplicationContext()).getContentType();
        global = new GlobalClass(getApplicationContext());

        String locations_act = sharedpreferences.getString("locations_active", "");
        String matloc = sharedpreferences.getString(global.getMaterialesLocation(), "");
        String MaterialesPreferer_ean=sharedpreferences.getString("materiales_send_ean", "");
        String MaterialesPreferer_as=sharedpreferences.getString("materiales_send_as", "");
        String locationsPreferer=sharedpreferences.getString(global.getKeyuserLocations(), "");


        Gson gson = new Gson();
        String loc_active = locations_act;
        LinkedList matrerialLoc = gson.fromJson(matloc, LinkedList.class);
        LinkedList mat_ean = gson.fromJson(MaterialesPreferer_ean, LinkedList.class);
        LinkedList mat_as= gson.fromJson(MaterialesPreferer_as, LinkedList.class);

        locationsPreferer_general = new LinkedList<String>(Arrays.asList((String[])locationsPreferer.split(",")));
        if(locationsPreferer_general!=null) {
            locations=locationsPreferer_general;

        }

        if(loc_active!=null && loc_active!=""){
            et_location.setText(loc_active);
            et_location.setEnabled(false);
            et_material.setEnabled(true);
            et_material.requestFocus();
        }else{
            et_material.setEnabled(false);
            et_location.requestFocus();
        }
        if(matrerialLoc!=null){
            materiales=matrerialLoc;
        }
        if(mat_ean!=null){
            materiales_send_ean=mat_ean;
        }
        if(mat_as!=null){
            materiales_send_as=mat_as;
        }

        printLog();

        adapter= new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,materiales);
        MaterialesList.setAdapter(adapter);

        et_location.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                String loc_ipt=et_location.getText().toString();
                if(!loc_ipt.equals("")){
                    if(locations.indexOf(loc_ipt)==-1){ //SI NO PERTENCE A LA LISTA DE LOCACIONES
                        et_location.getText().clear();
                    }else{
                        //et_material.requestFocus();
                        et_location.setEnabled(false);
                        et_material.setEnabled(true);
                        et_material.requestFocus();
                        locations_active.add(loc_ipt);
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putString("locations_active",loc_ipt);
                        editor.commit();

                    }
                }
                return false;
            }
        });
        et_material.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                System.out.println("uusario");
                System.out.println(sharedpreferences.getString(global.getKeyuser(), ""));
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    System.out.println("ENTERRRR");

                    String mat_ipt = et_material.getText().toString();
                    String loc_ipt = et_location.getText().toString();
                    System.out.println(mat_ipt);
                    if (!mat_ipt.equals("")) {
                        addMaterial(mat_ipt);
                    }
                }
                return false;
            }

        });
        btn_saveMaterial.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if(materiales.size()>0 ){
                    new AlertDialog.Builder(AsignarMaterialActivity2.this)
                            .setTitle("¿ENVIAR MATERIALES ASIGNADOS?")
                            .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    saveAllMateriales();
                                }
                            }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).create().show();
                }else{
                    Toast.makeText(getApplicationContext(), "No hay materiales en lista", Toast.LENGTH_LONG).show();
                }
            }
        });

        MaterialesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                new AlertDialog.Builder(AsignarMaterialActivity2.this)
                        .setTitle("¿BORRAR "+materiales.get(position)+" ?")
                        .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                removeLocation(position,materiales.get(position));
                            }
                        }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).create().show();
            }
        });


        btn_cleanTextView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                new AlertDialog.Builder(AsignarMaterialActivity2.this)
                        .setTitle("¿NUEVA UBICACIÓN")
                        .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                et_material.getText().clear();
                                et_location.getText().clear();
                                et_location.setEnabled(true);
                                et_material.setEnabled(false);
                                et_location.requestFocus();
                                materiales.removeAll(materiales);
                                MatIngresado.removeAll(MatIngresado);
                                materiales_send_ean.removeAll(materiales_send_ean);
                                materiales_send_as.removeAll(materiales_send_as);
                                SharedPreferences.Editor editor = sharedpreferences.edit();
                                locations_active.removeAll(locations_active);
                                editor.remove("materiales_send_ean");
                                editor.remove("materiales_send_as");
                                editor.remove(global.getMaterialesLocation());
                                editor.remove("locations_active");
                                editor.apply();
                                printLog();
                                adapter.notifyDataSetChanged();
                            }
                        }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).create().show();

            }
        });


    }
    public void printLog(){
        System.out.println("VALORES");
        System.out.println("MatIngresado");
        System.out.println(MatIngresado);
        System.out.println("materiales");
        System.out.println(materiales);
        System.out.println("materiales_send_ean");
        System.out.println(materiales_send_ean);
        System.out.println("materiales_send_as");
        System.out.println(materiales_send_as);
    }
    public void saveAllMateriales(){
        if(materiales.size()>0){
            if(!global.isNetworkConnected()){
                Toast.makeText(getApplicationContext(), global.getnotNetworkMessage(), Toast.LENGTH_LONG).show();
            }else{
                DialogActivity dg = new DialogActivity(AsignarMaterialActivity2.this);
                dg.startLoadingDialog();
                String url=global.getUrlApi()+"saveLocationMaterial/";
                StringRequest request = new StringRequest(Request.Method.POST,
                        url,
                        response -> {
                            JSONObject obj = null;
                            System.out.println("respuesta enviar");
                            System.out.println(response);
                            try {
                                obj = new JSONObject(response);
                                System.out.println("response save all");
                                System.out.println(response);
                                if (obj.get("VALIDATE").toString().equals("1")) { // SI ES CORRECTO validate=1

                                    Gson gson = new Gson();
                                    locations_active.removeAll(locations_active);
                                    et_material.getText().clear();
                                    et_location.getText().clear();
                                    et_location.setEnabled(true);
                                    et_material.setEnabled(false);
                                    et_location.requestFocus();
                                    materiales.removeAll(materiales);
                                    SharedPreferences.Editor editor = sharedpreferences.edit();
                                    locations_active.removeAll(locations_active);
                                    MatIngresado.removeAll(MatIngresado);
                                    materiales_send_ean.removeAll(materiales_send_ean);
                                    materiales_send_as.removeAll(materiales_send_as);
                                    editor.remove("materiales_send_ean");
                                    editor.remove("materiales_send_as");
                                    editor.remove(global.getMaterialesLocation());
                                    editor.remove("locations_active");
                                    editor.apply();
                                    adapter.notifyDataSetChanged();
                                    Toast.makeText(AsignarMaterialActivity2.this,obj.get("MSG").toString(),Toast.LENGTH_SHORT).show();

                                }else{

                                    Toast.makeText(getApplicationContext(), obj.get("MSG").toString(), Toast.LENGTH_LONG).show();
                                }
                                new android.os.Handler().postDelayed(
                                        new Runnable() {
                                            public void run() {
                                                dg.dismissDialog();
                                                dg.startSuccessDialog();
                                                new android.os.Handler().postDelayed(
                                                        new Runnable() {
                                                            public void run() {
                                                                dg.dismissDialog();
                                                            }
                                                        }, 2000);

                                            }
                                        }, 1000);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }, error -> {
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String,String> params = new HashMap<>();
                        String mat_send_as="";
                        String mat_send_ean="";
                        int j=0;
                        int i=0;
                        for(String mt: materiales_send_as){
                            if(j==0){
                                mat_send_as=mt;
                            }else{
                                mat_send_as=mat_send_as+","+mt;
                            }
                            j++;
                        }
                        for(String mt: materiales_send_ean){
                            if(i==0){
                                mat_send_ean=mt;
                            }else{
                                mat_send_ean=mat_send_ean+","+mt;
                            }
                            i++;
                        }
                        params.put("SUCURSAL",sharedpreferences.getString(global.getKeyuserSucursal(), ""));
                        params.put("LOCATION",et_location.getText().toString());
                        params.put("MATERIAL_AS",mat_send_as);
                        params.put("MATERIAL_EAN",mat_send_ean);
                        params.put("TOKEN",global.getTokenAuthApi());
                        return params;

                    }
                /*@Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Content-Type", ContentType);
                    headers.put("Authorization", Authorization);
                    return headers;
                 }*/

                };
                Volley.newRequestQueue(getApplicationContext()).add(request);
            }

        }
    }


    public void addMaterial(String material){

        Gson gson = new Gson();
        if(!global.isNetworkConnected()){
            Toast.makeText(getApplicationContext(), global.getnotNetworkMessage(), Toast.LENGTH_LONG).show();
            materiales.addFirst(material +" | ");
            MatIngresado.add(material);
            materiales_send_ean.add(material);
            adapter.notifyDataSetChanged();
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putString("materiales_send_ean",gson.toJson(materiales_send_as));
            editor.putString(global.getMaterialesLocation(), gson.toJson(materiales));
            editor.commit();
            et_material.getText().clear();
            et_material.requestFocus();
        }else{
            if(MatIngresado.indexOf(material)==-1 ){
                String url=global.getUrlApi()+"showProduct/";
                StringRequest request = new StringRequest(Request.Method.POST,
                        url,
                        response -> {
                            System.out.println("RESPUEST");
                            System.out.println(response);
                            JSONObject obj = null;
                            try {
                                obj = new JSONObject(response);
                                if (obj.get("VALIDATE").toString().equals("1")) { // SI ES CORRECTO validate=1
                                    System.out.println(obj.get("DATA").toString());
                                    JSONObject data = new JSONObject(obj.get("DATA").toString());
                                    if(materiales_send_ean.indexOf(material)==-1){
                                        if(MatIngresado.indexOf(data.get("CODIGO_AS").toString())==-1){
                                            materiales.addFirst(data.get("CODIGO_AS").toString() +" | "+ data.get("DESCRIPCION").toString().trim() +"");
                                            MatIngresado.add(data.get("CODIGO_AS").toString());
                                            materiales_send_ean.add(material);
                                        }else{
                                            Toast.makeText(getApplicationContext(), data.get("CODIGO_AS").toString()+" ya fue escaneado antes ", Toast.LENGTH_LONG).show();
                                        }
                                    }else{
                                        Toast.makeText(getApplicationContext(), material+" ya fue escaneado antes ", Toast.LENGTH_LONG).show();
                                    }
                                    adapter.notifyDataSetChanged();
                                    et_material.getText().clear();
                                    et_material.requestFocus();

                                }else if(obj.get("VALIDATE").toString().equals("2")) {
                                    System.out.println("en validate 2");
                                    JSONObject data = new JSONObject(obj.get("DATA").toString());
                                    materiales.addFirst(data.get("CODIGO_AS").toString() +" | "+ data.get("DESCRIPCION").toString().trim() +"");
                                    if(materiales_send_as.indexOf(data.get("CODIGO_AS").toString())==-1){
                                        if(MatIngresado.indexOf(data.get("CODIGO_AS").toString())==-1){
                                            materiales.addFirst(data.get("CODIGO_AS").toString()+" | "+ data.get("DESCRIPCION").toString().trim() +"");
                                            MatIngresado.add(data.get("CODIGO_AS").toString());
                                            materiales_send_as.add(data.get("CODIGO_AS").toString());
                                        }else{
                                            Toast.makeText(getApplicationContext(), data.get("CODIGO_AS").toString()+" ya fue escaneado antes ", Toast.LENGTH_LONG).show();
                                        }
                                    }else{
                                        Toast.makeText(getApplicationContext(), data.get("CODIGO_AS").toString()+" ya fue escaneado antes ", Toast.LENGTH_LONG).show();
                                    }
                                    adapter.notifyDataSetChanged();
                                    et_material.getText().clear();
                                    et_material.requestFocus();
                                }else{



                                    if(materiales_send_as.indexOf(material)==-1){
                                        materiales.addFirst(material +" | ");
                                        MatIngresado.add(material);
                                        materiales_send_ean.add(material);
                                    }else{
                                        Toast.makeText(getApplicationContext(), material+" ya fue escaneado antes ", Toast.LENGTH_LONG).show();
                                    }
                                    adapter.notifyDataSetChanged();
                                    et_material.getText().clear();
                                    et_material.requestFocus();

                                }
                                SharedPreferences.Editor editor = sharedpreferences.edit();
                                editor.putString("materiales_send_as",gson.toJson(materiales_send_as));
                                editor.putString("materiales_send_ean",gson.toJson(materiales_send_ean));
                                editor.putString(global.getMaterialesLocation(), gson.toJson(materiales));
                                editor.commit();

                                System.out.println("MatIngresado");
                                System.out.println(MatIngresado);
                                System.out.println("materiales_send_as");
                                System.out.println(materiales_send_as);
                                System.out.println("materiales_send_ean");
                                System.out.println(materiales_send_ean);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }, error -> {
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String,String> params = new HashMap<>();
                        System.out.println("SUERNAMEEEE");
                        System.out.println(sharedpreferences.getString(global.getKeyuser(), ""));
                        params.put("SUCURSAL",sharedpreferences.getString(global.getKeyuserSucursal(), ""));
                        params.put("MATERIAL",material);
                        params.put("TOKEN",global.getTokenAuthApi());
                        params.put("USERNAME",sharedpreferences.getString(global.getKeyuser(), ""));

                        return params;

                    }
                /* @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Content-Type", "application/json");
                    return headers;
                 }*/
                };
                Volley.newRequestQueue(getApplicationContext()).add(request);

            }else{
                Toast.makeText(getApplicationContext(), material+" ya fue escaneado antes ", Toast.LENGTH_LONG).show();
                et_material.getText().clear();
                et_material.requestFocus();
            }

        }

    }

    public void removeLocation(int position,String value){
        String item_general[]=value.split("\\|"); //"EAN: 54545 | D: "
        String item_material[]=item_general[0].split(":");
        int pos_as=materiales_send_as.indexOf(value);
        int pos_ean=materiales_send_ean.indexOf(value);
        int pos_ingresado=MatIngresado.indexOf(item_material[0].trim());
        if(value.indexOf("EAN")!=-1){
            if (pos_ean!=-1){
                materiales_send_ean.remove(pos_ean);
            }
        }
        if(value.indexOf("AS")!=-1){
            if (pos_as!=-1){
                materiales_send_as.remove(pos_as);
            }
        }
        if(pos_ingresado !=-1){
            MatIngresado.remove(pos_ingresado);
        }
        materiales.remove(position);
        adapter.notifyDataSetChanged();
    }

}
