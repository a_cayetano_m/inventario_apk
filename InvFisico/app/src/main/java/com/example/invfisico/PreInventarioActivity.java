package com.example.invfisico;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.invfisico.DB.DBHelper;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class PreInventarioActivity extends AppCompatActivity {

    public static SharedPreferences sharedpreferences;
    Button  btn_closeSession,btn_saveMaterial;
    public String Authorization="",UrlApi="",ContentType="";
    public GlobalClass global;
    public static final String MyPREFERENCES = "MyPrefs";

    LinkedList<String> MatIngresado = new LinkedList<String>();
    LinkedList<String> materiales_send_as = new LinkedList<String>();
    LinkedList<String> materiales_send_ean = new LinkedList<String>();
    LinkedList<Integer> cantidad = new LinkedList<Integer>();
    LinkedList<String> MatListMissing = new LinkedList<String>(); //Lista de materiales faltantes
    LinkedList<String> DesListMissing = new LinkedList<String>(); //Lista de  Descripcijones faltantes
    LinkedList<String> Missing = new LinkedList<String>(); //Lista de faltantes


    LinkedList<String> materiales = new LinkedList<String>();
    LinkedList<String> locations = new LinkedList<String>();
    ArrayAdapter<String> adapter;
    ArrayAdapter<String> adapterMissing;

    private String location_active;
    private ListView MaterialesList;
    LinkedList<String> locationsPreferer_general;
    EditText et_material, et_location;
    TextView title_inv;
    public static String _cod_as_ = "";
    public static String _cod_ean_ = "";
    public static String _desc_ = "";
    public static String _umb_ = "";
    public static String _cant_ = "";
    public static String _fact_ = "";
    Spinner sp_locations;
    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preinventario);
        getSupportActionBar().hide();

        Authorization=new GlobalClass(getApplicationContext()).getApiAuth();
        UrlApi=new GlobalClass(getApplicationContext()).getUrlApi();
        ContentType=new GlobalClass(getApplicationContext()).getContentType();
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        global = new GlobalClass(getApplicationContext());

        MaterialesList = (ListView) findViewById(R.id.MaterialesList);
        String mat_ipt_desp = "";
        et_material=findViewById(R.id.et_material);
        et_location=findViewById(R.id.et_location);
        title_inv=  (TextView)findViewById(R.id.title_inv);

        btn_saveMaterial= (Button) findViewById(R.id.btn_saveMaterial);
        title_inv.setText("PRE-INVENTARIO");
        FocusLocation();
        DisableMaterial();
        /*et_location.setEnabled(false);
        et_material.requestFocus();*/

        Gson gson = new Gson();
        String materiales_send_eanPreferer=sharedpreferences.getString("materiales_send_ean_inv", "");
        String materiales_invPreferer=sharedpreferences.getString("materiales_inv", "");
        String materiales_send_asPreferer=sharedpreferences.getString("materiales_send_as_inv", "");
        String MatIngresadoPreferer=sharedpreferences.getString("MatIngresado_inv", "");
        String CantidadPreferer=sharedpreferences.getString("Cantidad_inv", "");



        LinkedList matsendean = gson.fromJson(materiales_send_eanPreferer, LinkedList.class);
        LinkedList matsendas = gson.fromJson(materiales_send_asPreferer, LinkedList.class);
        LinkedList mating = gson.fromJson(MatIngresadoPreferer, LinkedList.class);
        LinkedList cant = gson.fromJson(CantidadPreferer, LinkedList.class);

        getLocations();
        _getListMaterials();
        DBHelper dbHelper = new DBHelper(getApplicationContext());
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = "SELECT id,locacion FROM " + global.getTbl_pre_inventario()+ " limit 1";

        Cursor c = (db.rawQuery(query, null));
        String loca_cion = "";
        if (c.getCount() > 0) {
            if (c.moveToFirst()) {
                do {
                    loca_cion = c.getString(1).trim();
                    et_location.setText(loca_cion);
                    et_location.setEnabled(false);
                    EnableMaterial();
                } while (c.moveToNext());
            }
        }

        adapter= new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,materiales);
        MaterialesList.setAdapter(adapter);

        btn_saveMaterial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String location = et_location.getText().toString().trim();
                et_location.setText(location);
                if(location.equals("")){
                    Toast.makeText(getApplicationContext(),"Debe escanear la Locacion",Toast.LENGTH_SHORT).show();
                }else{
                    new AlertDialog.Builder(PreInventarioActivity.this)
                            .setTitle("¿GUARDAR CONTEO?")
                            .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {


                                    if(materiales.size()>0){
                                        sendConteo();

                                    }else{
                                        //Toast.makeText(getApplicationContext(),"Debe ingresar materiales",Toast.LENGTH_SHORT).show();
                                        //Se cambia el texto informativo por el combo en el que figura los codigos pendientes de contar
                                        sendConteo();
                                    }
                                    dialog.dismiss();
                                }
                            }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).create().show();
                }
            }
        });

        et_location.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    String loc_ipt = et_location.getText().toString().trim();
                    et_location.setText(loc_ipt);
                    if (!loc_ipt.equals("")) {

                        if(!global.isNetworkConnected()){
                            Toast.makeText(getApplicationContext(), global.getnotNetworkMessage(), Toast.LENGTH_LONG).show();
                        }else {
                            DialogActivity dg = new DialogActivity(PreInventarioActivity.this);
                            dg.startLoadingDialog();
                            String url=global.getUrlApi()+"validateLocations/";
                            StringRequest request = new StringRequest(Request.Method.POST,
                                    url,
                                    response -> {
                                        JSONObject obj = null;
                                        try {
                                            obj = new JSONObject(response);
                                            if (obj.get("VALIDATE").toString().equals("1")) { // SI ES CORRECTO validate=1
                                                et_location.setEnabled(false);
                                                EnableMaterial();

                                            }else {
                                                Toast.makeText(getApplicationContext(), obj.get("MSG").toString(), Toast.LENGTH_LONG).show();
                                                et_location.getText().clear();
                                                et_location.requestFocus();
                                            }
                                            new android.os.Handler().postDelayed(
                                                    new Runnable() {
                                                        public void run() {
                                                            dg.dismissDialog();
                                                        }
                                                    }, 1000);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }, error -> {
                            }){
                                @Override
                                protected Map<String, String> getParams() throws AuthFailureError {
                                    Map<String,String> params = new HashMap<>();
                                    params.put("SUCURSAL",sharedpreferences.getString(global.getKeyuserSucursal(), ""));
                                    params.put("TOKEN", global.getTokenAuthApi());
                                    params.put("VALUE", loc_ipt);
                                    return params;

                                }

                            };
                            Volley.newRequestQueue(getApplicationContext()).add(request);

                        }
                    }else{
                        EnableLocation();
                    }
                }
                return false;
            }
        });


        et_material.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    DBHelper dbHelper = new DBHelper(getApplicationContext());
                    SQLiteDatabase db = dbHelper.getReadableDatabase();

                    String mat_ipt = et_material.getText().toString();
                    String loc_ipt = et_location.getText().toString();

                    if(!mat_ipt.equals("")){
                        if(mat_ipt.length()<7) {
                            String material = mat_ipt.replaceFirst("^0+", "");

                            _addMaterial(loc_ipt,material);
                            et_material.getText().clear();
                        }else{
                            _addMaterial(loc_ipt,mat_ipt);
                            et_material.getText().clear();
                        }
                    }else{
                        Toast.makeText(getApplicationContext(),"Debe ingresar un material",Toast.LENGTH_SHORT).show();
                    }
                    et_material.requestFocus();
                    FocusMaterial();
                    FocusMaterial();
                    FocusMaterial();
                }
                return false;
            }

        });


        MaterialesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                _showDetails(position,materiales);
            }
        });
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent = new Intent(getApplicationContext(),PrincipalActivity.class);
        finish();
        startActivity(intent);

    }
    public void _showDetails(int position,LinkedList lista){
        LayoutInflater inflater = getLayoutInflater();
        final View v = inflater.inflate(R.layout.activity_pre_inventario_det, null);
        final AlertDialog.Builder builder =  new AlertDialog.Builder(PreInventarioActivity.this);
        builder.setView(v);
        final ImageView icon_close = v.findViewById(R.id.icon_close); /* Initialice variables de vista llamada */
        final Button btn_delete_inv_item = v.findViewById(R.id.btn_delete_inv_item);
        final Button btn_save_inv_cantidad = v.findViewById(R.id.btn_save_inv_cantidad);
        final TextView title_inv_material = v.findViewById(R.id.title_inv_material);
        final TextView title_inv_location = v.findViewById(R.id.title_inv_location);
        final TextView title_inv_cantidad = v.findViewById(R.id.title_inv_cantidad);
        final TextView title_inv_descripcion = v.findViewById(R.id.title_inv_descripcion);
        final TextView title_inv_umb = v.findViewById(R.id.title_inv_umb);

        String[] item= lista.get(position).toString().split("\\|");
        String mat=item[1].trim();
        String des=item[2].trim();
        String ca_n=item[3].trim();
        String umb = item[4].trim();
        String id=item[0].split(":")[1].trim();
        String ca="";
        if(item.length>2){
            ca=item[3].split(":")[1].trim();
        }else{
            ca="";
        }

        title_inv_material.setText(mat);
        title_inv_location.setText(et_location.getText().toString().trim());
        title_inv_cantidad.setText(ca);
        title_inv_descripcion.setText(des);
        title_inv_umb.setText(umb);

        final AlertDialog popud = builder.show();
        TextView et_cantidad_mod= v.findViewById(R.id.et_cantidad_mod);
        icon_close.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                popud.dismiss();
            }
        });
        btn_save_inv_cantidad.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                DBHelper dbHelper = new DBHelper(getApplicationContext());
                SQLiteDatabase db = dbHelper.getReadableDatabase();
                String itemSelected=adapter.getItem(position).toString().trim();
                String item[]=itemSelected.split("\\|"); //"EAN: 54545 | D: "
                String cod=item[1].trim();
                String query = "SELECT * FROM " + global.getTbl_pre_inventario()+ " where id = "+id;
                Cursor c = (db.rawQuery(query, null));
                DialogActivity dg = new DialogActivity(PreInventarioActivity.this);
                dg.startLoadingDialog();

                if (c.moveToFirst()) {
                    do {
                        String r_id = c.getString(0).trim();
                        String r_factor = c.getString(4).trim();
                        String r_cantidad = c.getString(6).toString().trim();
                        int cnt = Integer.parseInt(et_cantidad_mod.getText().toString().trim());

                        db.execSQL("UPDATE "+global.getTbl_pre_inventario()+" SET cantidad="+cnt+" WHERE id="+r_id);
                        break;
                    } while (c.moveToNext());
                }

                new android.os.Handler().postDelayed(
                        new Runnable() {
                            public void run() {
                                dg.dismissDialog();
                                _getListMaterials();
                                FocusMaterial();
                                KeyBoard();
                                popud.dismiss();
                            }
                        }, 0);
                Toast.makeText(getApplicationContext(),"Se guardó correctamente",Toast.LENGTH_SHORT).show();
                FocusMaterial();
                KeyBoard();
            }
        });
        btn_delete_inv_item.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                new AlertDialog.Builder(PreInventarioActivity.this)
                        .setTitle("¿Seguro de borrar?")
                        .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                _removeMaterial(mat,id);
                                Toast.makeText(getApplicationContext(), "Se borró correctamente", Toast.LENGTH_LONG).show();
                                _getListMaterials();
                                popud.dismiss();
                            }
                        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).create().show();
                _getListMaterials();
                adapter.notifyDataSetChanged();

            }

        });
    }
    public void _removeAllMaterial(){
        DBHelper dbHelper = new DBHelper(getApplicationContext());
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        db.execSQL("DELETE FROM "+global.getTbl_pre_inventario());
        materiales.removeAll(materiales);
        adapter.notifyDataSetChanged();
    }
    public  void _getDataPreInv() {
        DBHelper dbHelper = new DBHelper(getApplicationContext());
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = "SELECT id,locacion FROM " + global.getTbl_pre_inventario()+ " limit 1";

        Cursor c = (db.rawQuery(query, null));
        String loca_cion = "";
        if (c.getCount() > 0) {
            if (c.moveToFirst()) {
                do {
                    loca_cion = c.getString(1).trim();
                } while (c.moveToNext());
            }
        }

    }
    public void getLocations() {
        locations.removeAll(locations);
        DBHelper dbHelper = new DBHelper(getApplicationContext());
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = "SELECT id,locacion FROM " + global.getTbl_location_inv()+ " order by id desc";

        Cursor c = (db.rawQuery(query, null));
        if (c.getCount() > 0) {
            if (c.moveToFirst()) {
                do {
                    String r_id = c.getString(0).trim();
                    String r_loc = c.getString(1).trim();
                    locations.add(r_loc);
                } while (c.moveToNext());
            }
        }
    }
    public void KeyBoard(){
        View view= this.getCurrentFocus();
        if(view != null){
            InputMethodManager inn=(InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            inn.hideSoftInputFromWindow(view.getWindowToken(),0);
        }
    }
    public void FocusMaterial(){
        et_material.getText().clear();
        et_material.requestFocus();
        KeyBoard();
    }
    public void FocusLocation(){
        et_location.getText().clear();
        et_location.requestFocus();
    }
    public void DisableLocation(){
        et_location.setEnabled(false);
    }
    public void EnableLocation(){
        et_location.setEnabled(true);
        et_location.requestFocus();
    }
    public void DisableMaterial(){
        et_material.setEnabled(false);
    }
    public void EnableMaterial(){
        et_material.setEnabled(true);
        et_material.requestFocus();
    }

    public void sendConteo(){
        if(!global.isNetworkConnected()){
            Toast.makeText(getApplicationContext(), global.getnotNetworkMessage(), Toast.LENGTH_LONG).show();
        }else{
            DBHelper dbHelper = new DBHelper(PreInventarioActivity.this);
            SQLiteDatabase db=dbHelper.getReadableDatabase();
            String location_ = et_location.getText().toString();

            //si no encuentra codigos que no existe lo envia
            DialogActivity dg = new DialogActivity(PreInventarioActivity.this);
            dg.startLoadingDialog();
            String url=global.getUrlApi()+"createPreInventario/";
            StringRequest request = new StringRequest(Request.Method.POST,
                    url,
                    response -> {
                        JSONObject obj = null;
                        try {
                            JSONObject jsonObj = new JSONObject(response);
                            obj = new JSONObject(response);
                            if (obj.get("VALIDATE").toString().equals("1")) { // SI ES CORRECTO validate=1
                                Toast.makeText(getApplicationContext(),obj.get("MSG").toString(),Toast.LENGTH_SHORT).show();
                                db.execSQL("DELETE FROM "+global.getTbl_pre_inventario()+" WHERE locacion='"+location_+"'");
                                EnableLocation();
                                DisableMaterial();
                                FocusLocation();
                                _removeAllMaterial();
                            }else{
                                Toast.makeText(getApplicationContext(),obj.get("MSG").toString(),Toast.LENGTH_SHORT).show();
                            }
                            new android.os.Handler().postDelayed(
                                    new Runnable() {
                                        public void run() {
                                            dg.dismissDialog();
                                            dg.startSuccessDialog();
                                            new android.os.Handler().postDelayed(
                                                    new Runnable() {
                                                        public void run() {
                                                            dg.dismissDialog();
                                                        }
                                                    }, 1000);

                                        }
                                    }, 1000);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }, error -> {
            }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String,String> params = new HashMap<>();
                    params.put("SUCURSAL",sharedpreferences.getString(global.getKeyuserSucursal(), ""));
                    params.put("MATERIAL",getAllPreinventario());
                    params.put("LOCATION",et_location.getText().toString());
                    params.put("TOKEN",global.getTokenAuthApi());
                    params.put("USERNAME",sharedpreferences.getString(global.getKeyuser(), ""));

                    return params;

                }
            };
            Missing.removeAll(Missing);
            Volley.newRequestQueue(getApplicationContext()).add(request);

            if(Missing.size()>0){
                /* MOSTRAR VISTA COMO EN UN MODAL */
                LayoutInflater inflater = getLayoutInflater();
                final View vi = inflater.inflate(R.layout.activity_mat_missing, null);
                final AlertDialog.Builder builder =  new AlertDialog.Builder(PreInventarioActivity.this);
                builder.setView(vi);
                final ImageView icon_close_missing = vi.findViewById(R.id.icon_close_missing); /* Initialice variables de vista llamada */
                final ListView ListMissing = (ListView) vi.findViewById(R.id.MissingListItem);
                final AlertDialog popud = builder.show();
                final TextView title_mat_missing= (TextView) vi.findViewById(R.id.title_mat_missing);
                title_mat_missing.setText("MATERIALES FALTANTES "+et_location.getText().toString());
                adapterMissing= new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,Missing);
                ListMissing.setAdapter(adapterMissing);

                ListMissing.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        DBHelper dbHelper = new DBHelper(getApplicationContext());
                        SQLiteDatabase db = dbHelper.getReadableDatabase();
                        String itemSct = adapterMissing.getItem(position).toString().trim();
                        String itemS[] = itemSct.split("\\|"); //"EAN: 54545 | D: "
                        String as = itemS[0].trim();

                        showDetailsNewMaterial(et_location.getText().toString().trim(), as);
                        popud.dismiss();
                        KeyBoard();
                    }
                });


                icon_close_missing.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View vi){
                        popud.dismiss();
                    }
                });
            }
            //termina el else para envio
        }
    }
    public String getAllPreinventario() {
        DBHelper dbHelper = new DBHelper(getApplicationContext());
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String location_ = et_location.getText().toString();
        String query = "SELECT cod_as,sum(cantidad) FROM " + global.getTbl_pre_inventario()+" WHERE trim(locacion)=trim('"+location_+"')  group by cod_as order by hora desc";

        Cursor c = (db.rawQuery(query, null));
        ArrayList<String> list_as=new ArrayList<>();
        ArrayList<HashMap<String, String>> mate_riales = new ArrayList<HashMap<String, String>> ();
        HashMap<String, String> _detalle = new HashMap<String, String>();
        //coda,codan,cantidad
        int i=0;
        if (c.getCount() > 0) {
            if (c.moveToFirst()) {
                do {
                    _detalle.put("codas",c.getString(0).trim());
                    _detalle.put("cantidad",c.getString(1).trim());
                    mate_riales.add(_detalle);
                    _detalle.clear();
                    list_as.add(c.getString(0).trim()+","+c.getString(1).trim());
                } while (c.moveToNext());
            }
        }
        String result = TextUtils.join("~", list_as);
        return result;
    }

    public void _addMaterial(String locacion, String material) {
        Gson gson = new Gson();
        DialogActivity dg = new DialogActivity(PreInventarioActivity.this);
        dg.startLoadingDialog();
        DBHelper dbHelper = new DBHelper(PreInventarioActivity.this);
        SQLiteDatabase db=dbHelper.getReadableDatabase();
        String query="";
        String url=global.getUrlApi()+"ver_articulo/";
        System.out.println(url);
        StringRequest request = new StringRequest(Request.Method.POST,
                url,
                response -> {
                    JSONObject obj = null;
                    String json = response.replaceAll("\r\n","").replace("sql","").trim();
                    try {
                        obj = new JSONObject(json);

                        if (obj.get("VALIDATE").toString().equals("1")) { // SI ES CORRECTO validate=1
                            JSONArray array = new JSONArray(obj.get("RTA").toString());
                            JSONObject row = array.getJSONObject(0);
                            String R_cod_as = row.get("COD_AS").toString().trim();
                            String R_descripcion = row.get("DESCRIPCION").toString().trim();
                            String R_factor = row.get("FACTOR").toString().trim();
                            String R_unidad = row.get("UM_BASE").toString().trim();
                            String R_ean = row.get("COD_EAN").toString().trim();
                            _showDetailsNewMaterial(locacion, material, R_cod_as, R_ean, R_descripcion, R_unidad, R_factor);
                            _getListMaterials();
                            FocusMaterial();
                        }else{
                            Toast.makeText(getApplicationContext(),obj.get("Articulo no encontrado").toString(),Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> {
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("CODIGO", material);
                params.put("SUCURSAL",sharedpreferences.getString(global.getKeyuserSucursal(), ""));
                params.put("USERNAME",sharedpreferences.getString(global.getKeyuser(), ""));
                params.put("TOKEN",global.getTokenAuthApi());
                return params;
            }
        };
        Volley.newRequestQueue(getApplicationContext()).add(request);
    }
    public void _showDetailsNewMaterial(String loc, String mat, String cod, String cod_ean, String desc, String umb, String fac) {
        LayoutInflater inflater_nuevo = getLayoutInflater();
        final View vie = inflater_nuevo.inflate(R.layout.activity_inventario_det_nuevo, null);
        final AlertDialog.Builder builder =  new AlertDialog.Builder(PreInventarioActivity.this);
        builder.setView(vie);
        final ImageView icon_close_nuevo = vie.findViewById(R.id.icon_close); /* Initialice variables de vista llamada */
        final Button btn_save_inv_cantidad_nuevo = vie.findViewById(R.id.btn_save_inv_cantidad_nuevo);
        final TextView title_det_nuevo = vie.findViewById(R.id.title_det_nuevo);
        final TextView title_inv_material_nuevo = vie.findViewById(R.id.title_inv_material_nuevo);
        final TextView title_inv_location_nuevo = vie.findViewById(R.id.title_inv_location_nuevo);

        DBHelper dbHelper = new DBHelper(getApplicationContext());
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String sql = "SELECT cod_ean, count(id) cantidad FROM " + global.getTbl_pre_inventario() + " where locacion='" + loc + "' and cod_ean='"+cod_ean+"' group by cod_ean";
        Cursor c_n_ = (db.rawQuery(sql, null));
        Integer contador = 0;
        final int cont_reg_=c_n_.getCount();
        if(cont_reg_>0) {
            if (c_n_.moveToFirst()) {
                do {
                    contador = Integer.parseInt(c_n_.getString(1).trim());
                } while (c_n_.moveToNext());
            }
        }

        String query = "SELECT id, cod_as, cantidad, cod_ean FROM " + global.getTbl_pre_inventario() + " where locacion='" + loc + "' order by hora desc LIMIT 1";
        Cursor c_n = (db.rawQuery(query, null));

        int r_id_v=0;
        String r_as_v="";
        int r_cant = 0;

        final int cont_reg=c_n.getCount();
        if(cont_reg>0){
            if (c_n.moveToFirst()) {
                do {
                    r_id_v = Integer.parseInt(c_n.getString(0).trim());
                    r_as_v = c_n.getString(1).trim();
                    r_cant = Integer.parseInt(c_n.getString(2).trim());
                } while (c_n.moveToNext());
            }
        }
        title_det_nuevo.setText(desc);

        title_inv_material_nuevo.setText(mat);
        title_inv_location_nuevo.setText(loc);
        TextView et_cantidad_mod_nuevo= vie.findViewById(R.id.et_cantidad_mod_nuevo);
        final AlertDialog popud_nuevo = builder.show();

        String finalR_as_v = r_as_v;
        int finalR_cant = r_cant;
        int finalR_id = r_id_v;
        int finalR_contador = contador;
        btn_save_inv_cantidad_nuevo.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                int cantidad_nueva=Integer.parseInt(et_cantidad_mod_nuevo.getText().toString());
                if(cantidad_nueva>0){
                    DialogActivity dg = new DialogActivity(PreInventarioActivity.this);
                    dg.startLoadingDialog();
                    ContentValues values = new ContentValues();

                    if(finalR_as_v.equals(cod)){
                        cantidad_nueva = cantidad_nueva + finalR_cant;
                        db.execSQL("UPDATE "+global.getTbl_pre_inventario()+" SET cantidad="+cantidad_nueva+" WHERE id="+finalR_id);
                    }else{
                        values.put("locacion", loc);
                        values.put("cod_ean",cod_ean);
                        values.put("cod_as",cod);
                        values.put("description", desc);
                        values.put("um_base", umb);
                        values.put("cantidad",cantidad_nueva);
                        values.put("contado",finalR_contador);
                        values.put("hora",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
                        db.insert(global.getTbl_pre_inventario(),null,values);
                    }
                    new android.os.Handler().postDelayed(
                            new Runnable() {
                                public void run() {

                                    _getListMaterials();

                                }
                            }, 0);
                    popud_nuevo.dismiss();
                    dg.dismissDialog();
                }else{
                    Toast.makeText(getApplicationContext(),"Debe ingresar una cantidad",Toast.LENGTH_SHORT).show();
                }

            }
        });
        icon_close_nuevo.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View vi){
                popud_nuevo.dismiss();
            }
        });
    }
    public void showDetailsNewMaterial(String loc, String mat){
        LayoutInflater inflater_nuevo = getLayoutInflater();
        final View vie = inflater_nuevo.inflate(R.layout.activity_inventario_det_nuevo, null);
        final AlertDialog.Builder builder =  new AlertDialog.Builder(PreInventarioActivity.this);
        builder.setView(vie);
        final ImageView icon_close_nuevo = vie.findViewById(R.id.icon_close); /* Initialice variables de vista llamada */
        final Button btn_save_inv_cantidad_nuevo = vie.findViewById(R.id.btn_save_inv_cantidad_nuevo);
        final TextView title_det_nuevo = vie.findViewById(R.id.title_det_nuevo);
        final TextView title_inv_material_nuevo = vie.findViewById(R.id.title_inv_material_nuevo);
        final TextView title_inv_location_nuevo = vie.findViewById(R.id.title_inv_location_nuevo);


        DBHelper dbHelper = new DBHelper(getApplicationContext());
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = "SELECT * FROM " + global.getTbl_material() + " where codas='" + mat + "' and factor = 1 LIMIT 1";
        Cursor c_n = (db.rawQuery(query, null));

        String r_id_v="";
        String r_loc_v="";
        String r_descripcion_v="";
        String r_as_v="";
        String r_ean_v="";
        String r_factor_v="";
        String r_unidadbase_v="";
        final int cont_reg=c_n.getCount();
        if(cont_reg>0){
            title_det_nuevo.setText("MATERIAL FALTANTE");
            if (c_n.moveToFirst()) {
                do {
                    r_loc_v = c_n.getString(1).trim();
                    r_descripcion_v = c_n.getString(2).trim();
                    r_ean_v = c_n.getString(3).toString().trim();
                    r_factor_v = c_n.getString(4).toString().trim();
                    r_unidadbase_v = c_n.getString(5).toString().trim();
                    r_as_v = c_n.getString(6).toString().trim();
                } while (c_n.moveToNext());
            }
        }else{
            title_det_nuevo.setText("MATERIAL NO ASIGNADO");
        }

        title_inv_material_nuevo.setText(mat);
        title_inv_location_nuevo.setText(loc);
        TextView et_cantidad_mod_nuevo= vie.findViewById(R.id.et_cantidad_mod_nuevo);
        final AlertDialog popud_nuevo = builder.show();

        String finalR_loc_v = r_loc_v;
        String finalR_descripcion_v = r_descripcion_v;
        String finalR_ean_v = r_ean_v;
        String finalR_factor_v = r_factor_v;
        String finalR_unidadbase_v = r_unidadbase_v;
        String finalR_as_v = r_as_v;
        btn_save_inv_cantidad_nuevo.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                String cantidad_nueva=et_cantidad_mod_nuevo.getText().toString();
                if(!cantidad_nueva.equals("")){
                    DialogActivity dg = new DialogActivity(PreInventarioActivity.this);
                    dg.startLoadingDialog();
                    ContentValues values = new ContentValues();
                    if(cont_reg>0){
                        values.put("locacion", loc);
                        values.put("descripcion", finalR_descripcion_v);
                        values.put("codean", finalR_ean_v);
                        values.put("factor", finalR_factor_v);
                        values.put("unidadbase", finalR_unidadbase_v);
                        values.put("codas", finalR_as_v);
                        values.put("cantidad",cantidad_nueva);
                        values.put("indicador","1"); // Es uno de los que si estaban asignados
                        values.put("contado","");
                        values.put("hora",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
                        db.insert(global.getTbl_location_count(),null,values);
                    }else{
                        title_det_nuevo.setText("MATERIAL NO ASIGNADO");
                        if(mat.length()<=6){ //SI ES COD AS
                            values.put("codas",mat.trim());
                            values.put("codean","");
                        }else{
                            values.put("codean",mat.trim());
                            values.put("codas","");
                        }
                        values.put("locacion", loc);
                        values.put("descripcion", "");
                        values.put("factor", "");
                        values.put("unidadbase", "");
                        values.put("cantidad",cantidad_nueva);
                        values.put("descripcion","SIN DESCRIPCIÓN");
                        values.put("indicador","0"); // Es uno de los que no estaban asignados
                        values.put("contado","");
                        values.put("hora",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
                        db.insert(global.getTbl_location_count(),null,values);

                    }
                    dg.dismissDialog();
                    new android.os.Handler().postDelayed(
                            new Runnable() {
                                public void run() {
                                    getListMaterials();
                                }
                            }, 0);
                    popud_nuevo.dismiss();
                }else{
                    Toast.makeText(getApplicationContext(),"Debe ingresar una cantidad",Toast.LENGTH_SHORT).show();
                }

            }
        });
        icon_close_nuevo.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View vi){
                popud_nuevo.dismiss();
            }
        });


    }
    public void _getListMaterials() {
        materiales.removeAll(materiales);
        DBHelper dbHelper = new DBHelper(getApplicationContext());
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = "SELECT * FROM " + global.getTbl_pre_inventario()+ " order by hora desc";

        Cursor c = (db.rawQuery(query, null));
        String r_loc="";
        if (c.getCount() > 0) {
            if (c.moveToFirst()) {
                do {
                    String r_id = c.getString(0).trim();
                    String r_descripcion = c.getString(4).trim();
                    String r_unidad = c.getString(5).toString().trim();
                    String r_as = c.getString(3).toString().trim();
                    String r_cantidad = c.getString(6).toString().trim();
                    String r_contado = "";
                    if(Integer.parseInt(c.getString(7).toString().trim())==1) {
                        r_contado = "*";
                    }
                    if(materiales.indexOf(r_as + " | " + r_descripcion)==-1){
                        materiales.add("ID:"+r_id+" | "+r_as + " | " + r_descripcion + " | C:" + r_cantidad+" | " + r_unidad +" | " + r_contado);
                    }

                } while (c.moveToNext());
            }

           /* et_location.setText(r_loc);
            et_location.setEnabled(false);*/
            adapter= new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,materiales);
            MaterialesList.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }
    }
    public void getListMaterials() {
        materiales.removeAll(materiales);
        DBHelper dbHelper = new DBHelper(getApplicationContext());
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = "SELECT * FROM " + global.getTbl_location_count()+ " order by hora desc";

        Cursor c = (db.rawQuery(query, null));
        String r_loc="";
        if (c.getCount() > 0) {
            if (c.moveToFirst()) {
                do {
                    String r_id = c.getString(0).trim();
                    r_loc = c.getString(1).trim();
                    String r_descripcion = c.getString(2).trim();
                    String r_ean = c.getString(3).toString().trim();
                    String r_factor = c.getString(4).toString().trim();
                    String r_unidad = c.getString(5).toString().trim();
                    String r_as = c.getString(6).toString().trim();
                    String r_cantidad = c.getString(7).toString().trim();
                    String r_contado = c.getString(9).toString().trim();
                    if(r_as.equals("")){
                        if(materiales.indexOf(r_ean + " | " + r_descripcion)==-1){
                            materiales.add("ID:"+r_id+" | "+r_ean + " | " + r_descripcion + " | C:" + r_cantidad+" | " + r_unidad +" | " + r_contado);
                        }
                    }else{
                        if(materiales.indexOf(r_as + " | " + r_descripcion)==-1){
                            materiales.add("ID:"+r_id+" | "+r_as + " | " + r_descripcion + " | C:" + r_cantidad+" | " + r_unidad +" | " + r_contado);
                        }
                    }

                } while (c.moveToNext());
            }

           /* et_location.setText(r_loc);
            et_location.setEnabled(false);*/
            adapter= new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,materiales);
            MaterialesList.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }
    }
    public void removeMaterial(String cod,String id){
        DBHelper dbHelper = new DBHelper(getApplicationContext());
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        if(cod.length()>6){
            db.execSQL("DELETE FROM "+global.getTbl_location_count()+" where codean='"+cod+"' AND id = "+id);
        }else{
            db.execSQL("DELETE FROM "+global.getTbl_location_count()+" where codas='"+cod+"' AND id = "+id);
        }
        getListMaterials();
        adapter.notifyDataSetChanged();
    }
    public void _removeMaterial(String cod,String id){
        DBHelper dbHelper = new DBHelper(getApplicationContext());
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.execSQL("DELETE FROM "+global.getTbl_pre_inventario()+" where cod_as='"+cod+"' AND id = "+id);
        getListMaterials();
        adapter.notifyDataSetChanged();
    }
}
