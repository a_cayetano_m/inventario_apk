package com.example.invfisico;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.invfisico.DB.DBHelper;
import com.google.android.material.textview.MaterialTextView;
import com.google.gson.Gson;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.SQLOutput;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.zip.Inflater;

@SuppressWarnings("ALL")
public class Locations extends AppCompatActivity {

    SearchView SearchLocations;
    public String Authorization="",UrlApi="",ContentType="";
    LinkedList<String> locations = new LinkedList<String>();
    LinkedList<String> locations_items = new LinkedList<String>();
    private ListView LocationList;
    public GlobalClass global;
    private boolean change=false;
    ArrayAdapter adapter;
    ArrayAdapter adapter_det;
    private String loc=""; // diferencia si hay algun cambio
    Button btn_deleteAllLocation,btn_printLocationZonas;
    public static SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs";
    LinkedList<String> locationsPreferer_general;
    public static final String GETLOCATION= "GETLOCATION";
    public static final String ADDLOCATION= "ADDLOCATION";
    public static final String DELETELOCATION= "DELETELOCATION";
    public static final String UPDATELOCATION= "UPDATELOCATION";

    @SuppressLint("MissingInflatedId")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_locations);
        getSupportActionBar().hide();

        Authorization=new GlobalClass(getApplicationContext()).getApiAuth();
        UrlApi=new GlobalClass(getApplicationContext()).getUrlApi();
        ContentType=new GlobalClass(getApplicationContext()).getContentType();
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        global = new GlobalClass(getApplicationContext());

        btn_deleteAllLocation = findViewById(R.id.btn_deleteAllLocation);
        btn_printLocationZonas = findViewById(R.id.btn_printLocationZonas);
        SearchLocations=findViewById(R.id.search_locations);
        LocationList = (ListView) findViewById(R.id.LocationList);


        getListAllLocation();


        adapter= new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,locations);
        LocationList.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        btn_printLocationZonas.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                new AlertDialog.Builder(Locations.this)
                        .setTitle("¿IMPRIMIR UBICACIONES?")
                        .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                printLocationZonas();
                            }
                        }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).create().show();
            }
        });
        SearchLocations.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                adapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if(newText!=""){
                    System.out.println(newText.length());
                    System.out.println(newText);
                    if(newText.length()>=2){
                        getLocations(newText);
                    }
                    adapter.notifyDataSetChanged();
                }
                return false;
            }
        });
        LocationList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String location=(String) ((MaterialTextView)view).getText();

                if(!global.isNetworkConnected()){
                    Toast.makeText(getApplicationContext(), global.getnotNetworkMessage(), Toast.LENGTH_LONG).show();
                }else {
                    getDetailsLocation(location);
                }

            }
        });

    }
    public void getLocations(String search){
        if(!global.isNetworkConnected()){
            Toast.makeText(getApplicationContext(), global.getnotNetworkMessage(), Toast.LENGTH_LONG).show();
        }else {
            DBHelper dbHelper = new DBHelper(Locations.this);
            SQLiteDatabase db=dbHelper.getWritableDatabase();
            String url=global.getUrlApi()+"showLocations/";
            System.out.println("response11");
            System.out.println("search");
            System.out.println(search);
            StringRequest request = new StringRequest(Request.Method.POST,
                    url,
                    response -> {
                        JSONObject obj = null;
                        try {
                            System.out.println("response");
                            System.out.println(response);
                            obj = new JSONObject(response);
                            if (obj.get("VALIDATE").toString().equals("1")) { // SI ES CORRECTO validate=1
                                String[] item= obj.get("LOCATIONS").toString().trim().split(",");
                                locations.removeAll(locations);
                                for (String l:item ) {
                                    locations.add(l.trim());
                                    adapter= new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,locations);
                                    LocationList.setAdapter(adapter);
                                    adapter.notifyDataSetChanged();
                                }

                            }else {
                                Toast.makeText(getApplicationContext(), obj.get("MSG").toString(), Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }, error -> {
            }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String,String> params = new HashMap<>();
                    params.put("SUCURSAL",sharedpreferences.getString(global.getKeyuserSucursal(), ""));
                    params.put("TOKEN", global.getTokenAuthApi());
                    params.put("VALUE", search);
                    return params;

                }

            };
            Volley.newRequestQueue(getApplicationContext()).add(request);

        }

    }
    public void getListAllLocation() {
        locations.removeAll(locations);
        DBHelper dbHelper = new DBHelper(getApplicationContext());
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = "SELECT id,locacion FROM " + global.getTbl_all_location()+ " order by id asc";
        Cursor c = (db.rawQuery(query, null));
        System.out.println(c.getCount());
        if (c.getCount() > 0) {
            if (c.moveToFirst()) {
                do {
                    String r_id = c.getString(0).trim();
                    String r_loc = c.getString(1).trim();
                    locations.add(r_loc);

                } while (c.moveToNext());
            }
            adapter= new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,locations);
            LocationList.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }
    }
    public void getDetailsLocation(String loc){
        /* MOSTRAR VISTA COMO EN UN MODAL */
        LayoutInflater inflater = getLayoutInflater();
        final View v = inflater.inflate(R.layout.activity_locations_det, null);
        final AlertDialog.Builder builder =  new AlertDialog.Builder(Locations.this);
        builder.setView(v);
        final ProgressBar progressBarLoadata= v.findViewById(R.id.progressBarLoadata);
        progressBarLoadata.setVisibility(View.VISIBLE);
        final AlertDialog popud = builder.show();
        final ImageView icon_close_det = v.findViewById(R.id.icon_close_det); /* Initialice variables de vista llamada */
        final Button btn_deleteContent = v.findViewById(R.id.btn_deleteContent); /* Initialice variables de vista llamada */
        final Button btn_deleteLoc = v.findViewById(R.id.btn_deleteLoc); /* Initialice variables de vista llamada */

        final Button btn_printLocation = v.findViewById(R.id.btn_printLocation);
        final TextView title_locacion = v.findViewById(R.id.title_locacion);

        final ListView LocationListItem_det= (ListView) v.findViewById(R.id.LocationListItem_det);
        title_locacion.setText(loc);

        icon_close_det.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View vi){
                popud.dismiss();
            }
        });
        btn_printLocation.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View vi){
                new AlertDialog.Builder(Locations.this)
                        .setTitle("¿IMPRIMIR "+loc+"?")
                        .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                printLocation(loc);
                            }
                        }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).create().show();
            }
        });
        btn_deleteContent.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                new AlertDialog.Builder(Locations.this)
                        .setTitle("¿BORRAR CONTENIDO DE "+loc+"?")
                        .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if(locations_items.size()>0){
                                    deleteContentLocation(loc);
                                }else{
                                    Toast.makeText(getApplicationContext(),"No hay productos asignados",Toast.LENGTH_SHORT).show();
                                }
                            }
                        }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).create().show();
            }
        });
        btn_deleteLoc.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                new AlertDialog.Builder(Locations.this)
                        .setTitle("¿BORRAR UBICACION "+loc+"?")
                        .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                deleteAllLocation(loc);
                            }
                        }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).create().show();
            }
        });
        LocationListItem_det.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(!global.isNetworkConnected()){
                    Toast.makeText(getApplicationContext(), global.getnotNetworkMessage(), Toast.LENGTH_LONG).show();
                }else {

                    String val_list=LocationListItem_det.getAdapter().getItem(position).toString();
                    String[] val = val_list.split(" \\| ");
                    new AlertDialog.Builder(Locations.this)
                            .setTitle("¿BORRAR "+val[0].trim()+"?")
                            .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    deleteLocation(loc,val[0].trim());
                                    popud.dismiss();
                                }
                            }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).create().show();
                }

            }
        });
        String url=global.getUrlApi()+"showProductSucursal/";
        StringRequest request = new StringRequest(Request.Method.POST,
                url,
                response -> {
                    System.out.println("response");
                    System.out.println(response);
                    JSONObject obj = null;
                    try {
                        obj = new JSONObject(response);
                        if (obj.get("VALIDATE").toString().equals("1")) { // SI ES CORRECTO validate=1
                            JSONArray array = new JSONArray(obj.get("DATA").toString());
                            locations_items.removeAll(locations_items);
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject row = array.getJSONObject(i);
                                String val=row.get("CODIGO_AS")+" | "+row.get("DESCRIPCION");
                                if(locations_items.indexOf(val)==-1){
                                    locations_items.add(row.get("CODIGO_AS")+" | "+row.get("DESCRIPCION"));
                                }

                            }

                            adapter_det= new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,locations_items);
                            LocationListItem_det.setAdapter(adapter_det);
                            adapter_det.notifyDataSetChanged();
                        }else{

                        }
                        progressBarLoadata.setVisibility(View.GONE);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> {
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("SUCURSAL",sharedpreferences.getString(global.getKeyuserSucursal(), ""));
                params.put("TOKEN",global.getTokenAuthApi());
                params.put("LOCACION",loc);
                return params;

            }
        };
        Volley.newRequestQueue(getApplicationContext()).add(request);
    }
    public void printLocation(String Loc){
        String url=global.getUrlApi()+"printLocation/";
        StringRequest request = new StringRequest(Request.Method.POST,
                url,
                response -> {
                    System.out.println("asdasd");
                    System.out.println(response);
                    JSONObject obj = null;
                    try {
                        obj = new JSONObject(response);
                        if (obj.get("VALIDATE").toString().equals("1")) { // SI ES CORRECTO validate=1

                            Toast.makeText(getApplicationContext(),obj.get("MSG").toString(),Toast.LENGTH_SHORT).show();

                        }else{

                            Toast.makeText(getApplicationContext(), obj.get("MSG").toString(), Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> {
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("SUCURSAL",sharedpreferences.getString(global.getKeyuserSucursal(), ""));
                params.put("LOCATION",Loc);
                params.put("TOKEN",global.getTokenAuthApi());
                return params;

            }
        };
        Volley.newRequestQueue(getApplicationContext()).add(request);
    }
    public void printLocationZonas() {

        int count_adapter=LocationList.getAdapter().getCount();
        if(count_adapter>0){
            DialogActivity dg = new DialogActivity(Locations.this);
            dg.startLoadingDialog();
            String print_loc="";
            for (int i=0;i<count_adapter;i++){
                if(i==0){
                    print_loc=LocationList.getAdapter().getItem(i).toString();
                }else{
                    print_loc=print_loc+","+LocationList.getAdapter().getItem(i).toString();
                }
            }
            String url=global.getUrlApi()+"printZoneLocation/";
            String finalPrint_loc = print_loc;
            StringRequest request = new StringRequest(Request.Method.POST,
                    url,
                    response -> {
                        JSONObject obj = null;
                        try {
                            obj = new JSONObject(response);
                            if (obj.get("VALIDATE").toString().equals("1")) { // SI ES CORRECTO validate=1

                                Toast.makeText(getApplicationContext(),obj.get("MSG").toString(),Toast.LENGTH_SHORT).show();

                            }else{

                                Toast.makeText(getApplicationContext(), obj.get("MSG").toString(), Toast.LENGTH_LONG).show();
                            }
                            new android.os.Handler().postDelayed(
                                    new Runnable() {
                                        public void run() {
                                            dg.dismissDialog();
                                            dg.startSuccessDialog();
                                            new android.os.Handler().postDelayed(
                                                    new Runnable() {
                                                        public void run() {
                                                            dg.dismissDialog();
                                                        }
                                                    }, 2000);

                                        }
                                    }, 1000);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }, error -> {
            }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String,String> params = new HashMap<>();
                    params.put("SUCURSAL",sharedpreferences.getString(global.getKeyuserSucursal(), ""));
                    params.put("LOCATION", finalPrint_loc);
                    params.put("TOKEN",global.getTokenAuthApi());
                    return params;

                }
            };
            Volley.newRequestQueue(getApplicationContext()).add(request);
        }

    }
    public void printLocationZonas2(){
        String url=global.getUrlApi()+"printLocation/";
        StringRequest request = new StringRequest(Request.Method.POST,
                url,
                response -> {
                    JSONObject obj = null;
                    try {
                        obj = new JSONObject(response);
                        if (obj.get("VALIDATE").toString().equals("1")) { // SI ES CORRECTO validate=1

                            Toast.makeText(getApplicationContext(),obj.get("MSG").toString(),Toast.LENGTH_SHORT).show();

                        }else{

                            Toast.makeText(getApplicationContext(), obj.get("MSG").toString(), Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> {
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("SUCURSAL",sharedpreferences.getString(global.getKeyuserSucursal(), ""));
                params.put("LOCATION","");
                params.put("TOKEN",global.getTokenAuthApi());
                return params;

            }
        };
        Volley.newRequestQueue(getApplicationContext()).add(request);
    }
    public void deleteContentLocation(String loc){
        DialogActivity dg = new DialogActivity(Locations.this);
        dg.startLoadingDialog();
        String url=global.getUrlApi()+"deleteContentLocation/";
        StringRequest request = new StringRequest(Request.Method.POST,
                url,
                response -> {
                    JSONObject obj = null;
                    System.out.println("respuesta deleteContentLocation");
                    System.out.println(response);
                    try {
                        obj = new JSONObject(response);
                        if (obj.get("VALIDATE").toString().equals("1")) { // SI ES CORRECTO validate=1
                            Toast.makeText(getApplicationContext(),obj.get("MSG").toString(),Toast.LENGTH_SHORT).show();
                            new android.os.Handler().postDelayed(
                                    new Runnable() {
                                        public void run() {
                                            dg.dismissDialog();
                                            Intent intent = new Intent(getApplicationContext(),Locations.class);
                                            startActivity(intent);
                                        }
                                    }, 1000);

                        }else{

                            Toast.makeText(getApplicationContext(), obj.get("MSG").toString(), Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> {
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("SUCURSAL",sharedpreferences.getString(global.getKeyuserSucursal(), ""));
                params.put("LOCATION",loc);
                params.put("CODTIENDA",sharedpreferences.getString(global.getKeyuserCodTienda(), ""));
                params.put("TOKEN",global.getTokenAuthApi());
                return params;

            }
        };
        Volley.newRequestQueue(getApplicationContext()).add(request);
    }
    public void removeLocation(String location){
        DBHelper dbHelper = new DBHelper(getApplicationContext());
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        db.execSQL("DELETE FROM "+global.getTbl_all_location()+" where locacion='"+location+"'");
    }
    public void deleteAllLocation(String loc){
        String url=global.getUrlApi()+"deleteLocation/";
        StringRequest request = new StringRequest(Request.Method.POST,
                url,
                response -> {
                    JSONObject obj = null;
                    System.out.println("respuesta deleteLocation");
                    System.out.println(response);
                    try {
                        obj = new JSONObject(response);
                        if (obj.get("VALIDATE").toString().equals("1")) { // SI ES CORRECTO validate=1
                            Toast.makeText(getApplicationContext(),obj.get("MSG").toString(),Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(this,PrincipalActivity.class);
                            startActivity(intent);
                        }else{

                            Toast.makeText(getApplicationContext(), obj.get("MSG").toString(), Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> {
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("SUCURSAL",sharedpreferences.getString(global.getKeyuserSucursal(), ""));
                params.put("LOCATION",loc);
                params.put("CODTIENDA",sharedpreferences.getString(global.getKeyuserCodTienda(), ""));
                params.put("TOKEN",global.getTokenAuthApi());
                return params;

            }
        };
        Volley.newRequestQueue(getApplicationContext()).add(request);
    }

    public void deleteLocation(String loc,String val){
        String url=global.getUrlApi()+"deleteLocationUnidad/";
        StringRequest request = new StringRequest(Request.Method.POST,
                url,
                response -> {
                    JSONObject obj = null;
                    System.out.println("respuesta deleteLocationUnidad");
                    System.out.println(response);
                    try {
                        obj = new JSONObject(response);
                        if (obj.get("VALIDATE").toString().equals("1")) { // SI ES CORRECTO validate=1
                            Toast.makeText(getApplicationContext(),obj.get("MSG").toString(),Toast.LENGTH_SHORT).show();
                        }else{

                            Toast.makeText(getApplicationContext(), obj.get("MSG").toString(), Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> {
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("SUCURSAL",sharedpreferences.getString(global.getKeyuserSucursal(), ""));
                params.put("LOCATION",loc);
                params.put("AS",val);
                params.put("CODTIENDA",sharedpreferences.getString(global.getKeyuserCodTienda(), ""));
                params.put("TOKEN",global.getTokenAuthApi());
                return params;

            }
        };
        Volley.newRequestQueue(getApplicationContext()).add(request);
    }


}
