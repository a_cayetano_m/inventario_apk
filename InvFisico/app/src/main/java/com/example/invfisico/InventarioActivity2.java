package com.example.invfisico;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.invfisico.DB.DBHelper;
import com.google.gson.Gson;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class InventarioActivity2 extends AppCompatActivity {

    public static SharedPreferences sharedpreferences;
    Button btn_create_inventario, btn_closeSession,btn_clean;
    public String Authorization="",UrlApi="",ContentType="";
    public GlobalClass global;
    public static final String MyPREFERENCES = "MyPrefs";
    ArrayAdapter<String> adapter;
    ArrayAdapter<String> adapterList;
    LinkedList<String> sl_materiales= new LinkedList<String>();
    LinkedList<String> materiales = new LinkedList<String>();
    LinkedList<String> MatIngresado = new LinkedList<String>();
    LinkedList<String> materiales_send_as = new LinkedList<String>();
    LinkedList<String> materiales_send_ean = new LinkedList<String>();
    LinkedList<Integer> cantidad = new LinkedList<Integer>();
    LinkedList<String> locations = new LinkedList<String>();
    LinkedList<String> sp_materiales_locations = new LinkedList<String>();
    LinkedList<String> locationsPreferer_general;
    private ListView MaterialesList2;
    EditText et_material,et_location;
    TextView title_loc_inv2;
    Spinner sp_materiales;
    String loc_title;
    TextView userName;
    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventario2);
        getSupportActionBar().hide();

        Authorization=new GlobalClass(getApplicationContext()).getApiAuth();
        UrlApi=new GlobalClass(getApplicationContext()).getUrlApi();
        ContentType=new GlobalClass(getApplicationContext()).getContentType();
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        global = new GlobalClass(getApplicationContext());
        btn_clean= (Button) findViewById(R.id.btn_clean_inv2);
        sp_materiales=(Spinner) findViewById(R.id.et_mat);
        et_material=findViewById(R.id.et_material);
        et_location=findViewById(R.id.et_location);
        title_loc_inv2=findViewById(R.id.title_loc_inv2);

        MaterialesList2 = (ListView) findViewById(R.id.MaterialesList2);

        Gson gson = new Gson();
        String materiales_send_eanPreferer=sharedpreferences.getString("materiales_send_ean_inv", "");
        String materiales_invPreferer=sharedpreferences.getString("materiales_inv", "");
        String materiales_send_asPreferer=sharedpreferences.getString("materiales_send_as_inv", "");
        String MatIngresadoPreferer=sharedpreferences.getString("MatIngresado_inv", "");
        String CantidadPreferer=sharedpreferences.getString("Cantidad_inv", "");
        String locationsPreferer=sharedpreferences.getString("Locacion_inv", "");
        String list_materiales_cont2Preferer=sharedpreferences.getString("list_materiales_cont2", "");
        String sp_materiales_locations2Preferer=sharedpreferences.getString("sp_materiales_locations2", "");


        LinkedList matsendean = gson.fromJson(materiales_send_eanPreferer, LinkedList.class);
        LinkedList mat = gson.fromJson(materiales_invPreferer, LinkedList.class);
        LinkedList matsendas = gson.fromJson(materiales_send_asPreferer, LinkedList.class);
        LinkedList mating = gson.fromJson(MatIngresadoPreferer, LinkedList.class);
        LinkedList cant = gson.fromJson(CantidadPreferer, LinkedList.class);
        LinkedList loc = gson.fromJson(locationsPreferer, LinkedList.class);

        LinkedList lmc = gson.fromJson(list_materiales_cont2Preferer, LinkedList.class);
        LinkedList lmcloc = gson.fromJson(sp_materiales_locations2Preferer, LinkedList.class);
        if(lmc!=null) {
            sl_materiales=lmc;
            adapter= new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item,sl_materiales);
            sp_materiales.setAdapter(adapter);
            if(lmcloc!=null){
                sp_materiales_locations=lmcloc;
            }
            System.out.println("SIN GETAMTERAL");
        }else{
            System.out.println("CON MATERIAL");
            getMateriales();
        }
        if(loc!=null) {
            locations=loc;
        }
        if(matsendean!=null){
            materiales_send_ean=matsendean;
        }
        if(mat!=null){
            materiales=mat;
        }
        if(matsendas!=null){
            materiales_send_as=matsendas;
        }
        if(mating!=null){
            MatIngresado=mating;
        }
        if(cant!=null){
            cantidad=cant;
        }
        adapterList= new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,materiales);
        MaterialesList2.setAdapter(adapterList);

        et_location.requestFocus();
        et_material.setEnabled(false);

        userName=findViewById(R.id.userName);
        System.out.println(userName);
        if(userName!=null){
            userName.setText(sharedpreferences.getString(global.getKeyuser(), ""));
        }

        sp_materiales.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("sp_materiales_locations");
                System.out.println(sp_materiales_locations);
                if(sp_materiales_locations.size()>0){
                    loc_title=sp_materiales_locations.get(position).toString();
                    title_loc_inv2.setText(sp_materiales_locations.get(position).toString());
                    et_material.requestFocus();
                }

            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Nada fue seleccionado. Por cierto, no he visto que este método se desencadene
            }
        });
        btn_clean.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View vi) {
                new AlertDialog.Builder(InventarioActivity2.this)
                        .setTitle("¿BORRAR MATERIALES CONTADOS?")
                        .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                cleanList();
                            }
                        }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).create().show();
            }
        });

        et_location.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                System.out.println("et_location");
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {

                    String location_ipt=et_location.getText().toString();
                    String loc_ipt=loc_title;
                    System.out.println("location_ipt");
                    System.out.println(location_ipt);
                    System.out.println("loc_ipt");
                    System.out.println(loc_ipt);

                    if(location_ipt.equals(loc_ipt)){
                        et_material.requestFocus();
                        et_material.setEnabled(true);
                        et_location.setEnabled(false);
                    }else{
                        Toast.makeText(getApplicationContext(),"Ubicación incorrecta",Toast.LENGTH_SHORT).show();

                    }
                }
                return false;
            }

        });


        et_material.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                System.out.println("et_material");
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {

                    String mat_ipt=et_material.getText().toString();
                    String loc_ipt=loc_title;

                    if(!mat_ipt.equals("")){

                        addMaterial(mat_ipt,loc_ipt);
                        et_material.getText().clear();
                        et_material.requestFocus();
                    }else{
                        Toast.makeText(getApplicationContext(),"Debe ingresar un material",Toast.LENGTH_SHORT).show();
                        et_material.requestFocus();
                    }
                }
                return false;
            }

        });
        MaterialesList2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                LayoutInflater inflater = getLayoutInflater();
                final View v = inflater.inflate(R.layout.activity_inventario_det, null);
                final AlertDialog.Builder builder =  new AlertDialog.Builder(InventarioActivity2.this);
                builder.setView(v);
                final ImageView icon_close = v.findViewById(R.id.icon_close); /* Initialice variables de vista llamada */
                final Button btn_delete_inv_item = v.findViewById(R.id.btn_delete_inv_item);
                final Button btn_save_inv_cantidad = v.findViewById(R.id.btn_saveMaterial);
                final TextView title_inv_material = v.findViewById(R.id.title_inv_material);
                final TextView title_inv_location = v.findViewById(R.id.title_inv_location);
                final TextView title_inv_cantidad = v.findViewById(R.id.title_inv_cantidad);

                String[] item= materiales.get(position).toString().split("\\|");
                System.out.println("item");
                System.out.println(item);
                String mat=item[0].trim();
                String des=item[1].trim();
                String ca=item[2].split(":")[1].trim();
                System.out.println("item");
                System.out.println(item);

                title_inv_material.setText(mat);
                int posi_det=MatIngresado.indexOf(mat);
                System.out.println("locations");
                System.out.println(locations);
                title_inv_location.setText(locations.get(posi_det).toString().trim());
                title_inv_cantidad.setText(ca);

                final AlertDialog popud = builder.show();
                TextView et_cantidad_mod= v.findViewById(R.id.et_cantidad_mod);
                icon_close.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        popud.dismiss();
                    }
                });
                btn_save_inv_cantidad.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        String[] item= materiales.get(position).toString().split("C:");

                        Integer.parseInt(item[1].trim());
                        String b_item=item[0].trim();
                        String n_cantidad= String.valueOf(Integer.parseInt(et_cantidad_mod.getText().toString()));
                        materiales.remove(position);
                        materiales.add(position,b_item+" C:"+n_cantidad);
                        int p=MatIngresado.indexOf(mat);
                        if(p!=-1){ //Si existe
                            cantidad.set(p,Integer.parseInt(mat));
                        }
                        adapterList.notifyDataSetChanged();
                        Toast.makeText(getApplicationContext(),"Se guardó correctamente",Toast.LENGTH_SHORT).show();
                        popud.dismiss();
                    }
                });
                btn_delete_inv_item.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        new AlertDialog.Builder(InventarioActivity2.this)
                                .setTitle("¿SEGURO DE BORRAR?")
                                .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        cleanList();
                                        popud.dismiss();
                                        adapter.notifyDataSetChanged();
                                    }
                                }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                }).create().show();

                    }
                });
            }
        });
    }
    public void cleanList(){
        if(materiales!=null){
            materiales.removeAll(materiales);
        }
        if(locations!=null){
            locations.removeAll(locations);
        }
        if(materiales_send_ean!=null){
            materiales_send_ean.removeAll(materiales_send_ean);
        }
        if(materiales_send_as!=null){
            materiales_send_as.removeAll(materiales_send_as);
        }
        if(cantidad!=null){
            cantidad.removeAll(cantidad);
        }

        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.remove("materiales_send_ean_inv");
        editor.remove("materiales_inv");
        editor.remove("materiales_send_as_inv");
        editor.remove("MatIngresado_inv");
        editor.remove("Cantidad_inv");
        editor.remove("Locacion_inv");
        editor.remove("sp_materiales_locations2");
        editor.remove("list_materiales_cont2");
        editor.apply();
        et_location.getText().clear();
        et_location.requestFocus();
        et_location.setEnabled(true);
        et_material.setEnabled(false);

        adapterList.notifyDataSetChanged();
    }
    public void addMaterial(String material,String locacion){


        Gson gson = new Gson();

        DBHelper dbHelper = new DBHelper(InventarioActivity2.this);
        SQLiteDatabase db=dbHelper.getReadableDatabase();
        String query = "SELECT * FROM "+global.getTbl_material()+ " WHERE codean='"+material+"' AND locacion='"+locacion+"'";
        System.out.println("query");
        System.out.println(query);
        Cursor c=(db.rawQuery(query,null));

        if(c.getCount()>0){
            if(c.moveToFirst()) {
                do{
                    String r_descripcion = c.getString(2).trim();
                    String r_ean = c.getString(3).toString().trim();
                    String r_as = c.getString(6).toString().trim();
                    String r_factor = c.getString(4).toString().trim();
                    String r_unidad = c.getString(5).toString().trim();

                    String concat=r_as+" | "+r_descripcion;

                    if(concat.equals(sp_materiales.getSelectedItem().toString())){
                        if (materiales_send_ean.indexOf(material) == -1) {
                            if (MatIngresado.indexOf(r_as) == -1) {
                                materiales.addFirst(r_as + " | " + r_descripcion + " | C: "+r_factor);
                                materiales_send_ean.add(material);
                                MatIngresado.add(r_as.toString());
                                cantidad.add(Integer.parseInt(r_factor));
                                locations.add(title_loc_inv2.getText().toString().trim());
                                Toast.makeText(getApplicationContext(),global.getconfirmMessage(),Toast.LENGTH_LONG).show();
                            }else{
                                int i=0;
                                int ps=0;
                                boolean existe=false;
                                for (String m: materiales){
                                    if(m.contains(r_as+" | "+r_descripcion)){
                                        ps=i;
                                        existe=true;
                                        break;
                                    }
                                    i++;
                                }
                                if(existe){
                                    String[] valores= materiales.get(ps).split("C:");
                                    String suma= String.valueOf(Integer.parseInt(r_factor)+Integer.parseInt(valores[1].trim()));
                                    materiales.remove(ps);
                                    materiales.add(ps,r_as+" | "+r_descripcion+" | C: "+suma);
                                    materiales_send_ean.add(material);
                                    int p=MatIngresado.indexOf(r_as);
                                    if(p!=-1){ //Si existe
                                        cantidad.set(p,Integer.parseInt(suma));
                                    }
                                    Toast.makeText(getApplicationContext(),global.getconfirmMessage(),Toast.LENGTH_LONG).show();
                                }
                            }
                        }else{
                            int i=0;
                            int ps=0;
                            boolean existe=false;
                            for (String m: materiales){
                                if(m.contains(r_as+" | "+r_descripcion)){
                                    ps=i;
                                    existe=true;
                                    break;
                                }
                                i++;
                            }
                            if(existe){
                                String[] valores= materiales.get(ps).split("C:");
                                String suma= String.valueOf(Integer.parseInt(r_factor)+Integer.parseInt(valores[1].trim()));
                                materiales.remove(ps);
                                materiales.add(ps,r_as+" | "+r_descripcion+" | C: "+suma);
                                int p=MatIngresado.indexOf(r_as);
                                if(p!=-1){ //Si existe
                                    cantidad.set(p,Integer.parseInt(suma));
                                }
                                Toast.makeText(getApplicationContext(),global.getconfirmMessage(),Toast.LENGTH_LONG).show();
                            }
                        }
                        List<Integer> indices = IntStream.range(0, sl_materiales.size())
                                .filter(i -> Objects.equals(sl_materiales.get(i), concat))
                                .boxed().collect(Collectors.toList());
                        System.out.println("INDICES");
                        System.out.println(indices);
                        int idx=0;
                        for ( Integer ind : indices){
                            if(sp_materiales_locations.get(ind).equals(locacion)){
                                idx=ind;
                                break;
                            }
                        }
                        sl_materiales.remove(idx);
                        sp_materiales_locations.remove(idx);
                        adapter= new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item,sl_materiales);
                        sp_materiales.setAdapter(adapter);

                    }else{
                        Toast.makeText(getApplicationContext(),"No es el material seleccionado",Toast.LENGTH_LONG).show();
                        et_material.requestFocus();
                    }

                    adapterList.notifyDataSetChanged();
                }while (c.moveToNext());

            }
        }else{ // Sin EAN busca por AS
            query = "SELECT * FROM "+global.getTbl_material()+ " WHERE codas='"+material+"' AND locacion='"+locacion+"' ORDER BY factor ASC LIMIT 1";
            c=(db.rawQuery(query,null));
            System.out.println("No exite EAN");
            System.out.println(c.getCount());
            if(c.moveToFirst()) {
                do{
                    String r_descripcion = c.getString(2).trim();
                    String r_ean = c.getString(3).toString().trim();
                    String r_as = c.getString(6).toString().trim();
                    String r_factor = c.getString(4).toString().trim();
                    String r_unidad = c.getString(5).toString().trim();

                    if (MatIngresado.indexOf(r_as) == -1) {
                        materiales.addFirst(r_as + " | " + r_descripcion + " | C: "+r_factor);
                        materiales_send_as.add(material);
                        MatIngresado.add(r_as.toString());
                        cantidad.add(Integer.parseInt(r_factor));
                        locations.add(title_loc_inv2.getText().toString().trim());
                        Toast.makeText(getApplicationContext(),global.getconfirmMessage(),Toast.LENGTH_LONG).show();
                    }else{
                        int i=0;
                        int ps=0;
                        boolean existe=false;
                        for (String m: materiales){
                            if(m.contains(r_as+" | "+r_descripcion)){
                                ps=i;
                                existe=true;
                                break;
                            }
                            i++;
                        }
                        if(existe){
                            String[] valores= materiales.get(ps).split("C:");
                            String suma= String.valueOf(Integer.parseInt(r_factor)+Integer.parseInt(valores[1].trim()));
                            materiales.remove(ps);
                            materiales.add(ps,r_as+" | "+r_descripcion+" | C: "+suma);
                            materiales_send_as.add(material);
                            locations.add(title_loc_inv2.getText().toString().trim());
                            int p=MatIngresado.indexOf(r_as);
                            if(p!=-1){ //Si existe
                                cantidad.set(p,Integer.parseInt(suma));
                            }
                            Toast.makeText(getApplicationContext(),global.getconfirmMessage(),Toast.LENGTH_LONG).show();
                        }
                    }
                }while (c.moveToNext());

            }
        }
        System.out.println("MatIngresado");
        System.out.println(MatIngresado);
        System.out.println("cantidad");
        System.out.println(cantidad);
        System.out.println("locacion");
        System.out.println(locations);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("materiales_send_ean_inv",gson.toJson(materiales_send_ean));
        editor.putString("materiales_inv",gson.toJson(materiales));
        editor.putString("materiales_send_as_inv",gson.toJson(materiales_send_as));
        editor.putString("MatIngresado_inv",gson.toJson(MatIngresado));
        editor.putString("Cantidad_inv",gson.toJson(cantidad));
        editor.putString("Locacion_inv",gson.toJson(locations));
        editor.commit();
        adapter.notifyDataSetChanged();
        c.close();
    }
    public void getMateriales(){
        DBHelper dbHelper = new DBHelper(InventarioActivity2.this);
        SQLiteDatabase db=dbHelper.getReadableDatabase();
        //String query = "SELECT codas,descripcion FROM "+global.getTbl_material()+ " group by codas";
        String query = "SELECT codas,descripcion,locacion FROM "+global.getTbl_material()+" group by codas";
        Cursor c=(db.rawQuery(query,null));
        if(c.getCount()>0) {
            if (c.moveToFirst()) {
                do {
                    sl_materiales.add(c.getString(0).toString().trim()+" | "+c.getString(1).toString().trim());
                    sp_materiales_locations.add(c.getString(2).toString().trim());
                }while (c.moveToNext());
            }
            adapter= new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item,sl_materiales);
            sp_materiales.setAdapter(adapter);

            Gson gson = new Gson();
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putString("list_materiales_cont2",gson.toJson(sl_materiales));
            editor.putString("sp_materiales_locations2",gson.toJson(sp_materiales_locations));
            editor.commit();
        }
    }
    public void removeLocation(int position,String material){

        String[] item= materiales.get(position).toString().split("\\|");
        String mat=item[0].trim();
        String des=item[1].trim();
        String ca=item[2].split(":")[1].trim();
        int p=MatIngresado.indexOf(mat);
        if(p!=-1){ //Si existe
            MatIngresado.remove(p);
            cantidad.remove(p);
            locations.remove(p);
            materiales.remove(position);
        }
        Gson gson = new Gson();
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("materiales_inv",gson.toJson(materiales));
        editor.putString("MatIngresado_inv",gson.toJson(MatIngresado));
        editor.putString("Cantidad_inv",gson.toJson(cantidad));
        adapter.notifyDataSetChanged();


    }

}
