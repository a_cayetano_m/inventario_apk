package com.example.invfisico;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.invfisico.DB.DBHelper;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class InventarioActivity2_nuevo extends AppCompatActivity {

    public static SharedPreferences sharedpreferences;
    Button  btn_closeSession,btn_saveMaterial,btn_clean;
    public String Authorization="",UrlApi="",ContentType="";
    public GlobalClass global;
    public static final String MyPREFERENCES = "MyPrefs";

    LinkedList<String> MatIngresado = new LinkedList<String>();
    LinkedList<String> materiales_send_as = new LinkedList<String>();
    LinkedList<String> materiales_send_ean = new LinkedList<String>();
    LinkedList<Integer> cantidad = new LinkedList<Integer>();
    LinkedList<String> MatListMissing = new LinkedList<String>(); //Lista de materiales faltantes
    LinkedList<String> DesListMissing = new LinkedList<String>(); //Lista de  Descripcijones faltantes
    LinkedList<String> Missing = new LinkedList<String>(); //Lista de faltantes


    LinkedList<String> materiales = new LinkedList<String>();
    LinkedList<String> locations = new LinkedList<String>();
    ArrayAdapter<String> adapter;
    ArrayAdapter<String> adapterMissing;

    private String location_active;
    private String material_active;
    private String descripcion_active;
    private ListView MaterialesList;
    LinkedList<String> locationsPreferer_general;
    EditText et_material, et_location;
    TextView title_inv;
    TextView material;
    TextView descripcion;
    Spinner sp_locations;
    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventario2_nuevo);
        getSupportActionBar().hide();

        Authorization=new GlobalClass(getApplicationContext()).getApiAuth();
        UrlApi=new GlobalClass(getApplicationContext()).getUrlApi();
        ContentType=new GlobalClass(getApplicationContext()).getContentType();
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        global = new GlobalClass(getApplicationContext());

        MaterialesList = (ListView) findViewById(R.id.MaterialesList);
        String mat_ipt_desp = "";
        et_material=findViewById(R.id.et_material);
        et_location=findViewById(R.id.et_location);
        title_inv=  (TextView)findViewById(R.id.title_inv);
        material=  (TextView)findViewById(R.id.material);
        descripcion=  (TextView)findViewById(R.id.descripcion);

        btn_saveMaterial= (Button) findViewById(R.id.btn_saveMaterial);
        btn_clean= (Button) findViewById(R.id.btn_clean);

        Bundle extras = getIntent().getExtras();
        location_active=extras.getString("LOCATION").trim();
        material_active=extras.getString("MATERIAL").trim();
        descripcion_active=extras.getString("DESC").trim();
        title_inv.setText("LOC :"+location_active);
        material.setText(" MAT :"+material_active);
        descripcion.setText(descripcion_active);
        FocusLocation();
        DisableMaterial();
        /*et_location.setEnabled(false);
        et_material.requestFocus();*/

        Gson gson = new Gson();
        String materiales_send_eanPreferer=sharedpreferences.getString("materiales_send_ean_inv", "");
        String materiales_invPreferer=sharedpreferences.getString("materiales_inv", "");
        String materiales_send_asPreferer=sharedpreferences.getString("materiales_send_as_inv", "");
        String MatIngresadoPreferer=sharedpreferences.getString("MatIngresado_inv", "");
        String CantidadPreferer=sharedpreferences.getString("Cantidad_inv", "");



        LinkedList matsendean = gson.fromJson(materiales_send_eanPreferer, LinkedList.class);
        LinkedList matsendas = gson.fromJson(materiales_send_asPreferer, LinkedList.class);
        LinkedList mating = gson.fromJson(MatIngresadoPreferer, LinkedList.class);
        LinkedList cant = gson.fromJson(CantidadPreferer, LinkedList.class);

        getLocations();
        getListMaterials();


        System.out.println("cantidad");
        System.out.println(cant);
        adapter= new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,materiales);
        MaterialesList.setAdapter(adapter);

        btn_saveMaterial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String location = et_location.getText().toString();
                if(location.equals("")){
                    Toast.makeText(getApplicationContext(),"Debe escanear la Locacion",Toast.LENGTH_SHORT).show();
                }else{
                new AlertDialog.Builder(InventarioActivity2_nuevo.this)
                        .setTitle("¿GUARDAR CONTEO?")
                        .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {


                                    if(materiales.size()>0){
                                        sendConteo();

                                    }else{
                                        //Toast.makeText(getApplicationContext(),"Debe ingresar materiales",Toast.LENGTH_SHORT).show();
                                        //Se cambia el texto informativo por el combo en el que figura los codigos pendientes de contar
                                        sendConteo();
                                    }
                                dialog.dismiss();
                            }
                        }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).create().show();
                }
            }
        });
        btn_clean.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View vi) {
                if(!global.isNetworkConnected()) {
                    Toast.makeText(getApplicationContext(), global.getnotNetworkMessage(), Toast.LENGTH_LONG).show();
                }else{
                    DBHelper dbHelper = new DBHelper(getApplicationContext());
                    SQLiteDatabase db = dbHelper.getReadableDatabase();
                    //ACTUALIZA DESCRIPCION
                    String query = "SELECT * FROM " + global.getTbl_location_count()+ " where locacion='"+location_active+"' and descripcion = 'SIN DESCRIPCIÓN'";
                    DialogActivity dg = new DialogActivity(InventarioActivity2_nuevo.this);
                    dg.startLoadingDialog();
                    Cursor c = (db.rawQuery(query, null));
                    if (c.getCount() > 0) {
                        if (c.moveToFirst()) {
                            do {
                                //Declaro los valores encontrados
                                String r_ean = c.getString(3).trim();
                                String r_as = c.getString(6).trim();
                                String codigo = "";
                                if(r_ean.equals("")){
                                    codigo = r_as;
                                }else {
                                    codigo = r_ean;
                                }

                                String url=global.getUrlApi()+"actualizaDescripcion/";
                                String finalCodigo = codigo;
                                StringRequest request = new StringRequest(Request.Method.POST,
                                        url,
                                        response -> {
                                            JSONObject obj = null;
                                            String json = response.replaceAll("\r\n","").replace("sql","").trim();
                                            System.out.println(response);
                                            try {
                                                obj = new JSONObject(json);
                                                if (obj.get("VALIDATE").toString().equals("1")) { // SI ES CORRECTO validate=1
                                                    JSONArray array = new JSONArray(obj.get("DATA").toString());
                                                    JSONObject row = array.getJSONObject(0);
                                                    String R_cod_as = row.get("CODIGO").toString().trim();
                                                    String R_descripcion = row.get("DESCRIPCION").toString().trim();
                                                    String R_factor = row.get("FACTOR").toString().trim();
                                                    String R_unidad = row.get("UNIDAD").toString().trim();
                                                    String R_ean = row.get("EAN").toString().trim();
                                                    int desiredLength = 6;
                                                    String R_cod_as_0 = String.format("%0"+desiredLength+"d",Integer.parseInt(R_cod_as));
                                                    if(r_ean.equals("")){
                                                        db.execSQL("UPDATE "+global.getTbl_location_count()+" SET descripcion='"+R_descripcion+"' , factor='"+R_factor+"', unidadbase='"+R_unidad+"', codean='"+R_ean+"' WHERE codas="+R_cod_as+" OR codas = '"+R_cod_as_0+"'");
                                                    }else {
                                                        db.execSQL("UPDATE "+global.getTbl_location_count()+" SET descripcion='"+R_descripcion+"' , factor='"+R_factor+"', unidadbase='"+R_unidad+"', codas='"+R_cod_as+"' WHERE codean='"+R_ean+"'");
                                                    }
                                                }else{
                                                    Toast.makeText(getApplicationContext(),obj.get("Sin Descripciones por Actualizar").toString(),Toast.LENGTH_SHORT).show();
                                                }

                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }, error -> {
                                }){
                                    @Override
                                    protected Map<String, String> getParams() throws AuthFailureError {
                                        Map<String,String> params = new HashMap<>();
                                        params.put("MATERIAL", finalCodigo);
                                        params.put("SUCURSAL",sharedpreferences.getString(global.getKeyuserSucursal(), ""));
                                        params.put("USERNAME",sharedpreferences.getString(global.getKeyuser(), ""));
                                        params.put("TOKEN",global.getTokenAuthApi());
                                        return params;
                                    }
                                };
                                Volley.newRequestQueue(getApplicationContext()).add(request);
                            } while (c.moveToNext());
                        }
                    }

                    //SI NO ENCUENTRA DESCRIPCION

                    String query2 = "SELECT * FROM " + global.getTbl_location_count()+ " where locacion='"+location_active+"' and descripcion = 'SIN DESCRIPCIÓN'";
                    Cursor c2 = (db.rawQuery(query2, null));
                    if (c2.getCount() > 0) {
                        if (c2.moveToFirst()) {
                            do {
                                //Declaro los valores encontrados
                                String r_ean = c2.getString(3).trim();
                                String r_as = c2.getString(6).trim();
                                String codigo = "";
                                if(r_ean.equals("")){
                                    db.execSQL("UPDATE "+global.getTbl_location_count()+" SET descripcion='NO SE ENCONTRO EN AS400' WHERE codas="+r_as);
                                }else {
                                    db.execSQL("UPDATE "+global.getTbl_location_count()+" SET descripcion='NO SE ENCONTRO EN AS400' WHERE codean="+r_ean);
                                }
                            } while (c2.moveToNext());
                        }
                    }



                    new android.os.Handler().postDelayed(
                            new Runnable() {
                                public void run() {
                                    dg.dismissDialog();
                                    getListMaterials();
                                }
                            }, 0);

                }
            }
        });

        et_location.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    String loc_ipt = et_location.getText().toString();
                    if (!loc_ipt.equals("")) {
                        if (!loc_ipt.equals(location_active)) {
                            et_location.getText().clear();
                            FocusLocation();
                            Toast.makeText(getApplicationContext(), "Debe ingresar la ubicación "+location_active, Toast.LENGTH_SHORT).show();
                        } else {
                            et_location.setEnabled(false);
                            EnableMaterial();
                        }

                    }else{
                        EnableLocation();
                    }
                }
                return false;
            }
        });


        et_material.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    DBHelper dbHelper = new DBHelper(getApplicationContext());
                    SQLiteDatabase db = dbHelper.getReadableDatabase();

                    String mat_ipt = et_material.getText().toString();
                    String loc_ipt = et_location.getText().toString();

                    if(!mat_ipt.equals("")){
                        if(mat_ipt.length()<7) {
                            String material = mat_ipt.replaceFirst("^0+", "");
                            addMaterial(loc_ipt,material);
                            et_material.getText().clear();
                        }else{
                            addMaterial(loc_ipt,mat_ipt);
                            et_material.getText().clear();
                        }
                    }else{
                        Toast.makeText(getApplicationContext(),"Debe ingresar un material",Toast.LENGTH_SHORT).show();
                    }
                    et_material.requestFocus();
                    FocusMaterial();
                    FocusMaterial();
                    FocusMaterial();
                }
                return false;
            }

        });


        MaterialesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                showDetails(position,materiales);
            }
        });
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent = new Intent(getApplicationContext(),Principal2Activity.class);
        finish();
        startActivity(intent);

    }
    public void showDetails(int position,LinkedList lista){
        LayoutInflater inflater = getLayoutInflater();
        final View v = inflater.inflate(R.layout.activity_inventario_det, null);
        final AlertDialog.Builder builder =  new AlertDialog.Builder(InventarioActivity2_nuevo.this);
        builder.setView(v);
        final ImageView icon_close = v.findViewById(R.id.icon_close); /* Initialice variables de vista llamada */
        final Button btn_delete_inv_item = v.findViewById(R.id.btn_delete_inv_item);
        final Button btn_save_inv_cantidad = v.findViewById(R.id.btn_save_inv_cantidad);
        final TextView title_inv_material = v.findViewById(R.id.title_inv_material);
        final TextView title_inv_location = v.findViewById(R.id.title_inv_location);
        final TextView title_inv_cantidad = v.findViewById(R.id.title_inv_cantidad);

        String[] item= lista.get(position).toString().split("\\|");
        String mat=item[1].trim();
        String des=item[2].trim();
        String id=item[0].split(":")[1].trim();
        String ca="";
        if(item.length>2){
            ca=item[3].split(":")[1].trim();
        }else{
            ca="";
        }


        title_inv_material.setText(mat);
        title_inv_location.setText(et_location.getText().toString().trim());
        title_inv_cantidad.setText(ca);

        final AlertDialog popud = builder.show();
        TextView et_cantidad_mod= v.findViewById(R.id.et_cantidad_mod);
        icon_close.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                popud.dismiss();
            }
        });
        btn_save_inv_cantidad.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                DBHelper dbHelper = new DBHelper(getApplicationContext());
                SQLiteDatabase db = dbHelper.getReadableDatabase();
                String itemSelected=adapter.getItem(position).toString().trim();
                String item[]=itemSelected.split("\\|"); //"EAN: 54545 | D: "
                String cod=item[1].trim();
                String query = "";
                if(cod.length()>6){
                    query = "SELECT * FROM " + global.getTbl_location_count()+ " where codean='"+cod+"' AND id = "+id;
                }else{
                    query = "SELECT * FROM " + global.getTbl_location_count()+ " where codas='"+cod+"' AND id = "+id;
                }
                System.out.println("aqui");
                System.out.println(query);
                Cursor c = (db.rawQuery(query, null));
                DialogActivity dg = new DialogActivity(InventarioActivity2_nuevo.this);
                dg.startLoadingDialog();

                if (c.moveToFirst()) {
                    do {
                        String r_id = c.getString(0).trim();
                        String r_factor = c.getString(4).trim();
                        String r_cantidad = c.getString(7).toString().trim();
                        int cnt;
                        if(et_cantidad_mod.getText().toString().trim().equals("")){
                            cnt=Integer.parseInt(r_factor);
                        }else{
                            cnt=Integer.parseInt(et_cantidad_mod.getText().toString().trim());
                        }

                        db.execSQL("UPDATE "+global.getTbl_location_count()+" SET cantidad="+cnt+" WHERE id="+r_id);
                        break;
                    } while (c.moveToNext());
                }

                new android.os.Handler().postDelayed(
                        new Runnable() {
                            public void run() {
                                dg.dismissDialog();
                                getListMaterials();
                                FocusMaterial();
                                KeyBoard();
                            }
                        }, 0);
                Toast.makeText(getApplicationContext(),"Se guardó correctamente",Toast.LENGTH_SHORT).show();
                popud.dismiss();
                FocusMaterial();
                KeyBoard();
            }
        });
        btn_delete_inv_item.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                new AlertDialog.Builder(InventarioActivity2_nuevo.this)
                        .setTitle("¿Seguro de borrar?")
                        .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                removeMaterial(mat,id);
                                Toast.makeText(getApplicationContext(), "Se borró correctamente", Toast.LENGTH_LONG).show();
                                getListMaterials();
                                popud.dismiss();
                            }
                        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).create().show();
                getListMaterials();
                adapter.notifyDataSetChanged();

            }

        });
    }
    public void removeAllMaterial(){
        DBHelper dbHelper = new DBHelper(getApplicationContext());
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        db.execSQL("DELETE FROM "+global.getTbl_location_count());
        materiales.removeAll(materiales);
        adapter.notifyDataSetChanged();
    }
    public void getLocations() {
        locations.removeAll(locations);
        DBHelper dbHelper = new DBHelper(getApplicationContext());
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = "SELECT id,locacion FROM " + global.getTbl_location_inv()+ " order by id desc";

        Cursor c = (db.rawQuery(query, null));
        if (c.getCount() > 0) {
            if (c.moveToFirst()) {
                do {
                    String r_id = c.getString(0).trim();
                    String r_loc = c.getString(1).trim();
                    locations.add(r_loc);
                } while (c.moveToNext());
            }
        }
    }
    public void KeyBoard(){
        View view= this.getCurrentFocus();
        if(view != null){
            InputMethodManager inn=(InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            inn.hideSoftInputFromWindow(view.getWindowToken(),0);
        }
    }
    public void FocusMaterial(){
        et_material.getText().clear();
        et_material.requestFocus();
        KeyBoard();
    }
    public void FocusLocation(){
        et_location.getText().clear();
        et_location.requestFocus();
    }
    public void DisableLocation(){
        et_location.setEnabled(false);
    }
    public void EnableLocation(){
        et_location.setEnabled(true);
        et_location.requestFocus();
    }
    public void DisableMaterial(){
        et_material.setEnabled(false);
    }
    public void EnableMaterial(){
        et_material.setEnabled(true);
        et_material.requestFocus();
    }



    
    public void sendConteo(){
        if(!global.isNetworkConnected()){
            Toast.makeText(getApplicationContext(), global.getnotNetworkMessage(), Toast.LENGTH_LONG).show();
        }else{
                if(et_location.equals("")){
                    Toast.makeText(getApplicationContext(),"Se tiene que escanear la locacion para enviar",Toast.LENGTH_SHORT).show();
                }else{
                    DBHelper dbHelper = new DBHelper(InventarioActivity2_nuevo.this);
                    SQLiteDatabase db=dbHelper.getReadableDatabase();

                    String queryConteo = "SELECT locacion FROM " + global.getTbl_location_count()+ " where locacion='"+location_active+"' and descripcion = 'SIN DESCRIPCIÓN' or descripcion = 'NO SE ENCONTRO EN AS400'";
                    Cursor c_2 = (db.rawQuery(queryConteo, null));
                    if (c_2.getCount() > 0) {
                        if (c_2.moveToFirst()) {
                            do {
                                Toast.makeText(getApplicationContext(),"Hay códigos ingresados que no existen en AS400",Toast.LENGTH_SHORT).show();
                            } while (c_2.moveToNext());
                        }
                    }else{
                        //si no encuentra codigos que no existe lo envia
                        System.out.println("EN ELSE PARA ENVIO CREATE");
                        DialogActivity dg = new DialogActivity(InventarioActivity2_nuevo.this);
                        dg.startLoadingDialog();
                        String url=global.getUrlApi()+"createSecondCont/";
                        StringRequest request = new StringRequest(Request.Method.POST,
                                url,
                                response -> {
                                    System.out.println("responseInv");
                                    System.out.println(response);
                                    JSONObject obj = null;
                                    try {
                                        obj = new JSONObject(response);
                                        if (obj.get("VALIDATE").toString().equals("1")) { // SI ES CORRECTO validate=1
                                            Toast.makeText(getApplicationContext(),obj.get("MSG").toString(),Toast.LENGTH_SHORT).show();
                                            db.execSQL("DELETE FROM "+global.getTbl_material()+" WHERE locacion='"+location_active+"' AND codas = '"+material_active+"'");
                                            String loca = "SELECT * FROM "+global.getTbl_material()+" WHERE locacion = '"+location_active+"'";
                                            Cursor c_loca=(db.rawQuery(loca,null));

                                            if(c_loca.getCount()>0){
                                                String query_cod = "SELECT distinct codas,descripcion FROM "+global.getTbl_material()+" where locacion = '"+location_active+"' order by codas asc limit 1";
                                                Cursor c_cod=(db.rawQuery(query_cod,null));
                                                c_cod.moveToFirst();
                                                String codigo= c_cod.getString(0);
                                                String desc= c_cod.getString(1);
                                                material_active = codigo;
                                                material.setText("MAT :"+material_active);
                                                descripcion_active = desc;
                                                descripcion.setText(descripcion_active);
                                                //title_inv.setText("CONTEO "+location_active);
                                                et_location.setText("");
                                                et_location.setEnabled(true);
                                            }else{
                                                db.execSQL("DELETE FROM "+global.getTbl_location_inv()+" WHERE locacion='"+location_active+"'");
                                                String query_loc = "SELECT locacion FROM "+global.getTbl_location_inv()+" ORDER BY locacion ASC LIMIT 1";
                                                Cursor c_loc=(db.rawQuery(query_loc,null));
                                                if(c_loc.getCount()>0) {
                                                    if(c_loc.moveToFirst()) {
                                                        do {
                                                            //location_active = "";
                                                            location_active = c_loc.getString(0).toString().trim();
                                                            String query_cod = "SELECT distinct codas,descripcion FROM "+global.getTbl_material()+" where locacion = '"+location_active+"' order by codas asc limit 1";
                                                            Cursor c_cod=(db.rawQuery(query_cod,null));
                                                            c_cod.moveToFirst();
                                                            String codigo= c_cod.getString(0);
                                                            String desc= c_cod.getString(1);
                                                            material_active = codigo;
                                                            material.setText("MAT :"+material_active);
                                                            descripcion_active = desc;
                                                            descripcion.setText(descripcion_active);
                                                            System.out.println("location_active");
                                                            System.out.println(location_active);
                                                            title_inv.setText("LOC :"+location_active);
                                                            //title_inv.setText("CONTEO "+location_active);
                                                            et_location.setText("");
                                                            et_location.setEnabled(true);
                                                            //EnableMaterial();
                                                        }while (c_loc.moveToNext());

                                                    }
                                                }else {
                                                    db.execSQL("DELETE FROM "+global.getTbl_material());
                                                    location_active = "";
                                                    title_inv.setText("LOC-TERM");
                                                    material.setText("COD-TER");
                                                    descripcion.setText("");
                                                }
                                            }
                                            DisableMaterial();
                                            FocusLocation();
                                            removeAllMaterial();

                                        }else{
                                            Toast.makeText(getApplicationContext(),obj.get("MSG").toString(),Toast.LENGTH_SHORT).show();
                                        }
                                        new android.os.Handler().postDelayed(
                                                new Runnable() {
                                                    public void run() {
                                                        dg.dismissDialog();
                                                        dg.startSuccessDialog();
                                                        new android.os.Handler().postDelayed(
                                                                new Runnable() {
                                                                    public void run() {
                                                                        dg.dismissDialog();
                                                                    }
                                                                }, 1000);

                                                    }
                                                }, 1000);

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }, error -> {
                        }){
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                Map<String,String> params = new HashMap<>();
                                System.out.println("getAllMaterial()");
                                System.out.println(getAllMaterial());
                                params.put("SUCURSAL",sharedpreferences.getString(global.getKeyuserSucursal(), ""));
                                params.put("MATERIAL",getAllMaterial());
                                params.put("LOCATION",location_active);
                                params.put("TOKEN",global.getTokenAuthApi());
                                params.put("USERNAME",sharedpreferences.getString(global.getKeyuser(), ""));

                                return params;

                            }
                        };
                        Volley.newRequestQueue(getApplicationContext()).add(request);
                    }
                }
        }
    }
    public String getAllMaterial(){
        DBHelper dbHelper = new DBHelper(getApplicationContext());
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String query = "SELECT id,locacion,descripcion,codean,factor,unidadbase,codas,sum(cantidad),indicador,contado,hora FROM " + global.getTbl_location_count()+" group by codas,codean";
        Cursor c = (db.rawQuery(query, null));
        String r_as="";
        ArrayList<String> list_as=new ArrayList<>();

        int i=0;
        if (c.getCount() > 0) {
            if (c.moveToFirst()) {
                do {
                    if(i==0){
                        if(c.getString(6).trim().equals("")){
                            r_as = c.getString(3).trim()+"-"+c.getString(7).trim()+"-"+c.getString(8).trim();
                            list_as.add(c.getString(3));
                        }else{
                            r_as = c.getString(6).trim()+"-"+c.getString(7).trim()+"-"+c.getString(8).trim();
                            list_as.add(c.getString(6));
                        }

                    }else{
                        if(list_as.indexOf(c.getString(6))==-1){
                            if(c.getString(6).trim().equals("")){
                                list_as.add(c.getString(3));
                                r_as = r_as +","+c.getString(3).trim()+"-"+c.getString(7).trim()+"-"+c.getString(8).trim();
                            }else{
                                list_as.add(c.getString(6));
                                r_as = r_as +","+c.getString(6).trim()+"-"+c.getString(7).trim()+"-"+c.getString(8).trim();
                            }

                        }
                    }
                    i++;
                } while (c.moveToNext());
            }
        }
        return r_as;
    }

    public void addMaterial(String locacion,String material){


        Gson gson = new Gson();
        DialogActivity dg = new DialogActivity(InventarioActivity2_nuevo.this);
        dg.startLoadingDialog();
        DBHelper dbHelper = new DBHelper(InventarioActivity2_nuevo.this);
        SQLiteDatabase db=dbHelper.getReadableDatabase();
        String query="";
        if(material.length()>6){
            query = "SELECT * FROM "+global.getTbl_material()+ " WHERE codean='"+material+"' AND locacion='"+locacion+"' group by codean LIMIT 1";
        }else{
            String Mat_Sin0 = material.replaceFirst("^0+","");
            query = "SELECT * FROM "+global.getTbl_material()+ " WHERE codas='"+Mat_Sin0+"' AND locacion='"+locacion+"' ORDER BY factor ASC LIMIT 1";
        }
        //REVISO SI EL MATERIAL EXISTE EL EL MAESTRO DEL RF
        Cursor c=(db.rawQuery(query,null));
        if(c.getCount()>0){  //SI EXISTE EN MATERIAL
            if(c.moveToFirst()) {
                do{
                    String r_descripcion = c.getString(2).trim();
                    String r_ean = c.getString(3).toString().trim();
                    String r_as = c.getString(6).toString().trim();
                    String r_factor = c.getString(4).toString().trim();
                    String r_unidad = c.getString(5).toString().trim();
                    String r_cantidad = r_factor;
                    //OBTENGO LOS VALORES DE LA TABLA MATERIAL
                    String query_det="";
                    if(r_as.equals("")){
                        query_det = "SELECT * FROM "+global.getTbl_location_count()+ " WHERE locacion='"+locacion.trim()+"' and (codean='"+r_ean+"') LIMIT 1";
                    }else{
                        query_det = "SELECT * FROM "+global.getTbl_location_count()+ " WHERE locacion='"+locacion.trim()+"' and (codas='"+r_as+"') LIMIT 1";
                    }
                    Cursor c_det=(db.rawQuery(query_det,null));
                    //BUSCO SI YA LO CONTE
                    System.out.println("query_det");
                    System.out.println(query_det);
                    if(c_det.getCount()>0) {
                        if (c_det.moveToFirst()) {
                            do {
                                //BUSCO EL ULTIMO CODIGO LEIDO
                                System.out.println("codigo ya leido");
                                String query_det2="";
                                query_det2 = "SELECT * FROM "+global.getTbl_location_count()+ " ORDER BY HORA DESC LIMIT 1";
                                Cursor c_det2=(db.rawQuery(query_det2,null));
                                if(c_det2.getCount()>0) {
                                    if (c_det2.moveToFirst()) {
                                        do {
                                            String r2_id = c_det2.getString(0).toString().trim();
                                            String r2_ean = c_det2.getString(3).toString().trim();
                                            String r2_as = c_det2.getString(6).toString().trim();
                                            String nuevo = "NO";
                                            if(r2_ean.equals(r_ean)){
                                                nuevo = "SI";
                                            }
                                            if(r_as.equals(r2_as)){
                                                nuevo = "SI";
                                            }
                                            System.out.println(nuevo);
                                            if(nuevo == "SI"){
                                                c.moveToFirst();
                                                String r_id_det = c_det.getString(0).trim();
                                                String r_factor_det = c_det.getString(4).trim();
                                                String r_cantidad_det =   c_det2.getString(7).trim();
                                                int cant=Integer.parseInt(r_cantidad_det)+Integer.parseInt(r_factor);
                                                db.execSQL("UPDATE "+global.getTbl_location_count()+" SET cantidad="+cant+", hora = '"+ new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()) +"'  WHERE id="+r2_id);
                                            }else{
                                                ContentValues values = new ContentValues();
                                                values.put("locacion",locacion.trim());
                                                values.put("descripcion",r_descripcion);
                                                values.put("codean",r_ean);
                                                values.put("factor",r_factor);
                                                values.put("unidadbase",r_unidad);
                                                values.put("codas",r_as);
                                                values.put("cantidad",r_cantidad);
                                                values.put("indicador","1");
                                                values.put("contado","*");
                                                values.put("hora",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
                                                db.insert(global.getTbl_location_count(),null,values);
                                                FocusMaterial();
                                                getListMaterials();
                                            }

                                        }while (c_det2.moveToNext());
                                    }
                                }
                                FocusMaterial();
                                getListMaterials();
                            }while (c_det.moveToNext());
                        }
                        FocusMaterial();
                        getListMaterials();
                    }else{
                        String query_factor = "SELECT * FROM "+global.getTbl_material()+ " WHERE codas='"+r_as+"' AND locacion='"+locacion+"' AND factor = 1 ORDER BY factor ASC LIMIT 1";
                        Cursor c_factor=(db.rawQuery(query_factor,null));
                        if(c_factor.getCount()>0) {
                            if (c_factor.moveToFirst()) {
                                do {
                                    String r_und2 = c_factor.getString(5).trim();
                                    ContentValues values = new ContentValues();
                                    values.put("locacion",locacion.trim());
                                    values.put("descripcion",r_descripcion);
                                    values.put("codean",r_ean);
                                    values.put("factor",r_factor);
                                    values.put("unidadbase",r_und2);
                                    values.put("codas",r_as);
                                    values.put("cantidad",r_cantidad);
                                    values.put("indicador","1");
                                    values.put("contado","");
                                    values.put("hora",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
                                    db.insert(global.getTbl_location_count(),null,values);
                                    FocusMaterial();
                                    getListMaterials();
                                }while (c_factor.moveToNext());
                            }
                        }else{
                            ContentValues values = new ContentValues();
                            values.put("locacion",locacion.trim());
                            values.put("descripcion",r_descripcion);
                            values.put("codean",r_ean);
                            values.put("factor",r_factor);
                            values.put("unidadbase",r_unidad);
                            values.put("codas",r_as);
                            values.put("cantidad",r_cantidad);
                            values.put("indicador","1");
                            values.put("contado","");
                            values.put("hora",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
                            db.insert(global.getTbl_location_count(),null,values);
                            FocusMaterial();
                            getListMaterials();
                        }
                    }

                }while (c.moveToNext());

            }
            new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {
                            dg.dismissDialog();FocusMaterial();
                        }
                    }, 0);
        }else{ // Sin EAN busca por AS
            if(material.length()<=6){
                query = "SELECT * FROM "+global.getTbl_location_count()+ " WHERE locacion='"+locacion.trim()+"' and (codas='"+material+"')";
            }else{
                query = "SELECT * FROM "+global.getTbl_location_count()+ " WHERE locacion='"+locacion.trim()+"' and (codean='"+material+"')";
            }
            Cursor c_4=(db.rawQuery(query,null));
            System.out.println("query EXISTENTE");
            System.out.println(query);
            if(c_4.getCount()>0) {  //SI EXISTE EN MATERIALES CONTADOS
                if (c_4.moveToFirst()) {
                    do {
                        String r_ean = c_4.getString(3).toString().trim();
                        String r_as = c_4.getString(6).toString().trim();
                        String r_descripcion = c_4.getString(2).toString().trim();
                        String r_factor = c_4.getString(4).toString().trim();
                        String r_unidad = c_4.getString(5).toString().trim();
                        String r_cantidad = r_factor;
                        String query_det3="";
                        query_det3 = "SELECT * FROM "+global.getTbl_location_count()+ " ORDER BY HORA DESC LIMIT 1";
                        Cursor c_det3=(db.rawQuery(query_det3,null));
                        if(c_det3.getCount()>0) {
                            if (c_det3.moveToFirst()) {
                                do {
                                    String r3_ean = c_det3.getString(3).toString().trim();
                                    String r3_as = c_det3.getString(6).toString().trim();
                                    String nuevo = "NO";
                                    if(r_as.equals("")){
                                        if(r3_ean.equals(r_ean)){
                                            nuevo = "SI";
                                        }
                                    }else{
                                        if(r3_as.equals(r_as)){
                                            nuevo = "SI";
                                        }
                                    }

                                    if(nuevo == "SI"){
                                        String r_id_det = c_det3.getString(0).trim();
                                        String r_factor_det = c_det3.getString(4).trim();
                                        String r_cantidad_det =   c_det3.getString(7).trim();
                                        if(r_factor_det.equals("")){
                                            r_factor_det="1";
                                        }
                                        int cant=Integer.parseInt(r_cantidad_det)+Integer.parseInt(r_factor_det);
                                        db.execSQL("UPDATE "+global.getTbl_location_count()+" SET cantidad="+cant+", hora = '"+ new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()) +"'  WHERE id="+r_id_det);
                                    } else {
                                        ContentValues values = new ContentValues();
                                        values.put("locacion",locacion.trim());
                                        values.put("descripcion",r_descripcion);
                                        values.put("codean",r_ean);
                                        values.put("factor",r_factor);
                                        values.put("unidadbase",r_unidad);
                                        values.put("codas",r_as);
                                        values.put("cantidad",r_cantidad);
                                        values.put("indicador","1");
                                        values.put("contado","*");
                                        values.put("hora",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
                                        db.insert(global.getTbl_location_count(),null,values);
                                        FocusMaterial();
                                        getListMaterials();
                                    }
                                }while (c_det3.moveToNext());
                                FocusMaterial();
                            }
                        }
                    } while (c.moveToNext());
                    FocusMaterial();
                }
                getListMaterials();
                new android.os.Handler().postDelayed(
                        new Runnable() {
                            public void run() {
                                dg.dismissDialog();
                                FocusMaterial();
                            }
                        }, 0);
                FocusMaterial();
            }else {

                new android.os.Handler().postDelayed(
                        new Runnable() {
                            public void run() {
                                dg.dismissDialog();
                                Toast.makeText(getApplicationContext(),"Solo escanear el Código Mostrado",Toast.LENGTH_SHORT).show();
                                FocusMaterial();
                            }
                        }, 0);
            }
        }

        /*getListMaterials();
        FocusMaterial();*/
        c.close();
    }
    public void showDetailsNewMaterial(String loc, String mat){
        LayoutInflater inflater_nuevo = getLayoutInflater();
        final View vie = inflater_nuevo.inflate(R.layout.activity_inventario_det_nuevo, null);
        final AlertDialog.Builder builder =  new AlertDialog.Builder(InventarioActivity2_nuevo.this);
        builder.setView(vie);
        final ImageView icon_close_nuevo = vie.findViewById(R.id.icon_close); /* Initialice variables de vista llamada */
        final Button btn_save_inv_cantidad_nuevo = vie.findViewById(R.id.btn_save_inv_cantidad_nuevo);
        final TextView title_det_nuevo = vie.findViewById(R.id.title_det_nuevo);
        final TextView title_inv_material_nuevo = vie.findViewById(R.id.title_inv_material_nuevo);
        final TextView title_inv_location_nuevo = vie.findViewById(R.id.title_inv_location_nuevo);


        DBHelper dbHelper = new DBHelper(getApplicationContext());
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = "SELECT * FROM " + global.getTbl_material() + " where codas='" + mat + "' and factor = 1 LIMIT 1";
        Cursor c_n = (db.rawQuery(query, null));

        String r_id_v="";
        String r_loc_v="";
        String r_descripcion_v="";
        String r_as_v="";
        String r_ean_v="";
        String r_factor_v="";
        String r_unidadbase_v="";
        final int cont_reg=c_n.getCount();
        if(cont_reg>0){
            title_det_nuevo.setText("MATERIAL FALTANTE");
            if (c_n.moveToFirst()) {
                do {
                     r_loc_v = c_n.getString(1).trim();
                     r_descripcion_v = c_n.getString(2).trim();
                     r_ean_v = c_n.getString(3).toString().trim();
                     r_factor_v = c_n.getString(4).toString().trim();
                     r_unidadbase_v = c_n.getString(5).toString().trim();
                     r_as_v = c_n.getString(6).toString().trim();
                } while (c_n.moveToNext());
            }
        }else{
            title_det_nuevo.setText("MATERIAL NO ASIGNADO");
        }

        title_inv_material_nuevo.setText(mat);
        title_inv_location_nuevo.setText(loc);
        TextView et_cantidad_mod_nuevo= vie.findViewById(R.id.et_cantidad_mod_nuevo);
        final AlertDialog popud_nuevo = builder.show();

        String finalR_loc_v = r_loc_v;
        String finalR_descripcion_v = r_descripcion_v;
        String finalR_ean_v = r_ean_v;
        String finalR_factor_v = r_factor_v;
        String finalR_unidadbase_v = r_unidadbase_v;
        String finalR_as_v = r_as_v;
        btn_save_inv_cantidad_nuevo.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                String cantidad_nueva=et_cantidad_mod_nuevo.getText().toString();
                if(!cantidad_nueva.equals("")){
                    DialogActivity dg = new DialogActivity(InventarioActivity2_nuevo.this);
                    dg.startLoadingDialog();
                    ContentValues values = new ContentValues();
                    if(cont_reg>0){
                        values.put("locacion", loc);
                        values.put("descripcion", finalR_descripcion_v);
                        values.put("codean", finalR_ean_v);
                        values.put("factor", finalR_factor_v);
                        values.put("unidadbase", finalR_unidadbase_v);
                        values.put("codas", finalR_as_v);
                        values.put("cantidad",cantidad_nueva);
                        System.out.println("se insert en existente");
                        values.put("indicador","1"); // Es uno de los que si estaban asignados
                        values.put("contado","");
                        values.put("hora",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
                        db.insert(global.getTbl_location_count(),null,values);
                    }else{
                        title_det_nuevo.setText("MATERIAL NO ASIGNADO");
                        if(mat.length()<=6){ //SI ES COD AS
                            values.put("codas",mat.trim());
                            values.put("codean","");
                        }else{
                            values.put("codean",mat.trim());
                            values.put("codas","");
                        }
                        values.put("locacion", loc);
                        values.put("descripcion", "");
                        values.put("factor", "");
                        values.put("unidadbase", "");
                        values.put("cantidad",cantidad_nueva);
                        values.put("descripcion","SIN DESCRIPCIÓN");
                        values.put("indicador","0"); // Es uno de los que no estaban asignados
                        values.put("contado","");
                        values.put("hora",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
                        db.insert(global.getTbl_location_count(),null,values);

                    }
                    new android.os.Handler().postDelayed(
                            new Runnable() {
                                public void run() {
                                    getListMaterials();
                                    dg.dismissDialog();

                                }
                            }, 0);
                    popud_nuevo.dismiss();
                }else{
                    Toast.makeText(getApplicationContext(),"Debe ingresar una cantidad",Toast.LENGTH_SHORT).show();
                }

            }
        });
        icon_close_nuevo.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View vi){
                popud_nuevo.dismiss();
            }
        });


    }
    public void getListMaterials() {
        materiales.removeAll(materiales);
        DBHelper dbHelper = new DBHelper(getApplicationContext());
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = "SELECT * FROM " + global.getTbl_location_count()+ " order by hora desc";

        Cursor c = (db.rawQuery(query, null));
        String r_loc="";
        if (c.getCount() > 0) {
            if (c.moveToFirst()) {
                do {
                    String r_id = c.getString(0).trim();
                    r_loc = c.getString(1).trim();
                    String r_descripcion = c.getString(2).trim();
                    String r_ean = c.getString(3).toString().trim();
                    String r_factor = c.getString(4).toString().trim();
                    String r_unidad = c.getString(5).toString().trim();
                    String r_as = c.getString(6).toString().trim();
                    String r_cantidad = c.getString(7).toString().trim();
                    String r_contado = c.getString(9).toString().trim();
                    if(r_as.equals("")){
                        if(materiales.indexOf(r_ean + " | " + r_descripcion)==-1){
                            materiales.add("ID:"+r_id+" | "+r_ean + " | " + r_descripcion + " | CANT: " + r_cantidad+" | UMB: " + r_unidad +" | " + r_contado);
                        }
                    }else{
                        if(materiales.indexOf(r_as + " | " + r_descripcion)==-1){
                            materiales.add("ID:"+r_id+" | "+r_as + " | " + r_descripcion + " | CANT: " + r_cantidad+" | UMB: " + r_unidad +" | " + r_contado);
                        }
                    }

                } while (c.moveToNext());
            }

           /* et_location.setText(r_loc);
            et_location.setEnabled(false);*/
            adapter= new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,materiales);
            MaterialesList.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }
    }
    public void removeMaterial(String cod,String id){
            DBHelper dbHelper = new DBHelper(getApplicationContext());
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            if(cod.length()>6){
                db.execSQL("DELETE FROM "+global.getTbl_location_count()+" where codean='"+cod+"' AND id = "+id);
            }else{
                db.execSQL("DELETE FROM "+global.getTbl_location_count()+" where codas='"+cod+"' AND id = "+id);
            }
            getListMaterials();
            adapter.notifyDataSetChanged();
    }
}
