package com.example.invfisico;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.invfisico.DB.DBHelper;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class AsignarMaterialActivity extends AppCompatActivity {

    public static SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs";
    private ListView MaterialesList;
    EditText et_material, et_location;
    public String Authorization="",UrlApi="",ContentType="";

    public static final String ADDMATERIAL= "ADDMATERIAL";
    public GlobalClass global;
    Button btn_cleanTextView;
    View btn_saveMaterial;
    public ProgressBar progressBar;

    LinkedList<String> materiales = new LinkedList<String>();
    LinkedList<String> locations = new LinkedList<String>();
    ArrayAdapter<String> adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asignarmaterial);
        getSupportActionBar().hide();
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        MaterialesList = (ListView) findViewById(R.id.MaterialesList);
        btn_cleanTextView = findViewById(R.id.btn_cleanTextView);
        btn_saveMaterial = findViewById(R.id.btn_saveMaterial);
        et_material=findViewById(R.id.et_material);
        et_location=findViewById(R.id.et_location);
        Authorization=new GlobalClass(getApplicationContext()).getApiAuth();
        UrlApi=new GlobalClass(getApplicationContext()).getUrlApi();
        ContentType=new GlobalClass(getApplicationContext()).getContentType();
        global = new GlobalClass(getApplicationContext());

        Gson gson = new Gson();

        //getLocations();
        getListMaterials();
        DisableMaterial();
        KeyBoard();

        if(!et_location.getText().toString().equals("")){
            System.out.println("EN IGUAL");
            EnableMaterial();
        }else{
            System.out.println("EN NO IGUAL");
            et_location.requestFocus();
        }

        et_location.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    String loc_ipt = et_location.getText().toString();
                    if (!loc_ipt.equals("")) {
                        if(!global.isNetworkConnected()){
                            Toast.makeText(getApplicationContext(), global.getnotNetworkMessage(), Toast.LENGTH_LONG).show();
                        }else {
                            DialogActivity dg = new DialogActivity(AsignarMaterialActivity.this);
                            dg.startLoadingDialog();
                            String url=global.getUrlApi()+"validateLocations/";
                            StringRequest request = new StringRequest(Request.Method.POST,
                                    url,
                                    response -> {
                                        JSONObject obj = null;
                                        try {
                                            System.out.println("response");
                                            System.out.println(response);
                                            obj = new JSONObject(response);
                                            if (obj.get("VALIDATE").toString().equals("1")) { // SI ES CORRECTO validate=1
                                                et_location.setEnabled(false);
                                                EnableMaterial();

                                            }else {
                                                Toast.makeText(getApplicationContext(), obj.get("MSG").toString(), Toast.LENGTH_LONG).show();
                                                et_location.getText().clear();
                                                et_location.requestFocus();
                                            }
                                            new android.os.Handler().postDelayed(
                                                    new Runnable() {
                                                        public void run() {
                                                            dg.dismissDialog();
                                                        }
                                                    }, 1000);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }, error -> {
                            }){
                                @Override
                                protected Map<String, String> getParams() throws AuthFailureError {
                                    Map<String,String> params = new HashMap<>();
                                    params.put("SUCURSAL",sharedpreferences.getString(global.getKeyuserSucursal(), ""));
                                    params.put("TOKEN", global.getTokenAuthApi());
                                    params.put("VALUE", loc_ipt);
                                    return params;

                                }

                            };
                            Volley.newRequestQueue(getApplicationContext()).add(request);

                        }



                    }else{
                        EnableLocation();
                    }
                }
                return false;
            }
        });
        et_material.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {

                    String mat_ipt = et_material.getText().toString();
                    String loc_ipt = et_location.getText().toString();
                    if (!mat_ipt.equals("")) {
                        addMaterial(loc_ipt,mat_ipt);
                        EnableMaterial();
                        FocusMaterial();
                        et_material.requestFocus();
                    }else{
                        Toast.makeText(getApplicationContext(),"Debe ingresar un material",Toast.LENGTH_SHORT).show();
                        et_material.requestFocus();
                        FocusMaterial();
                    }

                }

                return false;
            }

        });
        btn_saveMaterial.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                    new AlertDialog.Builder(AsignarMaterialActivity.this)
                            .setTitle("¿ENVIAR MATERIALES ASIGNADOS?")
                            .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    saveAllMateriales();
                                }
                            }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).create().show();
            }
        });

        MaterialesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                LayoutInflater inflater = getLayoutInflater();
                final View vie = inflater.inflate(R.layout.activity_asignacion_det, null);
                final AlertDialog.Builder builder =  new AlertDialog.Builder(AsignarMaterialActivity.this);
                builder.setView(vie);
                final ImageView icon_close = vie.findViewById(R.id.icon_close); /* Initialice variables de vista llamada */
                final Button btn_delete_asig_material = vie.findViewById(R.id.btn_delete_asig_material);
                final TextView title_codigo = vie.findViewById(R.id.title_codigo);
                final TextView title_eans = vie.findViewById(R.id.title_eans);
                final TextView title_descripcion = vie.findViewById(R.id.title_det);


                String c_as=getMaterial(position,title_codigo,title_eans,title_descripcion);

                final AlertDialog popud = builder.show();

                btn_delete_asig_material.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        new AlertDialog.Builder(AsignarMaterialActivity.this)
                                .setTitle("¿BORRAR "+c_as+"?")
                                .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        removeMaterial(c_as);
                                        adapter.notifyDataSetChanged();
                                    }
                                }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                }).create().show();

                        popud.dismiss();
                    }
                });

                icon_close.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        popud.dismiss();
                    }
                });
            }
        });


        btn_cleanTextView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                new AlertDialog.Builder(AsignarMaterialActivity.this)
                        .setTitle("¿NUEVA UBICACIÓN?")
                        .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                removeAllMaterial();
                                adapter.notifyDataSetChanged();
                                EnableLocation();
                                FocusLocation();
                            }
                        }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).create().show();

            }
        });
    }

    public void addMaterial(String location,String material){

        Gson gson = new Gson();
        DialogActivity dg = new DialogActivity(AsignarMaterialActivity.this);
        dg.startLoadingDialog();
        if(!global.isNetworkConnected()){

            FocusMaterial();
            DBHelper dbHelper = new DBHelper(getApplicationContext());
            SQLiteDatabase db=dbHelper.getReadableDatabase();
            String query = "SELECT * FROM "+global.getTbl_asig_locaciones()+ " WHERE locacion='"+location.toString()+"' and (codas='"+material+"' or codean='"+material+"')";
            Cursor c=(db.rawQuery(query,null));

            if(c.getCount()==0) {
                ContentValues values = new ContentValues();
                if(material.length()<=6){ //SI ES COD AS
                    values.put("codas",material.trim());
                    values.put("codean","");
                    values.put("indicador","0");
                }else{
                    values.put("codean",material.trim());
                    values.put("codas","");
                    values.put("indicador","1");
                }
                values.put("locacion",location.trim());
                values.put("descripcion","SIN DESCRIPCIÓN");
                db.insert(global.getTbl_asig_locaciones(),null,values);
                getListMaterials();
                Toast.makeText(getApplicationContext(), global.getnotNetworkMessage(), Toast.LENGTH_LONG).show();
                new android.os.Handler().postDelayed(
                        new Runnable() {
                            public void run() {
                                dg.dismissDialog();FocusMaterial();
                            }
                        }, 1000);
            }else{

                FocusMaterial();
                et_material.requestFocus();
                Toast.makeText(getApplicationContext(), material+" ya fueeeee escaneado antes ", Toast.LENGTH_LONG).show();
                new android.os.Handler().postDelayed(
                        new Runnable() {
                            public void run() {
                                dg.dismissDialog();FocusMaterial();
                            }
                        }, 1000);
            }

        }else{
            DBHelper dbHelper = new DBHelper(getApplicationContext());
            SQLiteDatabase db=dbHelper.getReadableDatabase();
            String query = "SELECT * FROM "+global.getTbl_asig_locaciones()+ " WHERE locacion='"+location+"' and (codas='"+material+"' or codean='"+material+"')";
            Cursor c=(db.rawQuery(query,null));

            if(c.getCount()==0){
                String url=global.getUrlApi()+"showProduct/";
                StringRequest request = new StringRequest(Request.Method.POST,
                        url,
                        response -> {

                            JSONObject obj = null;
                            try {
                                obj = new JSONObject(response);
                                ContentValues values = new ContentValues();
                                if (obj.get("VALIDATE").toString().equals("1")) { // SI ES CORRECTO validate=1

                                    JSONObject data = new JSONObject(obj.get("DATA").toString());
                                    if(material.length()<=6){ //SI ES COD AS
                                        values.put("codas",material.trim());
                                        values.put("codean","");
                                        values.put("indicador","0");
                                    }else{
                                        values.put("codean",material.trim());
                                        values.put("codas",data.get("CODIGO_AS").toString().trim());
                                        values.put("indicador","1");
                                    }
                                    values.put("locacion",location.trim());
                                    values.put("descripcion",data.get("DESCRIPCION").toString().trim());
                                    db.insert(global.getTbl_asig_locaciones(),null,values);

                                }else if(obj.get("VALIDATE").toString().equals("2")){
                                    Toast.makeText(getApplicationContext(), material+ " asignado antes",Toast.LENGTH_LONG).show();
                                }else{
                                    if(material.length()<=6){ //SI ES COD AS
                                        values.put("codas",material.trim());
                                        values.put("codean","");
                                        values.put("indicador","0");
                                    }else{
                                        values.put("codean",material.trim());
                                        values.put("codas","");
                                        values.put("indicador","1");
                                    }
                                    values.put("locacion",location.trim());
                                    values.put("descripcion","SIN DESCRIPCIÓN");
                                    db.insert(global.getTbl_asig_locaciones(),null,values);
                                }
                                getListMaterials();
                                new android.os.Handler().postDelayed(
                                        new Runnable() {
                                            public void run() {

                                                FocusMaterial();

                                                dg.dismissDialog();
                                            }
                                        }, 0);



                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }, error -> {
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String,String> params = new HashMap<>();
                        params.put("SUCURSAL",sharedpreferences.getString(global.getKeyuserSucursal(), ""));
                        params.put("MATERIAL",material);
                        params.put("LOCATION",location);
                        params.put("TOKEN",global.getTokenAuthApi());
                        params.put("USERNAME",sharedpreferences.getString(global.getKeyuser(), ""));

                        return params;

                    }
                };
                Volley.newRequestQueue(getApplicationContext()).add(request);

            }else{


                et_material.requestFocus();
                Toast.makeText(getApplicationContext(), material+" ya fue escaneado antes ", Toast.LENGTH_LONG).show();


            }
            FocusMaterial();
        }


    }
    public void KeyBoard(){
        View view= this.getCurrentFocus();
        if(view != null){
            InputMethodManager inn=(InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            inn.hideSoftInputFromWindow(view.getWindowToken(),0);
        }
    }
    public void FocusMaterial(){
        System.out.println("print");
        et_material.getText().clear();
        et_material.requestFocus();
        KeyBoard();
    }
    public void FocusLocation(){
        et_location.getText().clear();
        et_location.requestFocus();
    }
    public void DisableLocation(){
        et_location.setEnabled(false);
    }
    public void EnableLocation(){
        et_location.setEnabled(true);
        et_location.requestFocus();
    }
    public void DisableMaterial(){
        et_material.setEnabled(false);
    }
    public void EnableMaterial(){
        et_material.setEnabled(true);
        et_material.requestFocus();
    }

    public void getListMaterials() {
        materiales.removeAll(materiales);
        DBHelper dbHelper = new DBHelper(getApplicationContext());
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = "SELECT id,locacion,descripcion,codas,codean FROM " + global.getTbl_asig_locaciones()+ " order by id desc";

        Cursor c = (db.rawQuery(query, null));
        String r_loc="";
        if (c.getCount() > 0) {
            if (c.moveToFirst()) {
                do {
                    String r_id = c.getString(                                      0).trim();
                    r_loc = c.getString(1).trim();
                    String r_descripcion = c.getString(2).trim();
                    String r_as = c.getString(3).toString().trim();
                    String r_ean = c.getString(4).toString().trim();
                    if(r_as.equals("")){
                        if(materiales.indexOf(r_ean + " | " + r_descripcion)==-1){
                            materiales.add(r_ean + " | " + r_descripcion);
                        }
                    }else{
                        if(materiales.indexOf(r_as + " | " + r_descripcion)==-1){
                            materiales.add(r_as + " | " + r_descripcion);
                        }
                    }

                } while (c.moveToNext());
            }
            et_location.setText(r_loc);
            et_location.setEnabled(false);
            adapter= new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,materiales);
            MaterialesList.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }
    }
    public void getLocations() {
        locations.removeAll(locations);
        DBHelper dbHelper = new DBHelper(getApplicationContext());
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = "SELECT id,locacion FROM " + global.getTbl_all_location()+ " order by id desc";

        Cursor c = (db.rawQuery(query, null));
        if (c.getCount() > 0) {
            if (c.moveToFirst()) {
                do {
                    String r_id = c.getString(0).trim();
                    String r_loc = c.getString(1).trim();
                    locations.add(r_loc);
                } while (c.moveToNext());
            }
        }
    }

    public String getMaterial(int pos, TextView title_codigo, TextView title_eans, TextView title_descripcion){
        DBHelper dbHelper = new DBHelper(getApplicationContext());
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String itemSelected=adapter.getItem(pos).toString().trim();
        String item[]=itemSelected.split("\\|"); //"EAN: 54545 | D: "
        String cod=item[0].trim();
        String query="";
        if (cod.length()<=6) {
            query = "SELECT * FROM " + global.getTbl_asig_locaciones()+ " where codas='"+cod+"'";
        }else{
            query = "SELECT * FROM " + global.getTbl_asig_locaciones()+ " where codean='"+cod+"'";
        }
        Cursor c = (db.rawQuery(query, null));
        String r_descripcion="";
        String r_ean="";
        String r_as="";
        int i=0;
        if (c.getCount() > 0) {
            if (c.moveToFirst()) {
                do {

                    if(i==0){
                        r_ean = c.getString(3).trim();
                    }else{
                        r_ean =  r_ean +", "+ c.getString(3).trim();
                    }
                    r_descripcion = c.getString(2).trim();
                    r_as = c.getString(4).toString().trim();
                    i++;

                } while (c.moveToNext());
            }
            title_codigo.setText(r_as);
            title_eans.setText(r_ean);
            title_descripcion.setText(r_descripcion);

            adapter= new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,materiales);
            MaterialesList.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }
        return cod;
    }
    public String getAllMaterial(){
        DBHelper dbHelper = new DBHelper(getApplicationContext());
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String query = "SELECT * FROM " + global.getTbl_asig_locaciones();
        Cursor c = (db.rawQuery(query, null));
        String r_as="";
        ArrayList <String> list_as=new ArrayList<>();

        int i=0;
        if (c.getCount() > 0) {
            if (c.moveToFirst()) {
                do {
                    if(i==0){
                        r_as = c.getString(4).trim()+"-"+c.getString(3).trim()+"-"+c.getString(5).trim();
                    }else{
                        r_as = r_as +","+c.getString(4).trim()+"-"+c.getString(3).trim()+"-"+c.getString(5).trim();
                    }
                    i++;
                } while (c.moveToNext());
            }
        }
        System.out.println("r_as");
        System.out.println(r_as);
        /*if (c.getCount() > 0) {
            if (c.moveToFirst()) {
                do {
                    if(i==0){
                        if(c.getString(4).trim().equals("")){
                            r_as = c.getString(3).trim();
                            list_as.add(c.getString(3));
                        }else{
                            r_as = c.getString(4).trim();
                            list_as.add(c.getString(4));
                        }
                    }else{
                        if(c.getString(4).trim().equals("")){
                            if(list_as.indexOf(c.getString(3))==-1){
                                list_as.add(c.getString(3));
                                r_as = r_as +","+c.getString(3).trim();
                            }
                        }else{
                            if(list_as.indexOf(c.getString(4))==-1){
                                list_as.add(c.getString(4));
                                r_as =  r_as +","+ c.getString(4).trim();
                            }
                        }
                    }

                    i++;

                } while (c.moveToNext());
            }
        }*/
        return r_as;
    }
    public void saveAllMateriales(){
        if(materiales.size()>0){
            if(!global.isNetworkConnected()){
                Toast.makeText(getApplicationContext(), global.getnotNetworkMessage(), Toast.LENGTH_LONG).show();
            }else{
                DialogActivity dg = new DialogActivity(AsignarMaterialActivity.this);
                dg.startLoadingDialog();
                String url=global.getUrlApi()+"saveLocationMaterial/";
                StringRequest request = new StringRequest(Request.Method.POST,
                        url,
                        response -> {
                            System.out.println("response");
                            System.out.println(response);
                            JSONObject obj = null;

                            try {
                                obj = new JSONObject(response);
                                if (obj.get("VALIDATE").toString().equals("1")) { // SI ES CORRECTO validate=1
                                    removeAllMaterial();
                                    getListMaterials();
                                    EnableLocation();
                                    FocusLocation();
                                    adapter.notifyDataSetChanged();
                                    Toast.makeText(AsignarMaterialActivity.this,obj.get("MSG").toString(),Toast.LENGTH_SHORT).show();

                                }else{
                                    Toast.makeText(getApplicationContext(), obj.get("MSG").toString(), Toast.LENGTH_LONG).show();
                                }
                                new android.os.Handler().postDelayed(
                                        new Runnable() {
                                            public void run() {
                                                dg.dismissDialog();
                                                dg.startSuccessDialog();
                                                new android.os.Handler().postDelayed(
                                                        new Runnable() {
                                                            public void run() {
                                                                dg.dismissDialog();
                                                            }
                                                        }, 2000);

                                            }
                                        }, 1000);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }, error -> {
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String,String> params = new HashMap<>();
                        System.out.println(sharedpreferences);
                        System.out.println(sharedpreferences.getString(global.getKeyuserCodTienda(), ""));
                        params.put("SUCURSAL",sharedpreferences.getString(global.getKeyuserSucursal(), ""));
                        params.put("LOCATION",et_location.getText().toString());
                        params.put("MATERIAL",getAllMaterial());
                        params.put("USERNAME",sharedpreferences.getString(global.getKeyuser(), ""));
                        params.put("CODTIENDA",sharedpreferences.getString(global.getKeyuserCodTienda(), ""));
                        params.put("TOKEN",global.getTokenAuthApi());
                        return params;

                    }
                /*@Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Content-Type", ContentType);
                    headers.put("Authorization", Authorization);
                    return headers;
                 }*/
                };
                Volley.newRequestQueue(getApplicationContext()).add(request);
            }

        }else{
            Toast.makeText(getApplicationContext(), "No hay materiales para asignar", Toast.LENGTH_LONG).show();

        }
    }




    public void removeMaterial(String cod){
        DBHelper dbHelper = new DBHelper(getApplicationContext());
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        if(cod.length()<=6){
            db.execSQL("DELETE FROM "+global.getTbl_asig_locaciones()+" where codas='"+cod+"'");
        }else{
            db.execSQL("DELETE FROM "+global.getTbl_asig_locaciones()+" where codean='"+cod+"'");
        }
        DialogActivity dg = new DialogActivity(AsignarMaterialActivity.this);
        dg.startLoadingDialog();
        getListMaterials();
        adapter.notifyDataSetChanged();
        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        Toast.makeText(getApplicationContext(), "Se borró correctamente", Toast.LENGTH_LONG).show();

                        dg.dismissDialog();
                    }
                }, 1000);

    }
    public void removeAllMaterial(){
        DialogActivity dg = new DialogActivity(AsignarMaterialActivity.this);
        dg.startLoadingDialog();
        DBHelper dbHelper = new DBHelper(getApplicationContext());
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        db.execSQL("DELETE FROM "+global.getTbl_asig_locaciones());
        getListMaterials();
        adapter.notifyDataSetChanged();
        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        materiales.removeAll(materiales);
                        dg.dismissDialog();
                    }
                }, 1000);

    }

}
