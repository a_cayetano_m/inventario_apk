package com.example.invfisico;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
public class DeleteLocationActivity extends AppCompatActivity {

    int PERMISSION_ID = 1001;
    protected String Authorization="Basic VVNSQVBJMDAxOmt5MDF0bDIk";
    public static String id_client;
    ListView ListLocations;
    SearchView SearchLocations;
    ArrayList<String> locations;
    ArrayAdapter<String> adapter;
    Button btn_deleteAllLocation;
    /*@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deletelocation);
        getSupportActionBar().hide();

        SearchLocations=findViewById(R.id.search_locations);
        ListLocations = findViewById(R.id.list_locations);
        btn_deleteAllLocation = findViewById(R.id.btn_deleteAllLocation);
        locations = new ArrayList<String>();

        showLocations();

        btn_deleteAllLocation.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                new AlertDialog.Builder(DeleteLocationActivity.this)
                        .setTitle("¿Borrar todas las locaciones?")
                        .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                deleteAllLocation();
                            }
                        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).create().show();
            }
        });
        ListLocations.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("CLICK: "+locations.get(position));
            }
        });
        ListLocations.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                new AlertDialog.Builder(DeleteLocationActivity.this)
                        .setTitle("¿Borrar "+locations.get(position)+" ?")
                                .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        removeLocation(position);
                                    }
                                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).create().show();
                return false;
            }
        });

        adapter= new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,locations);
        System.out.println(locations);
        ListLocations.setAdapter(adapter);

        SearchLocations.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                adapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return false;
            }
        });


    }
    public void removeLocation(int remove){
        locations.remove(remove);
        adapter.notifyDataSetChanged();
    }
    public void deleteAllLocation(){
        locations.removeAll(locations);
        adapter.notifyDataSetChanged();
    }
    public void showLocations(){
        locations.add("A01-A01");
        locations.add("A02-A02");
        locations.add("A03-A03");
        locations.add("A04-A04");
        locations.add("A05-A05");
        locations.add("A06-A06");
        locations.add("A07-A07");
        locations.add("A08-A08");
        locations.add("A09-A09");

    }*/

}
