package com.example.invfisico;

import android.content.Context;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;

import android.content.SharedPreferences;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

public class LoginActivity extends AppCompatActivity{

    Button btn_l;
    EditText et_username, et_password;
    public static SharedPreferences sharedpreferences;
    public String Authorization="",UrlApi="",ContentType="";

    public static final String MyPREFERENCES = "MyPrefs";
    public static final String LoginStatus = "false";
    public static final String KEY_LOGIN = "LOGIN";
    public GlobalClass global;
    public static Boolean logeado = false ;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();
        global = new GlobalClass(getApplicationContext());
        Authorization=global.getApiAuth();
        UrlApi=global.getUrlApi();
        ContentType=global.getContentType();


        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        btn_l=findViewById(R.id.btn_login);
        et_username=findViewById(R.id.et_username);
        et_password=findViewById(R.id.et_password);

        logeado=isLoggin();
        if(logeado){
            Intent i =  new Intent(getApplicationContext(), PrincipalActivity.class);
            switch (sharedpreferences.getString(global.getKeyuserType(),"")) {
                case "1":{ // ALMACENERO
                    i =  new Intent(getApplicationContext(), PrincipalActivity.class);
                    break;
                }
                case "2":{ // INVENTARIO
                    i =  new Intent(getApplicationContext(), Principal2Activity.class);
                    break;
                }
            }

            finish();
            startActivity(i);
        }else {

            btn_l = findViewById(R.id.btn_login);
            et_username = findViewById(R.id.et_username);
            et_password = findViewById(R.id.et_password);
            btn_l.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(!global.isNetworkConnected()) {
                        Toast.makeText(getApplicationContext(), global.getnotNetworkMessage(), Toast.LENGTH_LONG).show();
                    }else{
                        if(!et_username.getText().toString().equals("") && !et_password.getText().toString().equals("") ){
                            login();
                        }else{
                            Toast.makeText(getApplicationContext(), "Ingresar Usuario y Contraseña", Toast.LENGTH_LONG).show();
                        }
                    }
                }
            });
        }   }
    public boolean isLoggin() {
        return sharedpreferences.getBoolean(LoginStatus, false);
    }

    public void login(){
        String url=global.getUrlApi()+"getAuth/";
        AtomicReference<String> errorrespuesta = new AtomicReference<>("0");
        StringRequest request = new StringRequest(Request.Method.POST,
                url,
                response -> {
                    System.out.println("respuesta usuario");
                    System.out.println(response);
                    JSONObject obj = null;
                    String correcto = "0";
                    try {
                        obj = new JSONObject(response);
                        if (obj.get("VALIDATE").toString().equals("1")) { // SI EXISTE EL USUARIO validate=1
                            correcto = "1";
                            //Toast.makeText(getApplicationContext(), "sos un capo", Toast.LENGTH_LONG).show();
                            if (obj.get("SESION").toString().equals("0")) {
                                System.out.println(obj.get("SUCURSAL").toString());
                                Gson gson = new Gson();
                                SharedPreferences.Editor editor = sharedpreferences.edit();
                                editor.putString(global.getKeyuserId(), obj.get("ID").toString());
                                editor.putString(global.getKeyuser(), obj.get("USERNAME").toString());
                                editor.putString(global.getKeyuserName(), obj.get("NAME").toString());
                                editor.putString(global.getKeyuserLastName(), obj.get("LASTNAME").toString());
                                editor.putString(global.getKeyuserEmail(), obj.get("EMAIL").toString());
                                editor.putString(global.getKeyuserType(), obj.get("TYPE").toString());
                                editor.putString(global.getKeyuserSucursal(), obj.get("SUCURSAL").toString());
                                editor.putString(global.getKeyuserCodTienda(), obj.get("CODTIENDA").toString());
                                editor.putString(global.getKeyuserDesTienda(), obj.get("DESTIENDA").toString());
                                editor.putString(global.getKeyuserZonas(), obj.get("ZONAS").toString());
                                editor.putString(global.getKeyuserMuebles(), obj.get("MUEBLES").toString());
                                editor.putString(global.getKeyuserNivel(), obj.get("NIVEL").toString());
                                editor.putString(global.getKeyuserSeparacion(), obj.get("SEPARACION").toString());
                                editor.putString(global.getKeyuserConteos(), obj.get("CONTEOS").toString()); //ES EL NUMERO DE CONTEO EN EL QUE SE ENCUENTRA EL PROCESO DE INVENTARIO
                                editor.putBoolean(LoginStatus, true);
                                editor.commit();
                                //--FIN LOGIN PASS
                                System.out.println("TYPE");
                                System.out.println(obj.get("TYPE").toString());

                                Intent i = new Intent(getApplicationContext(), PrincipalActivity.class);
                                switch (obj.get("TYPE").toString()) {
                                    case "1": { // ALMACENERO
                                        i = new Intent(getApplicationContext(), PrincipalActivity.class);
                                        finish();
                                        startActivity(i);
                                        break;
                                    }
                                    case "2": { // INVENTARIO
                                        i = new Intent(getApplicationContext(), Principal2Activity.class);
                                        finish();
                                        startActivity(i);
                                        break;
                                    }
                                    default: {
                                        Toast.makeText(getApplicationContext(), "Usuario y/o Contraseña Incorrecta", Toast.LENGTH_LONG).show();
                                        break;
                                    }
                                }
                            } else {
                                Toast.makeText(getApplicationContext(), "Usuario Ya Inicio Sesion en otro Dispositivo", Toast.LENGTH_LONG).show();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (correcto == "0"){
                        Toast.makeText(getApplicationContext(), "Usuario y/o Contraseña Incorrecta", Toast.LENGTH_LONG).show();
                    }
                }, error -> {
            System.out.println("Error2");
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("USERNAME",et_username.getText().toString());
                params.put("PASSWORD",et_password.getText().toString());
                params.put("TOKEN", global.getTokenAuthApi());
                return params;
            }
        };
        Volley.newRequestQueue(this).add(request);
    }



}
