package com.example.invfisico;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.invfisico.DB.DBHelper;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class AddLocationActivity extends AppCompatActivity{


    public String Authorization="",UrlApi="",ContentType="";
    public Button btn_sendLocation,btn_addLocation;
    public static SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs";
    Spinner sp_zona,sp_muebles,sp_nivel,sp_separacion;
    ArrayAdapter<String> adapter;
    ArrayAdapter<String> adapterLetters;
    ArrayAdapter<String> adapterNumber;
    private ListView LocationList;
    LinkedList<String> locations = new LinkedList<String>(); // LAS NUEVAS LOCACIONES
    public String locationsPreferer;
    public GlobalClass global;
    public String location_send="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addlocation);
        getSupportActionBar().hide();

        Authorization=new GlobalClass(getApplicationContext()).getApiAuth();
        UrlApi=new GlobalClass(getApplicationContext()).getUrlApi();
        ContentType=new GlobalClass(getApplicationContext()).getContentType();
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        global = new GlobalClass(getApplicationContext());


        LocationList = (ListView) findViewById(R.id.AddLocationList);
        sp_zona = (Spinner) findViewById(R.id.sp_zona);
        sp_nivel = (Spinner) findViewById(R.id.sp_nivel);
        sp_muebles = (Spinner) findViewById(R.id.sp_muebles);
        sp_separacion = (Spinner) findViewById(R.id.sp_separacion);

        String ZonasPreferer=sharedpreferences.getString(global.getKeyuserZonas(), "");
        String NivelPreferer=sharedpreferences.getString(global.getKeyuserNivel(), "");
        String MueblePreferer=sharedpreferences.getString(global.getKeyuserMuebles(), "");
        String SeparacionPreferer=sharedpreferences.getString(global.getKeyuserSeparacion(), "");

        LinkedList<String> ZonasPreferer_general = new LinkedList<String>(Arrays.asList((String[])ZonasPreferer.split(",")));
        LinkedList<String> NivelPreferer_general = new LinkedList<String>(Arrays.asList((String[])NivelPreferer.split(",")));
        LinkedList<String> MueblePreferer_general = new LinkedList<String>(Arrays.asList((String[])MueblePreferer.split(",")));
        LinkedList<String> SeparacionPreferer_general = new LinkedList<String>(Arrays.asList((String[])SeparacionPreferer.split(",")));

        if(ZonasPreferer_general!=null){
            ZonasPreferer_general.addFirst("");
            adapterLetters= new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item,ZonasPreferer_general);
            sp_zona.setAdapter(adapterLetters);
        }
        if(NivelPreferer_general!=null){
            NivelPreferer_general.addFirst("");
            adapterNumber= new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item,NivelPreferer_general);
            sp_nivel.setAdapter(adapterNumber);
        }
        if(MueblePreferer_general!=null){
            MueblePreferer_general.addFirst("");
            adapterLetters= new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item,MueblePreferer_general);
            sp_muebles.setAdapter(adapterLetters);
        }
        if( SeparacionPreferer_general!=null){
            SeparacionPreferer_general.addFirst("");
            adapterNumber= new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item,SeparacionPreferer_general);
            sp_separacion.setAdapter(adapterNumber);
        }


        getListLocations();
        /*ArrayAdapter<CharSequence> adapter_letras=ArrayAdapter.createFromResource(this,R.array.spinner_letters, android.R.layout.simple_spinner_item);
        adapter_letras.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_zona.setAdapter(adapter_letras);
        sp_nivel.setAdapter(adapter_letras);

        ArrayAdapter<CharSequence> adapter_numeros=ArrayAdapter.createFromResource(this,R.array.spinner_numeros, android.R.layout.simple_spinner_item);
        adapter_numeros.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_muebles.setAdapter(adapter_numeros);
        sp_separacion.setAdapter(adapter_numeros);*/

        btn_sendLocation = findViewById(R.id.btn_sendLocation);
        btn_addLocation = findViewById(R.id.btn_addLocation);



        btn_addLocation.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                String location=sp_zona.getSelectedItem().toString()+sp_muebles.getSelectedItem().toString()+"-"+sp_nivel.getSelectedItem().toString()+sp_separacion.getSelectedItem().toString();
                if(!isEmptyValues()) {
                    new AlertDialog.Builder(AddLocationActivity.this)
                            .setTitle("¿CREAR "+location+"?")
                            .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    addLocation(location);
                                    getListLocations();
                                    cleanValues();
                                }
                            }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).create().show();
                }else{
                    Toast.makeText(getApplicationContext(), "Debe ingresar ubicación", Toast.LENGTH_LONG).show();
                }


            }
        });
        btn_sendLocation.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                createLocation();
            }
        });

        Gson gson = new Gson();
        String locationsPrefererNew=sharedpreferences.getString(global.getKeyuserNewLocations(), "");
        String locationsPreferer=sharedpreferences.getString(global.getKeyuserLocations(), "");
        LinkedList locNew = gson.fromJson(locationsPrefererNew, LinkedList.class);
        if(locNew!=null){
            locations=locNew;
        }

        adapter= new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,locations);
        LocationList.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        LocationList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int pos, long id) {
                new AlertDialog.Builder(AddLocationActivity.this)
                        .setTitle("¿BORRAR "+locations.get(pos).toString()+"?")
                        .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                removeLocation(locations.get(pos).toString());
                                Toast.makeText(getApplicationContext(), "Se borró correctamente", Toast.LENGTH_LONG).show();
                                getListLocations();
                            }
                        }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).create().show();

                return true;
            }
        });
    }
    public void addLocation(String location){

        DBHelper dbHelper = new DBHelper(getApplicationContext());
        SQLiteDatabase db=dbHelper.getReadableDatabase();
        String query = "SELECT * FROM "+global.getTbl_location()+ " WHERE locacion='"+location+"'";
        Cursor c=(db.rawQuery(query,null));
        DialogActivity dg = new DialogActivity(AddLocationActivity.this);
        dg.startLoadingDialog();
        if(c.getCount()==0) {
                ContentValues values = new ContentValues();
                values.put("locacion",location.trim());
                db.insert(global.getTbl_location(),null,values);
        }else{
            Toast.makeText(getApplicationContext(), "Ubicación existente", Toast.LENGTH_LONG).show();
        }
        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        dg.dismissDialog();
                    }
                }, 1000);
    }
    public void getListLocations() {
        locations.removeAll(locations);
        DBHelper dbHelper = new DBHelper(getApplicationContext());
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = "SELECT id,locacion FROM " + global.getTbl_location()+ " order by id desc";

        Cursor c = (db.rawQuery(query, null));
        if (c.getCount() > 0) {
            if (c.moveToFirst()) {
                do {
                    String r_id = c.getString(0).trim();
                    String r_loc = c.getString(1).trim();
                    locations.add(r_loc);
                } while (c.moveToNext());
            }
            adapter= new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,locations);
            LocationList.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }
    }

    public void createLocation(){

        if (locations.size()>0){
            new AlertDialog.Builder(AddLocationActivity.this)
                    .setTitle("¿ENVIAR UBICACIONES?")
                    .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if(!global.isNetworkConnected()){
                                Toast.makeText(AddLocationActivity.this, global.getnotNetworkMessage(), Toast.LENGTH_LONG).show();
                            }else{

                                Integer i=0;
                                location_send="";
                                for (String l : locations) {
                                    if(i==0){
                                        location_send=location_send+l;
                                    }else{
                                        location_send=location_send+","+l;
                                    }
                                    i++;
                                }

                                if(location_send.length()>0){
                                    DialogActivity dg = new DialogActivity(AddLocationActivity.this);
                                    dg.startLoadingDialog();

                                    String url=global.getUrlApi()+"createLocation/";
                                    StringRequest request = new StringRequest(Request.Method.POST,
                                            url,
                                            response -> {
                                                JSONObject obj = null;
                                                try {

                                                    obj = new JSONObject(response);
                                                    if (obj.get("VALIDATE").toString().equals("1")) { // SI ES CORRECTO validate=1

                                                        Toast.makeText(AddLocationActivity.this, obj.get("MSG").toString(), Toast.LENGTH_LONG).show();
                                                        removeAllLocation();
                                                        getListLocations();
                                                    }else{
                                                        Toast.makeText(AddLocationActivity.this, obj.get("MSG").toString(), Toast.LENGTH_LONG).show();
                                                    }
                                                    new Handler().postDelayed(
                                                            new Runnable() {
                                                                public void run() {
                                                                    dg.dismissDialog();
                                                                    dg.startSuccessDialog();
                                                                    new Handler().postDelayed(
                                                                            new Runnable() {
                                                                                public void run() {
                                                                                    dg.dismissDialog();
                                                                                }
                                                                            }, 2000);

                                                                }
                                                            }, 1000);


                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }, error -> {
                                    }){
                                        @Override
                                        protected Map<String, String> getParams() throws AuthFailureError {
                                            Map<String,String> params = new HashMap<>();
                                            params.put("LOCATION",location_send);
                                            params.put("SUCURSAL",sharedpreferences.getString(global.getKeyuserSucursal(), ""));
                                            params.put("USERNAME",sharedpreferences.getString(global.getKeyuser(), ""));
                                            params.put("TOKEN", global.getTokenAuthApi());
                                            return params;

                                        }

                                    };
                                    Volley.newRequestQueue(getApplicationContext()).add(request);
                                }else{
                                    Toast.makeText(getApplicationContext(), "Debe ingresar una ubicación", Toast.LENGTH_LONG).show();
                                }

                            }
                        }
                    }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).create().show();

        }else{
            Toast.makeText(getApplicationContext(), "Debe ingresar una ubicación", Toast.LENGTH_LONG).show();
        }

    }
    public void orderLocationList(){
        Collections.sort(locations, Collections.reverseOrder());
    }
    public void removeLocation(String loc){
        DBHelper dbHelper = new DBHelper(getApplicationContext());
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        db.execSQL("DELETE FROM "+global.getTbl_location()+" where locacion='"+loc+"'");
    }
    public void removeAllLocation(){
        DBHelper dbHelper = new DBHelper(getApplicationContext());
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        db.execSQL("DELETE FROM "+global.getTbl_location());
        adapter.notifyDataSetChanged();
    }
    public Boolean isEmptyValues(){
        String z= sp_zona.getSelectedItem().toString();
        String m=sp_muebles.getSelectedItem().toString();
        String n=sp_nivel.getSelectedItem().toString();
        String s=sp_separacion.getSelectedItem().toString();
        if(z.length()>0 && m.length()>0 && n.length()>0 && s.length()>0){
            return false;
        }else{
            return true;
        }

    }
    public void cleanValues(){
        sp_zona.setSelection(0);
        sp_muebles.setSelection(0);
        sp_nivel.setSelection(0);
        sp_separacion.setSelection(0);
    }
}
