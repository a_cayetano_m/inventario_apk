package com.example.invfisico;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.os.Handler;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.invfisico.DB.DBHelper;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.SQLOutput;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class Principal2Activity extends AppCompatActivity {

    public static SharedPreferences sharedpreferences;
    Button btn_create_inventario, btn_closeSession;
    public String Authorization="",UrlApi="",ContentType="";
    public GlobalClass global;
    String conteo="";
    ArrayAdapter<String> adapter;
    public static final String MyPREFERENCES = "MyPrefs";
    public static final String GETLOCATIONSDET= "GETLOCATIONSDET";
    public static final String LoginStatus = "false";
    public ProgressBar progressBarLoadata;
    public View cart_btn_loadata ;
    TextView userName;
    public View const_cart_btn_loadata ;
    //TextView title_user;
    String ConteoPreferer="";
    @SuppressLint("MissingInflatedId")
    @Override
    protected void onResume() {
        super.onResume();
        setContentView(R.layout.activity_principal2);
        getSupportActionBar().hide();

        Authorization=new GlobalClass(getApplicationContext()).getApiAuth();
        UrlApi=new GlobalClass(getApplicationContext()).getUrlApi();
        ContentType=new GlobalClass(getApplicationContext()).getContentType();
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        global = new GlobalClass(getApplicationContext());
        userName=findViewById(R.id.userName);
        String version = "V"+global.getVersion_sistema().toString();
        userName.setText(sharedpreferences.getString(global.getKeyuser(), "")+" "+version);
        btn_closeSession = findViewById(R.id.btn_closeSession);
        btn_create_inventario = findViewById(R.id.btn_create_inventario);

        ConteoPreferer=sharedpreferences.getString(global.getKeyuserConteos(), "");
        conteo=ConteoPreferer;
        switch (conteo) {
            case "1":
                getLocations();
                break;
            case "2":
                getLocations2();
                break;
            case "3":
                getLocations2();
                break;
        }
        System.out.println("Version del inventario");
        //title_user.setText("INV04601");
        //title_user.setText(global.getKeyuser());

        btn_create_inventario.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                switch (conteo) {
                    case "1":
                        CreateInventario();
                        break;
                    case "2":
                        CreateSecondInventario();
                        break;
                    case "3":
                        CreateSecondInventario();
                        break;
                }
            }
        });

        btn_closeSession.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                new AlertDialog.Builder(Principal2Activity.this)
                        .setTitle("¿Cerrar sesión?")
                        .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String user = sharedpreferences.getString(global.getKeyuser(), "");
                                String url=global.getUrlApi()+"closeSesion/";
                                StringRequest request = new StringRequest(Request.Method.POST,
                                        url,
                                        response -> {
                                            System.out.println("respuesta usuario");
                                            System.out.println(response);
                                            JSONObject obj = null;
                                            String correcto = "0";
                                        }, error -> {
                                    System.out.println("Error2");
                                }){
                                    @Override
                                    protected Map<String, String> getParams() throws AuthFailureError {
                                        Map<String,String> params = new HashMap<>();
                                        params.put("USERNAME",user.toString());
                                        params.put("TOKEN", global.getTokenAuthApi());
                                        return params;
                                    }
                                };
                                Volley.newRequestQueue(getApplicationContext()).add(request);
                                closeSession();


                            }
                        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).create().show();
            }
        });

        progressBarLoadata = findViewById(R.id.progressBarLoadata);
        cart_btn_loadata = findViewById(R.id.cart_btn_loadata);
        const_cart_btn_loadata = findViewById(R.id.const_cart_btn_loadata);
        progressBarLoadata.setVisibility(View.GONE);
        cart_btn_loadata.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View vi){
                if(!global.isNetworkConnected()){
                    Toast.makeText(getApplicationContext(), global.getnotNetworkMessage(), Toast.LENGTH_LONG).show();
                }else{
                    System.out.println(conteo);
                    switch (conteo){
                        case "1":
                            FirstCont();
                            break;
                        case "2":
                            SecondCont();
                            break;
                        case "3":
                            SecondCont();
                            break;
                    }

                }
            }
        });
    }
    public void DeleteTables(){
        DBHelper dbHelper = new DBHelper(getApplicationContext());
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.execSQL("DELETE FROM "+global.getTbl_all_location());
        db.execSQL("DELETE FROM "+global.getTbl_asig_locaciones());
        db.execSQL("DELETE FROM "+global.getTbl_location());
        db.execSQL("DELETE FROM "+global.getTbl_location_count());
        db.execSQL("DELETE FROM "+global.getTbl_location_inv());
        db.execSQL("DELETE FROM "+global.getTbl_material());
    }
    public void FirstCont(){
        switch (conteo) {
            case "1":
                getLocations();
                break;
            case "2":
                getLocations2();
                break;
            case "3":
                getLocations2();
                break;
        }
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Segunda línea de código (se ejecutará después de 1 segundo)
            }
        }, 1000); // 1000 milisegundos = 1 segundo
        progressBarLoadata.setVisibility(View.VISIBLE);
        String url=global.getUrlApi()+"getLocationMaterial/";
        StringRequest request = new StringRequest(Request.Method.POST,
                url,
                response -> {
                    JSONObject obj = null;
                    System.out.println(response);
                    try {
                        obj = new JSONObject(response);
                        progressBarLoadata.setVisibility(View.GONE);
                        if (obj.get("VALIDATE").toString().equals("1")) { // SI ES CORRECTO validate=1
                            JSONArray array = new JSONArray(obj.get("DATA").toString());
                            if(array.length()>0){
                                DBHelper dbHelper = new DBHelper(Principal2Activity.this);
                                SQLiteDatabase db=dbHelper.getWritableDatabase();
                                if(db !=null){
                                    long id=0;
                                    try {
                                        db.execSQL("DELETE FROM "+global.getTbl_material());
                                        for (int i = 0; i < array.length(); i++) {
                                            JSONObject row = array.getJSONObject(i);
                                            ContentValues values = new ContentValues();
                                            values.put("locacion",row.get("LOCACION").toString().trim());
                                            values.put("descripcion",row.get("DESCRIPCION").toString().trim());
                                            values.put("codas",row.get("COD_AS").toString().trim());
                                            values.put("codean",row.get("COD_EAN").toString().trim());
                                            values.put("factor",row.get("FACTOR").toString().trim());
                                            values.put("unidadbase",row.get("UNIDAD_BASE").toString().trim());
                                            values.put("indicador",row.get("INDICADOR").toString().trim());
                                            db.insert(global.getTbl_material(),null,values);
                                        }
                                        const_cart_btn_loadata.setBackgroundResource(R.color.tl_success);
                                        Toast.makeText(getApplicationContext(),"Carga completa",Toast.LENGTH_LONG).show();
                                    }catch (Exception ex){
                                        System.out.println(ex.toString());
                                    }
                                }else{
                                    Toast.makeText(getApplicationContext(),"Error al cargar la información",Toast.LENGTH_LONG).show();
                                }
                            }else{
                                Toast.makeText(getApplicationContext(),"Sin materiales",Toast.LENGTH_LONG).show();
                            }
                        }else{
                            Toast.makeText(getApplicationContext(), obj.get("MSG").toString(), Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> {
            System.out.println("Error");
            System.out.println(error);
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("SUCURSAL",sharedpreferences.getString(global.getKeyuserSucursal(), ""));
                params.put("USERNAME",sharedpreferences.getString(global.getKeyuser(), ""));
                params.put("CONTEO",conteo);
                params.put("TOKEN",global.getTokenAuthApi());
                return params;

            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                15000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(getApplicationContext()).add(request);
    }
    public void SecondCont(){
        switch (conteo) {
            case "1":
                getLocations();
                break;
            case "2":
                getLocations2();
                break;
            case "3":
                getLocations2();
                break;
        }
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Segunda línea de código (se ejecutará después de 1 segundo)
            }
        }, 1000);
        progressBarLoadata.setVisibility(View.VISIBLE);
        String url=global.getUrlApi()+"getLocationMaterial/";
        StringRequest request = new StringRequest(Request.Method.POST,
                url,
                response -> {
                    JSONObject obj = null;
                    System.out.println(response);
                    try {
                        obj = new JSONObject(response);
                        progressBarLoadata.setVisibility(View.GONE);
                        if (obj.get("VALIDATE").toString().equals("1")) { // SI ES CORRECTO validate=1
                            JSONArray array = new JSONArray(obj.get("DATA").toString());
                            if(array.length()>0){
                                DBHelper dbHelper = new DBHelper(Principal2Activity.this);
                                SQLiteDatabase db=dbHelper.getWritableDatabase();
                                if(db !=null){
                                    long id=0;
                                    try {
                                        db.execSQL("DELETE FROM "+global.getTbl_material());
                                        for (int i = 0; i < array.length(); i++) {
                                            JSONObject row = array.getJSONObject(i);
                                            ContentValues values = new ContentValues();
                                            values.put("locacion",row.get("LOCACION").toString().trim());
                                            values.put("descripcion",row.get("DESCRIPCION").toString().trim());
                                            values.put("codas",row.get("COD_AS").toString().trim());
                                            values.put("codean",row.get("COD_EAN").toString().trim());
                                            values.put("factor",row.get("FACTOR").toString().trim());
                                            values.put("unidadbase",row.get("UNIDAD_BASE").toString().trim());
                                            values.put("indicador",row.get("INDICADOR").toString().trim());
                                            db.insert(global.getTbl_material(),null,values);
                                        }
                                        const_cart_btn_loadata.setBackgroundResource(R.color.tl_success);
                                        Toast.makeText(getApplicationContext(),"Carga completa",Toast.LENGTH_LONG).show();
                                    }catch (Exception ex){
                                        System.out.println(ex.toString());
                                    }
                                }else{
                                    Toast.makeText(getApplicationContext(),"Error al cargar la información",Toast.LENGTH_LONG).show();
                                }
                            }else{
                                Toast.makeText(getApplicationContext(),"Sin materiales",Toast.LENGTH_LONG).show();
                            }
                        }else{
                            Toast.makeText(getApplicationContext(), obj.get("MSG").toString(), Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> {
            System.out.println("Error");
            System.out.println(error);
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("SUCURSAL",sharedpreferences.getString(global.getKeyuserSucursal(), ""));
                params.put("USERNAME",sharedpreferences.getString(global.getKeyuser(), ""));
                params.put("CONTEO",conteo);
                params.put("TOKEN",global.getTokenAuthApi());
                return params;

            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                15000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(getApplicationContext()).add(request);
    }
    public void getLocations(){
        if(!global.isNetworkConnected()){
            Toast.makeText(getApplicationContext(), global.getnotNetworkMessage(), Toast.LENGTH_LONG).show();
        }else {
            DBHelper dbHelper = new DBHelper(getApplicationContext());
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            if(ConteoPreferer.length()>0) { //SI TIENE CONTEO
                String url=global.getUrlApi()+"locationsInv/";
                StringRequest request = new StringRequest(Request.Method.POST,
                        url,
                        response -> {
                            System.out.println("responseresponse");
                            System.out.println(response);
                            JSONObject obj = null;
                            try {
                                obj = new JSONObject(response);
                                if (obj.get("VALIDATE").toString().equals("1")) { // SI ES CORRECTO validate=1
                                    db.execSQL("DELETE FROM "+global.getTbl_location_inv());
                                    String[] item= obj.get("DATA").toString().trim().split(",");
                                    for (String l:item ) {
                                        ContentValues values = new ContentValues();
                                        values.put("locacion",l.trim());
                                        db.insert(global.getTbl_location_inv(),null,values);
                                    }
                                }else{
                                    Toast.makeText(getApplicationContext(), obj.get("MSG").toString(), Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }, error -> {
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String,String> params = new HashMap<>();

                        params.put("SUCURSAL",sharedpreferences.getString(global.getKeyuserSucursal(), ""));
                        params.put("USERNAME",sharedpreferences.getString(global.getKeyuser(), ""));
                        params.put("TOKEN",global.getTokenAuthApi());
                        params.put("CONTEO",conteo);
                        return params;

                    }

                };
                Volley.newRequestQueue(getApplicationContext()).add(request);

            }else{
                Toast.makeText(getApplicationContext(),"Sin conteos disponibles",Toast.LENGTH_SHORT).show();
            }

        }

    }
    public void getLocations2(){
        if(!global.isNetworkConnected()){
            Toast.makeText(getApplicationContext(), global.getnotNetworkMessage(), Toast.LENGTH_LONG).show();
        }else {
            DBHelper dbHelper = new DBHelper(getApplicationContext());
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            if(ConteoPreferer.length()>0) { //SI TIENE CONTEO
                String url=global.getUrlApi()+"locationsInv2/";
                StringRequest request = new StringRequest(Request.Method.POST,
                        url,
                        response -> {
                            System.out.println("responseresponse");
                            System.out.println(response);
                            JSONObject obj = null;
                            try {
                                obj = new JSONObject(response);
                                if (obj.get("VALIDATE").toString().equals("1")) { // SI ES CORRECTO validate=1
                                    db.execSQL("DELETE FROM "+global.getTbl_location_inv());
                                    String[] item= obj.get("DATA").toString().trim().split(",");
                                    for (String l:item ) {
                                        ContentValues values = new ContentValues();
                                        values.put("locacion",l.trim());
                                        db.insert(global.getTbl_location_inv(),null,values);
                                    }
                                }else{
                                    Toast.makeText(getApplicationContext(), obj.get("MSG").toString(), Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }, error -> {
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String,String> params = new HashMap<>();

                        params.put("SUCURSAL",sharedpreferences.getString(global.getKeyuserSucursal(), ""));
                        params.put("USERNAME",sharedpreferences.getString(global.getKeyuser(), ""));
                        params.put("TOKEN",global.getTokenAuthApi());
                        params.put("CONTEO",conteo);
                        return params;

                    }

                };
                Volley.newRequestQueue(getApplicationContext()).add(request);

            }else{
                Toast.makeText(getApplicationContext(),"Sin conteos disponibles",Toast.LENGTH_SHORT).show();
            }

        }

    }
    public void getLocations3(){
        if(!global.isNetworkConnected()){
            Toast.makeText(getApplicationContext(), global.getnotNetworkMessage(), Toast.LENGTH_LONG).show();
        }else {
            DBHelper dbHelper = new DBHelper(getApplicationContext());
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            if(ConteoPreferer.length()>0) { //SI TIENE CONTEO
                String url=global.getUrlApi()+"locationsInv3/";
                StringRequest request = new StringRequest(Request.Method.POST,
                        url,
                        response -> {
                            System.out.println("responseresponse");
                            System.out.println(response);
                            JSONObject obj = null;
                            try {
                                obj = new JSONObject(response);
                                if (obj.get("VALIDATE").toString().equals("1")) { // SI ES CORRECTO validate=1
                                    db.execSQL("DELETE FROM "+global.getTbl_location_inv());
                                    String[] item= obj.get("DATA").toString().trim().split(",");
                                    for (String l:item ) {
                                        ContentValues values = new ContentValues();
                                        values.put("locacion",l.trim());
                                        db.insert(global.getTbl_location_inv(),null,values);
                                    }
                                }else{
                                    Toast.makeText(getApplicationContext(), obj.get("MSG").toString(), Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }, error -> {
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String,String> params = new HashMap<>();

                        params.put("SUCURSAL",sharedpreferences.getString(global.getKeyuserSucursal(), ""));
                        params.put("USERNAME",sharedpreferences.getString(global.getKeyuser(), ""));
                        params.put("TOKEN",global.getTokenAuthApi());
                        params.put("CONTEO",conteo);
                        return params;

                    }

                };
                Volley.newRequestQueue(getApplicationContext()).add(request);

            }else{
                Toast.makeText(getApplicationContext(),"Sin conteos disponibles",Toast.LENGTH_SHORT).show();
            }

        }

    }
    public void CreateInventario(){
        DBHelper dbHelper = new DBHelper(getApplicationContext());
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = "SELECT count(1) as cantidad FROM " + global.getTbl_material();
        Cursor c = (db.rawQuery(query, null));
        int cant=0;
        if (c.moveToFirst()) {
            do {
                System.out.println("stringgg");
                System.out.println(c.getString(0));
                cant=Integer.valueOf(c.getString(0));
            } while (c.moveToNext());
        }
        if(cant!=0) {

            /* MOSTRAR VISTA COMO EN UN MODAL */
            LayoutInflater inflater = getLayoutInflater();
            final View v = inflater.inflate(R.layout.activity_locations_inv, null);
            final AlertDialog.Builder b =  new AlertDialog.Builder(Principal2Activity.this);
            b.setView(v);
            final ProgressBar progressBar = v.findViewById(R.id.progressBar);
            progressBar.setVisibility(View.GONE);
            final AlertDialog popud = b.show();
            final ImageView icon_close_det = v.findViewById(R.id.icon_close_det); /* Initialice variables de vista llamada */
            final View cart_btn_continuar = v.findViewById(R.id.cart_btn_continuar); /* Initialice variables de vista llamada */

            Spinner sp_loc=(Spinner) v.findViewById(R.id.sp_loc);
            TextView sp_title_conteo= v.findViewById(R.id.sp_title_conteo);

            String ConteoPreferer=sharedpreferences.getString(global.getKeyuserConteos(), "");
            String LcPreferer=sharedpreferences.getString(global.getKeyuserLocationsInv(), "");
            if(ConteoPreferer.length()>0){ //SI TIENE CONTEO
                sp_title_conteo.setText("Conteo:"+conteo);
                LinkedList<String> locations_sp = new LinkedList<String>();

                query = "SELECT locacion FROM " + global.getTbl_location_count()+ " LIMIT 1";

                c = (db.rawQuery(query, null));
                String activeLocation="";
                int activeLocation_index=0;
                if (c.getCount() > 0) {
                    if (c.moveToFirst()) {

                        do {
                            activeLocation = c.getString(0).trim();
                        } while (c.moveToNext());
                    }
                }
                query = "SELECT DISTINCT id,locacion FROM " + global.getTbl_location_inv()+ " order by locacion asc";
                c = (db.rawQuery(query, null));
                //try {
                //    Thread.sleep(3000); // Pausa de 5000 milisegundos (5 segundos)
                //} catch (InterruptedException e) {
                //    e.printStackTrace();
                //}
                if (c.getCount() > 0) {
                    int i=0;
                    if (c.moveToFirst()) {
                        do {
                            String r_loc = c.getString(1).trim();
                            if(activeLocation.trim().equals(r_loc.trim())){
                                activeLocation_index=i;
                            }
                            locations_sp.add(r_loc);
                            i++;
                        } while (c.moveToNext());
                    }
                    adapter= new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item,locations_sp);
                    sp_loc.setAdapter(adapter);
                    System.out.println("activeLocation_index");
                    System.out.println(activeLocation_index);
                    sp_loc.setSelection(activeLocation_index);
                    adapter.notifyDataSetChanged();
                }



            }else{
                Toast.makeText(getApplicationContext(),"Sin conteos disponibles",Toast.LENGTH_SHORT).show();
            }
            icon_close_det.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View vi){
                    popud.dismiss();
                }
            });
             cart_btn_continuar.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View vi){
                    String location_send=sp_loc.getSelectedItem().toString();

                    Intent intent = new Intent(getApplicationContext(),InventarioActivity.class);
                    intent.putExtra("LOCATION",location_send);
                    finish();
                    startActivity(intent);


                    if(!global.isNetworkConnected()){
                        Toast.makeText(getApplicationContext(), global.getnotNetworkMessage(), Toast.LENGTH_LONG).show();
                    }else{
                        progressBar.setVisibility(View.VISIBLE);
                        String url=global.getUrlApi()+"getLocationMaterial/";
                        StringRequest request = new StringRequest(Request.Method.POST,
                                url,
                                response -> {
                                    JSONObject obj = null;
                                    System.out.println("Respuesta cart_btn_continuar");
                                    System.out.println(response);
                                    try {
                                        obj = new JSONObject(response);
                                        progressBar.setVisibility(View.GONE);
                                        if (obj.get("VALIDATE").toString().equals("1")) { // SI ES CORRECTO validate=1
                                            JSONArray array = new JSONArray(obj.get("DATA").toString());
                                            if(array.length()>0){
                                                System.out.println("entra");
                                                DBHelper dbHelper = new DBHelper(Principal2Activity.this);
                                                SQLiteDatabase db=dbHelper.getWritableDatabase();
                                                if(db !=null){
                                                    long id=0;
                                                    try {
                                                        System.out.println("DB creada");
                                                        System.out.println(array);
                                                        db.execSQL("DELETE FROM "+global.getTbl_material());
                                                        for (int i = 0; i < array.length(); i++) {
                                                            JSONObject row = array.getJSONObject(i);
                                                            ContentValues values = new ContentValues();
                                                            values.put("locacion",row.get("LOCACION").toString().trim());
                                                            values.put("descripcion",row.get("DESCRIPCION").toString().trim());
                                                            values.put("codas",row.get("COD_AS").toString().trim());
                                                            values.put("codean",row.get("COD_EAN").toString().trim());
                                                            values.put("factor",row.get("FACTOR").toString().trim());
                                                            values.put("unidadbase",row.get("UNIDAD_BASE").toString().trim());
                                                            db.insert(global.getTbl_material(),null,values);
                                                        }
                                                        System.out.println("QUIIIII");
                                                        Toast.makeText(getApplicationContext(),"Carga completa",Toast.LENGTH_LONG).show();


                                                    }catch (Exception ex){
                                                        System.out.println(ex.toString());
                                                    }
                                                }else{
                                                    Toast.makeText(getApplicationContext(),"Error al cargar la información",Toast.LENGTH_LONG).show();
                                                }
                                            }else{
                                                Toast.makeText(getApplicationContext(),"Ubicación sin materiales",Toast.LENGTH_LONG).show();
                                            }
                                        }else{
                                            Toast.makeText(getApplicationContext(), obj.get("MSG").toString(), Toast.LENGTH_LONG).show();
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }, error -> {
                            System.out.println("Error");
                            System.out.println(error);
                        }){
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                Map<String,String> params = new HashMap<>();
                                params.put("SUCURSAL",sharedpreferences.getString(global.getKeyuserSucursal(), ""));
                                params.put("USERNAME",sharedpreferences.getString(global.getKeyuser(), ""));
                                params.put("TOKEN",global.getTokenAuthApi());
                                return params;

                            }
                        };
                        request.setRetryPolicy(new DefaultRetryPolicy(
                                15000,
                                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                        Volley.newRequestQueue(getApplicationContext()).add(request);
                    }





                }
            });
            sp_loc.setOnItemSelectedListener(
                    new AdapterView.OnItemSelectedListener() {
                        public void onItemSelected(
                                AdapterView<?> parent, View view, int position, long id) {
                            String val_selected=adapter.getItem(position).toString().trim();
                            DBHelper dbHelper = new DBHelper(Principal2Activity.this);
                            SQLiteDatabase db=dbHelper.getReadableDatabase();
                            String query = "SELECT locacion FROM "+global.getTbl_location_count()+ " LIMIT 1";
                            Cursor c=(db.rawQuery(query,null));
                            String r_loc="";
                            if (c.getCount() > 0) {
                                if (c.moveToFirst()) {
                                    do {
                                        r_loc = c.getString(0).trim();
                                    } while (c.moveToNext());
                                }
                                if(!r_loc.equals(val_selected)){
                                    Toast.makeText(getApplicationContext(), "Debe terminar el conteo en "+r_loc, Toast.LENGTH_LONG).show();
                                    sp_loc.setSelection(adapter.getPosition(r_loc));
                                }

                            }
                        }

                        public void onNothingSelected(AdapterView<?> parent) {
                            System.out.println("Spinner2: unselected");
                        }
                    });

        }else{
            Toast.makeText(getApplicationContext(), "Debe cargar la información antes", Toast.LENGTH_LONG).show();
        }
    }
    public void CreateSecondInventario(){
        System.out.println(conteo);
        DBHelper dbHelper = new DBHelper(getApplicationContext());
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String query = "SELECT count(1) as cantidad FROM " + global.getTbl_material();
        Cursor c = (db.rawQuery(query, null));
        int cant=0;
        if (c.moveToFirst()) {
            do {
                System.out.println("stringgg");
                System.out.println(c.getString(0));
                cant=Integer.valueOf(c.getString(0));
            } while (c.moveToNext());
        }
        if(cant!=0) {

            /* MOSTRAR VISTA COMO EN UN MODAL */
            LayoutInflater inflater = getLayoutInflater();
            final View v = inflater.inflate(R.layout.activity_locations_inv, null);
            final AlertDialog.Builder b =  new AlertDialog.Builder(Principal2Activity.this);
            b.setView(v);
            final ProgressBar progressBar = v.findViewById(R.id.progressBar);
            progressBar.setVisibility(View.GONE);
            final AlertDialog popud = b.show();
            final ImageView icon_close_det = v.findViewById(R.id.icon_close_det); /* Initialice variables de vista llamada */
            final View cart_btn_continuar = v.findViewById(R.id.cart_btn_continuar); /* Initialice variables de vista llamada */

            Spinner sp_loc=(Spinner) v.findViewById(R.id.sp_loc);
            TextView sp_title_conteo= v.findViewById(R.id.sp_title_conteo);

            String ConteoPreferer=sharedpreferences.getString(global.getKeyuserConteos(), "");
            String LcPreferer=sharedpreferences.getString(global.getKeyuserLocationsInv(), "");
            if(ConteoPreferer.length()>0){ //SI TIENE CONTEO
                sp_title_conteo.setText("Conteo:"+conteo);
                LinkedList<String> locations_sp = new LinkedList<String>();

                query = "SELECT locacion FROM " + global.getTbl_location_count()+ " LIMIT 1";

                c = (db.rawQuery(query, null));
                String activeLocation="";
                int activeLocation_index=0;
                if (c.getCount() > 0) {
                    if (c.moveToFirst()) {

                        do {
                            activeLocation = c.getString(0).trim();
                        } while (c.moveToNext());
                    }
                }
                query = "SELECT DISTINCT id,locacion FROM " + global.getTbl_location_inv()+ " order by locacion asc";
                c = (db.rawQuery(query, null));
                //try {
                //    Thread.sleep(3000); // Pausa de 5000 milisegundos (5 segundos)
                //} catch (InterruptedException e) {
                //    e.printStackTrace();
                //}
                if (c.getCount() > 0) {
                    int i=0;
                    if (c.moveToFirst()) {
                        do {
                            String r_loc = c.getString(1).trim();
                            if(activeLocation.trim().equals(r_loc.trim())){
                                activeLocation_index=i;
                            }
                            locations_sp.add(r_loc);
                            i++;
                        } while (c.moveToNext());
                    }
                    adapter= new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item,locations_sp);
                    sp_loc.setAdapter(adapter);
                    System.out.println("activeLocation_index");
                    System.out.println(activeLocation_index);
                    sp_loc.setSelection(activeLocation_index);
                    adapter.notifyDataSetChanged();
                }



            }else{
                Toast.makeText(getApplicationContext(),"Sin conteos disponibles",Toast.LENGTH_SHORT).show();
            }
            icon_close_det.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View vi){
                    popud.dismiss();
                }
            });
            cart_btn_continuar.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View vi){
                    String location_send=sp_loc.getSelectedItem().toString();
                    String query_cod = "SELECT codas,locacion,descripcion FROM "+global.getTbl_material()+" where locacion = '"+location_send+"' order by locacion asc";
                    Cursor c_2=(db.rawQuery(query_cod,null));
                    c_2.moveToFirst();
                    String codigo= c_2.getString(0);
                    String desc= c_2.getString(2);
                    Intent intent = new Intent(getApplicationContext(),InventarioActivity2_nuevo.class);
                    intent.putExtra("LOCATION",location_send);
                    intent.putExtra("MATERIAL",codigo);
                    intent.putExtra("DESC",desc);
                    System.out.println(desc);
                    finish();
                    startActivity(intent);

                    if(!global.isNetworkConnected()){
                        Toast.makeText(getApplicationContext(), global.getnotNetworkMessage(), Toast.LENGTH_LONG).show();
                    }else{
                        progressBar.setVisibility(View.VISIBLE);
                        String url=global.getUrlApi()+"getLocationMaterial/";
                        StringRequest request = new StringRequest(Request.Method.POST,
                                url,
                                response -> {
                                    JSONObject obj = null;
                                    System.out.println("Respuesta cart_btn_continuar");
                                    System.out.println(response);
                                    try {
                                        obj = new JSONObject(response);
                                        progressBar.setVisibility(View.GONE);
                                        if (obj.get("VALIDATE").toString().equals("1")) { // SI ES CORRECTO validate=1
                                            JSONArray array = new JSONArray(obj.get("DATA").toString());
                                            if(array.length()>0){
                                                System.out.println("entra");
                                                DBHelper dbHelper = new DBHelper(Principal2Activity.this);
                                                SQLiteDatabase db=dbHelper.getWritableDatabase();
                                                if(db !=null){
                                                    long id=0;
                                                    try {
                                                        System.out.println("DB creada");
                                                        System.out.println(array);
                                                        db.execSQL("DELETE FROM "+global.getTbl_material());
                                                        for (int i = 0; i < array.length(); i++) {
                                                            JSONObject row = array.getJSONObject(i);
                                                            ContentValues values = new ContentValues();
                                                            values.put("locacion",row.get("LOCACION").toString().trim());
                                                            values.put("descripcion",row.get("DESCRIPCION").toString().trim());
                                                            values.put("codas",row.get("COD_AS").toString().trim());
                                                            values.put("codean",row.get("COD_EAN").toString().trim());
                                                            values.put("factor",row.get("FACTOR").toString().trim());
                                                            values.put("unidadbase",row.get("UNIDAD_BASE").toString().trim());
                                                            db.insert(global.getTbl_material(),null,values);
                                                        }
                                                        System.out.println("QUIIIII");
                                                        Toast.makeText(getApplicationContext(),"Carga completa",Toast.LENGTH_LONG).show();


                                                    }catch (Exception ex){
                                                        System.out.println(ex.toString());
                                                    }
                                                }else{
                                                    Toast.makeText(getApplicationContext(),"Error al cargar la información",Toast.LENGTH_LONG).show();
                                                }
                                            }else{
                                                Toast.makeText(getApplicationContext(),"Ubicación sin materiales",Toast.LENGTH_LONG).show();
                                            }
                                        }else{
                                            Toast.makeText(getApplicationContext(), obj.get("MSG").toString(), Toast.LENGTH_LONG).show();
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }, error -> {
                            System.out.println("Error");
                            System.out.println(error);
                        }){
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                Map<String,String> params = new HashMap<>();
                                params.put("SUCURSAL",sharedpreferences.getString(global.getKeyuserSucursal(), ""));
                                params.put("USERNAME",sharedpreferences.getString(global.getKeyuser(), ""));
                                params.put("TOKEN",global.getTokenAuthApi());
                                return params;

                            }
                        };
                        request.setRetryPolicy(new DefaultRetryPolicy(
                                10000,
                                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                        Volley.newRequestQueue(getApplicationContext()).add(request);
                    }





                }
            });
            sp_loc.setOnItemSelectedListener(
                    new AdapterView.OnItemSelectedListener() {
                        public void onItemSelected(
                                AdapterView<?> parent, View view, int position, long id) {
                            String val_selected=adapter.getItem(position).toString().trim();
                            DBHelper dbHelper = new DBHelper(Principal2Activity.this);
                            SQLiteDatabase db=dbHelper.getReadableDatabase();
                            String query = "SELECT locacion FROM "+global.getTbl_location_count()+ " LIMIT 1";
                            Cursor c=(db.rawQuery(query,null));
                            String r_loc="";
                            if (c.getCount() > 0) {
                                if (c.moveToFirst()) {
                                    do {
                                        r_loc = c.getString(0).trim();
                                    } while (c.moveToNext());
                                }
                                if(!r_loc.equals(val_selected)){
                                    Toast.makeText(getApplicationContext(), "Debe terminar el conteo en "+r_loc, Toast.LENGTH_LONG).show();
                                    sp_loc.setSelection(adapter.getPosition(r_loc));
                                }

                            }
                        }

                        public void onNothingSelected(AdapterView<?> parent) {
                            System.out.println("Spinner2: unselected");
                        }
                    });

        }else{
            Toast.makeText(getApplicationContext(), "Debe cargar la información antes", Toast.LENGTH_LONG).show();
        }
    }

    public void closeSession(){

        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putBoolean(LoginStatus, false);
        editor.commit();
        DeleteTables();
        Intent intent = new Intent(this,LoginActivity.class);
        startActivity(intent);
    }
}