package perucode.center.qr_pcs;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.content.Context;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import android.content.SharedPreferences;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    Button btn_l;
    EditText et_username, et_password;
    public static SharedPreferences sharedpreferences;
    public static final String User = "user";
    public static final String id_client = "0";
    public static final String MyPREFERENCES = "MyPrefs";
    public static final String LoginStatus = "false";
    public static Boolean logeado = false ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        /* PARA QUE LA SESSION SE MANTENGA */
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        logeado=isLoggin();

        if(logeado){
            Intent i =  new Intent(getApplicationContext(),PrincipalActivity.class);
            i.putExtra("User",sharedpreferences.getString(User,"user"));
            i.putExtra("id_client",sharedpreferences.getString(id_client,"0"));
            finish();
            startActivity(i);
        }else{
            btn_l=findViewById(R.id.btn_login);
            et_username=findViewById(R.id.et_username);
            et_password=findViewById(R.id.et_password);

            btn_l.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    login();
                }
            } );
        }
    }

    public void login(){
        StringRequest request = new StringRequest(Request.Method.POST, "https://satelital.perucontrols.com/perucontrols/sistema/app_qr/login.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        /*Intent i =  new Intent(getApplicationContext(),PrincipalActivity.class);
                        i.putExtra("User","usuario");
                        i.putExtra("id_client","71956326");
                        finish();
                        startActivity(i);*/
                        if(response.contains("1")){
                            //-- OPTENIENDO EL DNI PARA GUARDARLO
                                String[] arrayString = response.split(";");
                            //-- FIN OBTENER ID

                            //--LOGIN PASS
                            SharedPreferences.Editor editor = sharedpreferences.edit();
                                editor.putString(User, et_username.getText().toString());
                                editor.putString(id_client, arrayString[1]);
                                editor.putBoolean(LoginStatus, true);
                                editor.commit();
                            //--FIN LOGIN PASS

                            Intent i =  new Intent(getApplicationContext(),PrincipalActivity.class);
                            i.putExtra("User",et_username.getText().toString());
                            i.putExtra("id_client",arrayString[1]);

                            finish();
                            startActivity(i);

                        }else{
                            Toast.makeText(getApplicationContext(),"Credenciales Erroneas",Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error){ /**/ }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String,String> params = new HashMap<>();
                        params.put("username",et_username.getText().toString());
                        params.put("password",et_password.getText().toString());
                        return params;
                    }
                };
        Volley.newRequestQueue(this).add(request);
    }

    public boolean isLoggin(){
        return sharedpreferences.getBoolean(LoginStatus, false);
    }

}
