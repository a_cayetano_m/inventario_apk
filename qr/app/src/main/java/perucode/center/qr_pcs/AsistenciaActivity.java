package perucode.center.qr_pcs;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Looper;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.util.HashMap;
import java.util.Map;

public class AsistenciaActivity extends AppCompatActivity {

    int PERMISSION_ID = 1001;
    public FusedLocationProviderClient mFusedLocationClient;
    public Button btn_ingreso, btn_salida ;
    TextView lon, lat;
    public static String dni_u;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asistencia);

        btn_ingreso = (Button) findViewById(R.id.btn_ingreso_oficina);
        btn_salida = (Button) findViewById(R.id.btn_salida_oficina);

        lon = (TextView) findViewById(R.id.txt_lon_oficina);
        lat = (TextView) findViewById(R.id.txt_lat_oficina);

        Bundle extras = getIntent().getExtras();
        dni_u = extras.getString("dni_usuario");

        btn_ingreso.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                ingreso_oficina();
            }
        });

        btn_salida.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                salida_oficina();
            }
        });

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        getLastLocation();

        insert_gps(dni_u);
    }

    public void ingreso_oficina(){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "https://satelital.perucontrols.com/perucontrols/sistema/loginapp/asistencia.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response.contains("1")){
                            String[] arrayString = response.split(";");

                            if(arrayString[1].equals("0")){
                                Intent intent = new Intent(AsistenciaActivity.this,scanerQRActivity.class);
                                intent.putExtra("dni_usuario", dni_u);
                                intent.putExtra("etapa", "inicio");
                                startActivity(intent);
                            }else if(arrayString[1].equals("1")){
                                Toast.makeText(getApplicationContext(),"Ya se registro una asistencia",Toast.LENGTH_SHORT).show();
                            }

                        }else{
                            Toast.makeText(getApplicationContext(),"Error-Problema de Red",Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){ /**/ }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("dni_u",dni_u);
                return params;
            }
        };

        Volley.newRequestQueue(AsistenciaActivity.this).add(stringRequest);
    }


    public void salida_oficina(){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "https://satelital.perucontrols.com/perucontrols/sistema/loginapp/asistencia.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response.contains("1")){

                            String[] arrayString = response.split(";");

                            if(arrayString[4].equals("0")){

                                if(arrayString[1].equals("1")){

                                    if(arrayString[5].equals("1")){
                                        Intent intent = new Intent(AsistenciaActivity.this,scanerQRActivity.class);
                                        intent.putExtra("dni_usuario", dni_u);
                                        intent.putExtra("etapa", "salida");
                                        startActivity(intent);
                                    }else if(arrayString[5].equals("0")){
                                        Toast.makeText(getApplicationContext(),"¡Rellenar Reporte!",Toast.LENGTH_SHORT).show();

                                        final AlertDialog.Builder builder =  new AlertDialog.Builder(AsistenciaActivity.this);
                                        LayoutInflater inflater = getLayoutInflater();
                                        final View view = inflater.inflate(R.layout.reporte_taller, null);

                                        final TextView codigo = view.findViewById(R.id.txt_codigo_reporte);
                                        final EditText reporte = view.findViewById(R.id.txt_reporte);
                                        final Button guardar = view.findViewById(R.id.btn_guardar_reporte);

                                        codigo.setText(arrayString[6]+"-Taller-"+arrayString[7]);

                                        builder.setView(view);
                                        final AlertDialog popud = builder.show();

                                        guardar.setOnClickListener(new View.OnClickListener(){
                                            @Override
                                            public void onClick(View v){
                                                popud.dismiss();
                                                guardar_reporte(dni_u,reporte.getText().toString());
                                                Intent intent = new Intent(AsistenciaActivity.this,scanerQRActivity.class);
                                                intent.putExtra("dni_usuario", dni_u);
                                                intent.putExtra("etapa", "salida");
                                                startActivity(intent);
                                            }
                                        });
                                    }

                                }else if(arrayString[1].equals("0")){
                                    Toast.makeText(getApplicationContext(),"Registrar primero una asistencia",Toast.LENGTH_SHORT).show();
                                }

                            }else if(arrayString[4].equals("1")){
                                Toast.makeText(getApplicationContext(),"Salida ya registrada",Toast.LENGTH_SHORT).show();
                            }

                        }else{
                            Toast.makeText(getApplicationContext(),"Error-Problema de Red",Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){ /**/ }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("dni_u",dni_u);
                return params;
            }
        };

        Volley.newRequestQueue(AsistenciaActivity.this).add(stringRequest);

    }

    public void guardar_reporte(final String dni, final String texto){
        StringRequest request = new StringRequest(Request.Method.POST, "https://satelital.perucontrols.com/perucontrols/sistema/loginapp/registro_reporte.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response.contains("1")){
                            Toast.makeText(getApplicationContext(),"Reporte guardado!",Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(getApplicationContext(),"Error-Problema de Red",Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){ /**/ }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("dni",dni);
                params.put("texto",texto);
                return params;
            }
        };
        Volley.newRequestQueue(this).add(request);
    }

    /*--------------------------------------------------------------------------------------------*/
    @SuppressLint("MissingPermission")
    private void getLastLocation(){
        if (checkPermissions()) {
            if (isLocationEnabled()) {
                mFusedLocationClient.getLastLocation().addOnCompleteListener(
                        new OnCompleteListener<Location>() {
                            @Override
                            public void onComplete(@NonNull Task<Location> task) {
                                Location location = task.getResult();
                                requestNewLocationData();
                                lat.setText(location.getLatitude()+"");
                                lon.setText(location.getLongitude()+"");
                            }
                        }
                );
            } else {
                Toast.makeText(this, "Activar ubicación", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        } else {
            requestPermissions();
        }
    }


    @SuppressLint("MissingPermission")
    private void requestNewLocationData(){
        insert_gps(dni_u);
        LocationRequest mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(5000);
        mLocationRequest.setFastestInterval(1000);
        //mLocationRequest.setNumUpdates(5);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mFusedLocationClient.requestLocationUpdates( mLocationRequest, mLocationCallback, Looper.myLooper());
    }

    private LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            insert_gps(dni_u);
            Location mLastLocation = locationResult.getLastLocation();
            lat.setText(mLastLocation.getLatitude()+"");
            lon.setText(mLastLocation.getLongitude()+"");
        }
    };

    private boolean checkPermissions() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},PERMISSION_ID);
    }

    private boolean isLocationEnabled() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_ID) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getLastLocation();
            }
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        if (checkPermissions()) {
            getLastLocation();
        }
    }

    public void insert_gps(final String dni){
        final String longitud = lon.getText().toString();
        final String latitud = lat.getText().toString();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, "https://satelital.perucontrols.com/perucontrols/sistema/loginapp/geocerca/gps_a.php?lon="+longitud+"&lat="+latitud+"&dni="+dni,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        String[] arrayString = response.split(";");

                        if(arrayString[1].equals("1")){

                            StringRequest stringRequest = new StringRequest(Request.Method.GET,"https://satelital.perucontrols.com/perucontrols/sistema/loginapp/geocerca/estado_geo.php?dni="+dni,
                                    new Response.Listener<String>() {
                                        @Override
                                        public void onResponse(String response) {
                                            String[] arrayString = response.split(";");

                                            String atr = "0";

                                            if(arrayString[1].equals(atr)){
                                                finish();
                                            }
                                        }
                                    },
                                    new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) { /**/ }
                                    });
                            //adding our stringrequest to queue
                            Volley.newRequestQueue(AsistenciaActivity.this).add(stringRequest);

                        }else {
                            Toast.makeText(getApplicationContext(),"ERROR RED",Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) { /**/ }
                });
        //adding our stringrequest to queue
        Volley.newRequestQueue(this).add(stringRequest);
    }

}




