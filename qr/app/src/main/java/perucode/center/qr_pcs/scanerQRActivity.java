package perucode.center.qr_pcs;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class scanerQRActivity extends AppCompatActivity {

    public Vibrator vibrator ;
    public int var_uno = 1;
    public SurfaceView cameraPreview;
    public TextView txtResult;
    public BarcodeDetector barcodeDetector;
    public CameraSource cameraSource;
    final int RequestCameraPermissionID = 1001;
    public static String dni_u;
    public static String etapa;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case RequestCameraPermissionID: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    try {
                        cameraSource.start(cameraPreview.getHolder());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scaner_qr);

        Bundle extras = getIntent().getExtras();
        dni_u = extras.getString("dni_usuario");
        etapa = extras.getString("etapa");

        //The key argument here must match that used in the other activity
        cameraPreview = (SurfaceView) findViewById(R.id.cameraPreview);
        txtResult = (TextView) findViewById(R.id.txtResult);
        txtResult.setVisibility(View.INVISIBLE);

        barcodeDetector = new BarcodeDetector.Builder(this)
                .setBarcodeFormats(Barcode.QR_CODE)
                .build();
        cameraSource = new CameraSource
                .Builder(this, barcodeDetector)
                .setRequestedPreviewSize(640, 480)
                .build();

        // Add Event
        cameraPreview.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder surfaceHolder) {
                if (ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    //Request permission
                    ActivityCompat.requestPermissions(scanerQRActivity.this, new String[]{Manifest.permission.CAMERA},RequestCameraPermissionID);
                    return;
                }
                try {
                    cameraSource.start(cameraPreview.getHolder());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) { /**/ }

            @Override
            public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
                cameraSource.stop();
            }
        });


        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() { /**/ }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                final SparseArray<Barcode> qrcodes = detections.getDetectedItems();

                if(qrcodes.size() != 0)
                {
                    txtResult.post(new Runnable() {
                    @Override
                    public void run() {
                            //Create vibrate*/
                           if(var_uno==1){
                                vibrator  = (Vibrator)getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(1000);
                                txtResult.setText(qrcodes.valueAt(0).displayValue);

                                String URL_PRODUCTSR = ""+txtResult.getText().toString()+"";

                                //-- MODAL
                                final AlertDialog.Builder builderr =  new AlertDialog.Builder(scanerQRActivity.this);
                                LayoutInflater inflater = getLayoutInflater();
                                final View view = inflater.inflate(R.layout.asistencia_campo, null);
                                final TextView fecha_actual = view.findViewById(R.id.txt_fecha_asistencia);
                                final TextView title = view.findViewById(R.id.txt_title_asistencia);
                                final Button registro = view.findViewById(R.id.btn_confirmar_asistencia);

                                //RETORNO VALOR
                                    StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_PRODUCTSR,
                                            new Response.Listener<String>() {
                                                @Override
                                                public void onResponse(String response) {
                                                    if(response.contains("1")){
                                                        String[] arrayString = response.split(";");

                                                        final String fecha_db = arrayString[3];

                                                        if(etapa.equals("inicio")){ /*-----------------------------------------------------------------------------------------------------*/
                                                                title.setText("CONFIRMAR ASISTENCIA");
                                                                fecha_actual.setText(arrayString[2]);
                                                                builderr.setView(view);
                                                                builderr.show();

                                                            registro.setOnClickListener(new View.OnClickListener(){
                                                                @Override
                                                                public void onClick(View v){
                                                                    finish();
                                                                    registro_ingreso_oficina(dni_u,fecha_db);
                                                                }
                                                            });

                                                        }else if(etapa.equals("salida")){ /*------------------------------------------------------------------------------------------------*/
                                                                title.setText("CONFIRMACIÓN DE SALIDA");
                                                                fecha_actual.setText(arrayString[2]);
                                                                builderr.setView(view);
                                                                builderr.show();

                                                                registro.setOnClickListener(new View.OnClickListener(){
                                                                    @Override
                                                                    public void onClick(View v){
                                                                        finish();
                                                                        registro_salida_oficina(dni_u,fecha_db);
                                                                    }
                                                                });
                                                        }

                                                    }else{
                                                        Toast.makeText(getApplicationContext(),"Error-Problema de Red",Toast.LENGTH_SHORT).show();
                                                    }
                                                }
                                    }, new Response.ErrorListener(){
                                        @Override
                                        public void onErrorResponse(VolleyError error){ /**/ }
                                    }){
                                            @Override
                                            protected Map<String, String> getParams() throws AuthFailureError {
                                                Map<String,String> params = new HashMap<>();
                                                params.put("dni_u",dni_u);

                                                if(etapa.equals("inicio")){
                                                    params.put("accion","1");
                                                }else if(etapa.equals("salida")){
                                                    params.put("accion","2");
                                                }

                                                return params;
                                            }
                                    };

                                    Volley.newRequestQueue(scanerQRActivity.this).add(stringRequest);
                                // FIN RETORNO VALOR

                                var_uno=2;
                            }
                    }
                    });
                }else{
                    var_uno=1;
                    //vibrator.cancel();
                }
            }
        });
    }

    public void registro_ingreso_oficina(final String dni, final String fecha_db){
        StringRequest request = new StringRequest(Request.Method.POST, "https://satelital.perucontrols.com/perucontrols/sistema/loginapp/registro_asistencia.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response.contains("1")){
                            Toast.makeText(getApplicationContext(),"Asistencia registrado con exitoso!",Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(getApplicationContext(),"Error-Problema de Red",Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){  }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("dni",dni);
                params.put("fecha",fecha_db);
                params.put("tipo_asistencia","1");
                return params;
            }
        };
        Volley.newRequestQueue(this).add(request);
    }

    public void registro_salida_oficina(final String dni, final String fecha_db){
        StringRequest request = new StringRequest(Request.Method.POST, "https://satelital.perucontrols.com/perucontrols/sistema/loginapp/registro_asistencia.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response.contains("1")){
                            Toast.makeText(getApplicationContext(),"Salida registrado con exito!",Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(getApplicationContext(),"Error-Problema de Red",Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){  }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("dni",dni);
                params.put("fecha",fecha_db);
                params.put("tipo_asistencia","2");
                return params;
            }
        };
        Volley.newRequestQueue(this).add(request);
    }

}
