package perucode.center.qr_pcs;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Looper;
import android.provider.Settings;
import android.widget.TextView;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

public class PrincipalActivity extends AppCompatActivity {

    int PERMISSION_ID = 1001;
    public FusedLocationProviderClient mFusedLocationClient;
    Button btn_a, btn_a_campo, btn_t, btn_t_campo, btn_v, btn_pend, btn_g_oficina, btn_g_campo;
    TextView lon_pr, lat_pr;
    public static String dni_user;

    private static final long GEO_DURATION = 60 * 60 * 1000;
    private static final String GEOFENCE_REQ_ID = "My Geofence";
    private static final float GEOFENCE_RADIUS = 500.0f; // in meters

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);

        btn_a = findViewById(R.id.btn_asistencia);
        btn_a_campo = findViewById(R.id.btn_asistencia_campo);
        btn_t = findViewById(R.id.btn_trabajo);
        btn_t_campo = findViewById(R.id.btn_trabajo_campo);
        btn_v = findViewById(R.id.btn_viaje);
        btn_pend = findViewById(R.id.btn_pendientes);

        btn_g_oficina = findViewById(R.id.btn_geo_oficina);
        btn_g_campo = findViewById(R.id.btn_geo_campo);

        lon_pr = findViewById(R.id.principal_longitud);
        lat_pr = findViewById(R.id.principal_latitud);

        Bundle extras = getIntent().getExtras();
        dni_user = extras.getString("id_client"); //SE OBTIENE EL DNI DEL ACTIVITY ANTERIOR

        btn_a.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                asistencia();
            }
        });

        btn_a_campo.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                asistencia_campo();
            }
        } );

        btn_t.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                trabajo();
            }
        } );

        btn_t_campo.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                trabajo_campo();
            }
        } );

        btn_v.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                viaje();
            }
        });

        btn_pend.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                StringRequest stringRequest = new StringRequest(Request.Method.GET, "https://satelital.perucontrols.com/perucontrols/sistema/pcs_pre_reporte.php?dni="+dni_user,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                String[] arrayString = response.split("!");

                                if(arrayString[1].equals("0")){
                                    Toast.makeText(getApplicationContext(),"No hay reportes pendientes",Toast.LENGTH_SHORT).show();
                                }else{
                                    Intent i = new Intent(getApplicationContext(),WebActivity.class);
                                    i.putExtra("URL", "https://satelital.perucontrols.com/perucontrols/sistema/pcs_pre_reporte.php?dni="+dni_user);
                                    startActivity(i);
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) { /* */}
                        });
                //adding our stringrequest to queue
                Volley.newRequestQueue(PrincipalActivity.this).add(stringRequest);
            }
        });

        btn_g_oficina.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                estado_geo("1");
            }
        });

        btn_g_campo.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                estado_geo("0");
            }
        });


        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        getLastLocation();

        btn_a.setEnabled(false);
        btn_a_campo.setEnabled(false);
        btn_t.setEnabled(false);
        btn_t_campo.setEnabled(false);

        lon_pr.setVisibility(View.INVISIBLE);
        lat_pr.setVisibility(View.INVISIBLE);

    } /* FIN onCreate*/

    public void asistencia(){
        Intent intent = new Intent(this,AsistenciaActivity.class);
        intent.putExtra("dni_usuario", dni_user);
        startActivity(intent);
    }

    public void asistencia_campo(){
        Intent intent = new Intent(this,AsistenciaCampo.class);
        intent.putExtra("dni_usuario", dni_user);
        startActivity(intent);
    }

    public void trabajo(){
        Intent intent = new Intent(this,TrabajoActivity.class);
        intent.putExtra("dni_usuario", dni_user);
        startActivity(intent);
    }

    public void trabajo_campo(){
        Intent intent = new Intent(this,TrabajoCampo.class);
        intent.putExtra("dni_usuario", dni_user);
        startActivity(intent);
    }

    public void viaje(){
        Intent intent = new Intent(this,ViajeActivity.class);
        intent.putExtra("dni_usuario", dni_user);
        startActivity(intent);
    }

    public void estado_geo(String estado_geo){
        StringRequest stringRequest = new StringRequest(Request.Method.GET, "https://satelital.perucontrols.com/perucontrols/sistema/loginapp/geocerca/forzado_estado.php?dni="+dni_user+"&geo="+estado_geo,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response.contains("1")){

                            String[] arrayString = response.split(";");

                            if(arrayString[1].equals("1")){ /* 1 = OFICINA */
                                btn_a.setVisibility(View.VISIBLE);
                                btn_a_campo.setVisibility(View.INVISIBLE);

                                btn_t.setVisibility(View.VISIBLE);
                                btn_t_campo.setVisibility(View.INVISIBLE);

                                btn_a.setEnabled(true);
                                btn_t.setEnabled(true);
                            }else if(arrayString[1].equals("0")){ /* 0 = CAMPO */
                                btn_a.setVisibility(View.INVISIBLE);
                                btn_a_campo.setVisibility(View.VISIBLE);

                                btn_t.setVisibility(View.INVISIBLE);
                                btn_t_campo.setVisibility(View.VISIBLE);

                                btn_a_campo.setEnabled(true);
                                btn_t_campo.setEnabled(true);
                            }
                            Toast.makeText(getApplicationContext(),"Cambio de opcion realizado",Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(getApplicationContext(),"Error de Red",Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) { /**/ }
                });
        //adding our stringrequest to queue
        Volley.newRequestQueue(this).add(stringRequest);
        /**/
        btn_a.setEnabled(false);
        btn_a_campo.setEnabled(false);
        btn_t.setEnabled(false);
        btn_t_campo.setEnabled(false);
    }

    /*--------------------------------------------------------------------------------------------*/
    @SuppressLint("MissingPermission")
    private void getLastLocation(){
        if (checkPermissions()) {
            if (isLocationEnabled()) {
                mFusedLocationClient.getLastLocation().addOnCompleteListener(
                        new OnCompleteListener<Location>() {
                            @Override
                            public void onComplete(@NonNull Task<Location> task) {
                                Location location = task.getResult();
                                requestNewLocationData();
                                lat_pr.setText(location.getLatitude()+"");
                                lon_pr.setText(location.getLongitude()+"");

                            }
                        }
                );
            } else {
                Toast.makeText(this, "Activar ubicación", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        } else {
            requestPermissions();
        }
    }


    @SuppressLint("MissingPermission")
    private void requestNewLocationData(){
        LocationRequest mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(5000);
        mLocationRequest.setFastestInterval(1000);
        //mLocationRequest.setNumUpdates(5); //CUANTAS VECES QUIERE QUE SOLO HAGA LA LECTURA DE DATOS
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mFusedLocationClient.requestLocationUpdates( mLocationRequest, mLocationCallback, Looper.myLooper());
    }

    private LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            Location mLastLocation = locationResult.getLastLocation();
            lat_pr.setText(mLastLocation.getLatitude()+"");
            lon_pr.setText(mLastLocation.getLongitude()+"");
            insert_gps(dni_user);
        }
    };

    private boolean checkPermissions() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},PERMISSION_ID);
    }

    private boolean isLocationEnabled() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_ID) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getLastLocation();
            }
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        if (checkPermissions()) {
            getLastLocation();
        }
    }

    public void insert_gps(final String dni){
        final String longitud = lon_pr.getText().toString();
        final String latitud = lat_pr.getText().toString();

        //Toast.makeText(getApplicationContext(),"LATITUD "+latitud,Toast.LENGTH_SHORT).show();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, "https://satelital.perucontrols.com/perucontrols/sistema/loginapp/geocerca/gps_a.php?lon="+longitud+"&lat="+latitud+"&dni="+dni,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                            String[] arrayString = response.split(";");

                            if(arrayString[1].equals("1")){

                                StringRequest stringRequest = new StringRequest(Request.Method.GET,"https://satelital.perucontrols.com/perucontrols/sistema/loginapp/geocerca/estado_geo.php?dni="+dni,
                                        new Response.Listener<String>() {
                                            @Override
                                            public void onResponse(String response) {
                                                String[] arrayString = response.split(";");

                                                if(arrayString[1].equals("1")){
                                                    btn_a.setVisibility(View.VISIBLE);
                                                    btn_a_campo.setVisibility(View.INVISIBLE);

                                                    btn_t.setVisibility(View.VISIBLE);
                                                    btn_t_campo.setVisibility(View.INVISIBLE);

                                                    btn_a.setEnabled(true);
                                                    btn_t.setEnabled(true);

                                                }else if(arrayString[1].equals("0")){
                                                    btn_a.setVisibility(View.INVISIBLE);
                                                    btn_a_campo.setVisibility(View.VISIBLE);

                                                    btn_t.setVisibility(View.INVISIBLE);
                                                    btn_t_campo.setVisibility(View.VISIBLE);

                                                    btn_a_campo.setEnabled(true);
                                                    btn_t_campo.setEnabled(true);
                                                }
                                                   // Toast.makeText(getApplicationContext(),arrayString[1],Toast.LENGTH_SHORT).show();
                                            }
                                        },
                                        new Response.ErrorListener() {
                                            @Override
                                            public void onErrorResponse(VolleyError error) { /**/ }
                                        });
                                        //adding our stringrequest to queue
                                        Volley.newRequestQueue(PrincipalActivity.this).add(stringRequest);

                            }else {
                                Toast.makeText(getApplicationContext(),"ERROR RED",Toast.LENGTH_SHORT).show();
                            }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) { /**/ }
                });
        //adding our stringrequest to queue
        Volley.newRequestQueue(this).add(stringRequest);
    }



}
