package perucode.center.qr_pcs;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Looper;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.util.HashMap;
import java.util.Map;

public class AsistenciaCampo extends AppCompatActivity {

    int PERMISSION_ID = 1001;
    public FusedLocationProviderClient mFusedLocationClient;
    public Button btn_ingreso, btn_salida;
    TextView lon, lat;
    public static String dni_u;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asistencia_campo);

        btn_ingreso = (Button) findViewById(R.id.btn_ingreso_campo);
        btn_salida = (Button) findViewById(R.id.btn_salida_campo);

        lon = (TextView) findViewById(R.id.txt_lon_campo);
        lat = (TextView) findViewById(R.id.txt_lat_campo);

        Bundle extras = getIntent().getExtras();
        dni_u = extras.getString("dni_usuario");

        btn_ingreso.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                ingreso_campo();
            }
        });

        btn_salida.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                salida_campo();
            }
        });

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        getLastLocation();

        insert_gps(dni_u);
    }

    public void ingreso_campo(){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "https://satelital.perucontrols.com/perucontrols/sistema/loginapp/asistencia.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response.contains("1")){
                            String[] arrayString = response.split(";");

                            final String fecha_db = arrayString[3];

                            if(arrayString[1].equals("0")){

                                final AlertDialog.Builder builder =  new AlertDialog.Builder(AsistenciaCampo.this);
                                LayoutInflater inflater = getLayoutInflater();
                                final View view = inflater.inflate(R.layout.asistencia_campo, null);
                                final TextView fecha_actual = view.findViewById(R.id.txt_fecha_asistencia);
                                final TextView title = view.findViewById(R.id.txt_title_asistencia);
                                final Button registro = view.findViewById(R.id.btn_confirmar_asistencia);

                                title.setText("CONFIRMAR ASISTENCIA");
                                fecha_actual.setText(arrayString[2]);
                                builder.setView(view);
                                final AlertDialog popud = builder.show();

                                registro.setOnClickListener(new View.OnClickListener(){
                                    @Override
                                    public void onClick(View v){
                                        popud.dismiss();
                                        registro_ingreso_campo(dni_u,fecha_db);
                                    }
                                });
                            }else if(arrayString[1].equals("1")){
                                Toast.makeText(getApplicationContext(),"Ya se registro una asistencia",Toast.LENGTH_SHORT).show();
                            }

                        }else{
                            Toast.makeText(getApplicationContext(),"Error-Problema de Red",Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){ /**/ }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("dni_u",dni_u);
                params.put("accion","1");

                return params;
            }
        };

        Volley.newRequestQueue(AsistenciaCampo.this).add(stringRequest);
    }

    public void salida_campo() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "https://satelital.perucontrols.com/perucontrols/sistema/loginapp/asistencia.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response.contains("1")){

                            String[] arrayString = response.split(";");

                            final String fecha_db = arrayString[3];

                            if(arrayString[4].equals("0")){

                                if(arrayString[1].equals("1")){

                                    final AlertDialog.Builder builder =  new AlertDialog.Builder(AsistenciaCampo.this);
                                    LayoutInflater inflater = getLayoutInflater();
                                    final View view = inflater.inflate(R.layout.asistencia_campo, null);
                                    final TextView fecha_actual = view.findViewById(R.id.txt_fecha_asistencia);
                                    final TextView title = view.findViewById(R.id.txt_title_asistencia);
                                    final Button registro = view.findViewById(R.id.btn_confirmar_asistencia);

                                    title.setText("CONFIRMACIÓN DE SALIDA");
                                    fecha_actual.setText(arrayString[2]);
                                    builder.setView(view);
                                    final AlertDialog popud = builder.show();

                                    registro.setOnClickListener(new View.OnClickListener(){
                                        @Override
                                        public void onClick(View v){
                                            popud.dismiss();
                                            registro_salida_campo(dni_u,fecha_db);
                                        }
                                    });

                                }else if(arrayString[1].equals("0")){
                                    Toast.makeText(getApplicationContext(),"Registrar primero llegada a campo",Toast.LENGTH_SHORT).show();
                                }

                            }else if(arrayString[4].equals("1")){
                                Toast.makeText(getApplicationContext(),"Salida de campo ya registrada",Toast.LENGTH_SHORT).show();
                            }

                        }else{
                            Toast.makeText(getApplicationContext(),"Error-Problema de Red",Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){ /**/ }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("dni_u",dni_u);
                params.put("accion","2");

                return params;
            }
        };

        Volley.newRequestQueue(AsistenciaCampo.this).add(stringRequest);

    }

    /*--------------------------------------------------------------------------------------------*/

    public void registro_ingreso_campo(final String dni, final String fecha_db){
        StringRequest request = new StringRequest(Request.Method.POST, "https://satelital.perucontrols.com/perucontrols/sistema/loginapp/registro_asistencia.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response.contains("1")){
                            Toast.makeText(getApplicationContext(),"Asistencia registrado con exitoso!",Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(getApplicationContext(),"Error-Problema de Red",Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){  }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("dni",dni);
                params.put("fecha",fecha_db);
                params.put("tipo_asistencia","3");
                return params;
            }
        };
        Volley.newRequestQueue(this).add(request);
    }

    public void registro_salida_campo(final String dni, final String fecha_db){
        StringRequest request = new StringRequest(Request.Method.POST, "https://satelital.perucontrols.com/perucontrols/sistema/loginapp/registro_asistencia.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response.contains("1")){
                            Toast.makeText(getApplicationContext(),"Salida registrado con exito!",Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(getApplicationContext(),"Error-Problema de Red",Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){  }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("dni",dni);
                params.put("fecha",fecha_db);
                params.put("tipo_asistencia","4");
                return params;
            }
        };
        Volley.newRequestQueue(this).add(request);
    }


    /*--------------------------------------------------------------------------------------------*/
    @SuppressLint("MissingPermission")
    private void getLastLocation(){
        if (checkPermissions()) {
            if (isLocationEnabled()) {
                mFusedLocationClient.getLastLocation().addOnCompleteListener(
                        new OnCompleteListener<Location>() {
                            @Override
                            public void onComplete(@NonNull Task<Location> task) {
                                Location location = task.getResult();
                                requestNewLocationData();
                                lat.setText(location.getLatitude()+"");
                                lon.setText(location.getLongitude()+"");
                            }
                        }
                );
            } else {
                Toast.makeText(this, "Activar ubicación", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        } else {
            requestPermissions();
        }
    }


    @SuppressLint("MissingPermission")
    private void requestNewLocationData(){
        insert_gps(dni_u);
        LocationRequest mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(5000);
        mLocationRequest.setFastestInterval(1000);
        //mLocationRequest.setNumUpdates(5);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mFusedLocationClient.requestLocationUpdates( mLocationRequest, mLocationCallback, Looper.myLooper());
    }

    private LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            insert_gps(dni_u);
            Location mLastLocation = locationResult.getLastLocation();
            lat.setText(mLastLocation.getLatitude()+"");
            lon.setText(mLastLocation.getLongitude()+"");
        }
    };

    private boolean checkPermissions() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},PERMISSION_ID);
    }

    private boolean isLocationEnabled() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_ID) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getLastLocation();
            }
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        if (checkPermissions()) {
            getLastLocation();
        }
    }


    public void insert_gps(final String dni){
        final String longitud = lon.getText().toString();
        final String latitud = lat.getText().toString();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, "https://satelital.perucontrols.com/perucontrols/sistema/loginapp/geocerca/gps_a.php?lon="+longitud+"&lat="+latitud+"&dni="+dni,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        String[] arrayString = response.split(";");

                        if(arrayString[1].equals("1")){

                            StringRequest stringRequest = new StringRequest(Request.Method.GET,"https://satelital.perucontrols.com/perucontrols/sistema/loginapp/geocerca/estado_geo.php?dni="+dni,
                                    new Response.Listener<String>() {
                                        @Override
                                        public void onResponse(String response) {
                                            String[] arrayString = response.split(";");

                                            String atr = "1";

                                            if(arrayString[1].equals(atr)){
                                                finish();
                                            }
                                        }
                                    },
                                    new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) { /**/ }
                                    });
                            //adding our stringrequest to queue
                            Volley.newRequestQueue(AsistenciaCampo.this).add(stringRequest);

                        }else {
                            Toast.makeText(getApplicationContext(),"ERROR RED",Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) { /**/ }
                });
        //adding our stringrequest to queue
        Volley.newRequestQueue(this).add(stringRequest);
    }

}
