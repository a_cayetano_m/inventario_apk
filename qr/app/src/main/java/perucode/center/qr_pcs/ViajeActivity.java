package perucode.center.qr_pcs;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class ViajeActivity extends AppCompatActivity {

    Button btn_ini, btn_fin;
    public static String dni_u;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viaje);

        btn_ini = (Button) findViewById(R.id.btn_inicio);
        btn_fin = (Button) findViewById(R.id.btn_fin);

        btn_ini.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                viaje_i();
            }
        } );

        btn_fin.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                viaje_f();
            }
        });

        Bundle extras = getIntent().getExtras();
        dni_u = extras.getString("dni_usuario");
    }

    public void viaje_i(){
        //RETORNO VALOR
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "https://satelital.perucontrols.com/perucontrols/sistema/loginapp/viaje.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response.contains("1")){
                            AlertDialog.Builder builderr =  new AlertDialog.Builder(ViajeActivity.this);
                            LayoutInflater inflater = getLayoutInflater();
                            View view = inflater.inflate(R.layout.viaje_popud, null);

                            final EditText destino = view.findViewById(R.id.txt_destino);
                            Button btn_viaje = view.findViewById(R.id.btn_regViaje);
                            Button btn_viajef = view.findViewById(R.id.btn_regViajefinal);

                            btn_viajef.setVisibility(View.INVISIBLE);
                            btn_viaje.setVisibility(View.VISIBLE);

                            String[] arrayString = response.split(";");

                            if(arrayString[1].equals("0")){
                                builderr.setView(view);
                                builderr.show();
                            }else if(arrayString[1].equals("1")){
                                Toast.makeText(getApplicationContext(),"Hay un destino ya registrado",Toast.LENGTH_SHORT).show();
                            }

                            btn_viaje.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    finish();
                                    registro(dni_u,destino.getText().toString());
                                }
                            });

                        }else{
                            Toast.makeText(getApplicationContext(),"Error-Problema de Red",Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){ /**/ }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("dni_u",dni_u);
                return params;
            }
        };

        Volley.newRequestQueue(ViajeActivity.this).add(stringRequest);
        // FIN RETORNO VALOR
    }

    public void registro(final String dni, final String destino){
        StringRequest request = new StringRequest(Request.Method.POST, "https://satelital.perucontrols.com/perucontrols/sistema/loginapp/registro_viaje.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response.contains("1")){
                            Toast.makeText(getApplicationContext(),"Destino guardado!",Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(getApplicationContext(),"Error-Problema de Red",Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){ /**/ }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("dni",dni);
                params.put("destino",destino);
                return params;
            }
        };
        Volley.newRequestQueue(this).add(request);
    }

    public void viaje_f(){
        final AlertDialog.Builder builderr =  new AlertDialog.Builder(ViajeActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        final View view = inflater.inflate(R.layout.viaje_popud, null);

        final EditText destino = view.findViewById(R.id.txt_destino);
        final TextView fecha_ref = view.findViewById(R.id.txt_fecha_reg);
        Button btn_viaje = view.findViewById(R.id.btn_regViaje);
        final Button btn_viajef = view.findViewById(R.id.btn_regViajefinal);

        btn_viaje.setVisibility(View.INVISIBLE);
        btn_viajef.setVisibility(View.VISIBLE);

        //RETORNO VALOR
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "https://satelital.perucontrols.com/perucontrols/sistema/loginapp/viaje.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response.contains("1")){
                            String[] arrayString = response.split(";");

                            if(arrayString[1].equals("0")){

                                Toast.makeText(getApplicationContext(),"No hay un inicio de viaje!",Toast.LENGTH_SHORT).show();

                            }else if(arrayString[1].equals("1")){
                                destino.setText(arrayString[2]);
                                fecha_ref.setText(arrayString[3]);

                                builderr.setView(view);
                                builderr.show();
                            }

                            btn_viajef.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    finish();
                                    registro_fin(dni_u,fecha_ref.getText().toString());
                                }
                            });

                        }else{
                            Toast.makeText(getApplicationContext(),"Error-Problema de Red",Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error){  }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String,String> params = new HashMap<>();
                        params.put("dni_u",dni_u);
                        return params;
                    }
                };

        Volley.newRequestQueue(ViajeActivity.this).add(stringRequest);
        // FIN RETORNO VALOR
    }

    public void registro_fin(final String dni, final String fecha){
        StringRequest request = new StringRequest(Request.Method.POST, "https://satelital.perucontrols.com/perucontrols/sistema/loginapp/registro_fin_viaje.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response.contains("1")){
                            Toast.makeText(getApplicationContext(),"Destino finalizado correctamente!",Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(getApplicationContext(),"Error-Problema de Red",Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){  }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("dni",dni);
                params.put("fecha",fecha);
                return params;
            }
        };
        Volley.newRequestQueue(this).add(request);
    }



}
