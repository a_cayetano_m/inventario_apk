package perucode.center.qr_pcs;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    public Button btn_login;
    public Button btn_login_web;
    public Vibrator vibrator ;
    public int var_uno = 1;
    //public ArrayList productList = new ArrayList<>();
    public SurfaceView cameraPreview;
    public TextView txtResult;
    public TextView mostrar;
    public BarcodeDetector barcodeDetector;
    public CameraSource cameraSource;
    final int RequestCameraPermissionID = 1001;
    public static String dni_u;

    //public static final String URL_PRODUCTS = "https://satelital.perucontrols.com/perucontrols/sistema/pcs_reporte_trabajo_nu.php";

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case RequestCameraPermissionID: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    try {
                        cameraSource.start(cameraPreview.getHolder());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Bundle extras = getIntent().getExtras();
        dni_u = extras.getString("dni_usuario");

        //btn_login=findViewById(R.id.btn_login); // boton login
        /*btn_login.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                login();
            }
        } );*/

        //btn_login_web=findViewById(R.id.btn_web); // boton login
        /*btn_login_web.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                open_web();
            }
        } );*/

        cameraPreview = (SurfaceView) findViewById(R.id.cameraPreview);
        txtResult = (TextView) findViewById(R.id.txtResult);
        ///mostrar = (TextView) findViewById(R.id.mostrar);

        barcodeDetector = new BarcodeDetector.Builder(this)
                .setBarcodeFormats(Barcode.QR_CODE)
                .build();
        cameraSource = new CameraSource
                .Builder(this, barcodeDetector)
                .setRequestedPreviewSize(640, 480)
                .build();
        //Add Event
        cameraPreview.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder surfaceHolder) {
                if (ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    //Request permission
                    ActivityCompat.requestPermissions(MainActivity.this,
                            new String[]{Manifest.permission.CAMERA},RequestCameraPermissionID);
                    return;
                }
                try {
                    cameraSource.start(cameraPreview.getHolder());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) { /**/ }

            @Override
            public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
                cameraSource.stop();
            }
        });

        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() { /**/ }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                final SparseArray<Barcode> qrcodes = detections.getDetectedItems();
                if(qrcodes.size() != 0)
                {
                    txtResult.post(new Runnable() {
                        @Override
                        public void run() {
                            //Create vibrate
                            if(var_uno==1){
                                vibrator  = (Vibrator)getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                                vibrator.vibrate(1000);
                                txtResult.setText(qrcodes.valueAt(0).displayValue);

                                String w_identidad = txtResult.getText().toString();
                                String subString_w = w_identidad.substring(4,5);

                                if(subString_w.equals("-")){
                                    final AlertDialog.Builder builder =  new AlertDialog.Builder(MainActivity.this);
                                    LayoutInflater inflater = getLayoutInflater();
                                    final View view = inflater.inflate(R.layout.option_scanner, null);
                                    final Button preventivo = view.findViewById(R.id.btn_preventivo);
                                    final Button correctivo = view.findViewById(R.id.btn_correctivo);

                                    preventivo.setOnClickListener(new View.OnClickListener(){
                                        @Override
                                        public void onClick(View v){
                                            scanner_web(txtResult.getText().toString(),"pre",dni_u);
                                        }
                                    });

                                    correctivo.setOnClickListener(new View.OnClickListener(){
                                        @Override
                                        public void onClick(View v){
                                            scanner_web(txtResult.getText().toString(),"corr",dni_u);
                                        }
                                    });

                                    builder.setView(view);
                                    final AlertDialog popud = builder.show();
                                }else{
                                    Toast.makeText(getApplicationContext(),"Formato de QR erroneo!",Toast.LENGTH_SHORT).show();
                                }

                                var_uno=2;
                            }
                        }
                    });
                }else{
                    var_uno=1;
                    //vibrator.cancel();
                }
            }
        });
    }

    public void scanner_web(String num_work_web, String tip_reporte, String dni){
        Intent i = new Intent(getApplicationContext(),WebActivity.class);
        i.putExtra("URL", "http://satelital.perucontrols.com/perucontrols/sistema/pcs_reporte_trabajo.php?works_identidad="+num_work_web+"&tip_reporte="+tip_reporte+"&dni="+dni);
        startActivity(i);
    }

    public void login(){
        StringRequest request = new StringRequest(Request.Method.POST, "http://webservices.perucontrols.com/app_pcs/qr_consulta.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        String alldata = "";
                        //TextView myAwesomeTextView = (TextView)findViewById(R.id.mostrar);

                        if(!response.isEmpty()){

                            try {
                                JSONArray array = new JSONArray(response);
                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject product = array.getJSONObject(i);

                                    String cat_nombre = product.optString("cat_nombre");
                                    String works_odm_serv  = product.optString("works_odm_serv");
                                    String works_od_serv =  product.optString("works_od_serv");
                                    String worktipo_nombre  =  product.optString("worktipo_nombre");
                                    String works_descrip  =  product.optString("works_descrip");
                                    String works_tecnico  =  product.optString("works_tecnico");
                                    String ep_nombre =  product.optString("ep_nombre");
                                    String puerto_nombre  =  product.optString("puerto_nombre");
                                    String works_fecha  =  product.optString("works_fecha");

                                    alldata += "|Fecha="+works_fecha+"| \n Sistema="+cat_nombre+"|Odm="+works_odm_serv+"|Od="+works_od_serv+"|Trabajo="+worktipo_nombre+"|Tecnico="+works_tecnico+"|EP="+ep_nombre+"|Puerto="+puerto_nombre+"|Descrip="+works_descrip+"\n\n";
                                }

                                /*myAwesomeTextView.setMovementMethod(new ScrollingMovementMethod());
                                myAwesomeTextView.setTextColor(Color.rgb(255,255,255));
                                myAwesomeTextView.setText(alldata);*/

                                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(txtResult.getWindowToken(), 0);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }else{
                            /*myAwesomeTextView.setMovementMethod(new ScrollingMovementMethod());
                            myAwesomeTextView.setTextColor(Color.rgb(255,140,0));
                            myAwesomeTextView.setText("****A TRABAJAR, NO HAY NADA*****");*/
                            Toast.makeText(getApplicationContext(),"SIN INFORMACIÓN",Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error){
                        System.out.println("ERROR: "+error);
                    }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String,String> params = new HashMap<>();
                        params.put("cod_qr",txtResult.getText().toString());
                        return params;
                    }
                };
        Volley.newRequestQueue(this).add(request);
    }

    /*public void open_web(){
        AlertDialog.Builder builderr =  new AlertDialog.Builder(MainActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.trabajo_popud, null);

        Button nuevo = view.findViewById(R.id.btn_nuevo);
        Button continuar = view.findViewById(R.id.btn_continuar);

        builderr.setView(view);
        builderr.show();

        nuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_PRODUCTS+"?dni="+dni_u+"",
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                /*try {
                                    //set time in mili
                                    Thread.sleep(1000);

                                   // Intent i =  new Intent(getApplicationContext(),WebActivity.class);

                                    //Toast.makeText(getApplicationContext(),response,Toast.LENGTH_SHORT).show();
                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                                Intent i = new Intent(getApplicationContext(),WebActivity.class);
                                i.putExtra("dni_usuario", dni_u);
                                startActivity(i);
                                finish();
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) { /* }
                        });

                //adding our stringrequest to queue
                Volley.newRequestQueue(MainActivity.this).add(stringRequest);
            }
        });


        continuar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //registro_translado(dni_u,destino.getText().toString());
            }
        });
    }*/

}
